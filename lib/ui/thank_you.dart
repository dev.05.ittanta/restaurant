import 'package:flutter/material.dart';
import 'package:restaurant/utils/custom_color.dart';
class ThankYou extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ThankYouState();
  }
}

class ThankYouState extends State<ThankYou> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,

        children: [
          Container(
            color: Colors.white,
            width: double.infinity,
            height: double.infinity,
            child: ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              padding: EdgeInsets.all(20),
              children: [
                SizedBox(
                  height: 50,
                ),
                Text("Successfully",textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 40, color: green_clr)),
                SizedBox(
                  height: 50,
                ),
                Image.asset("assets/images/thank_you_success.png", height: 250),
                SizedBox(
                  height: 10,
                ),
                Text("Your Order has been placed Successfully",textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 20, color: Colors.black)),
                SizedBox(
                  height: 10,
                ),
                Text("Oder Number: ",textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 15, color: Colors.black)),
                SizedBox(
                  height: 10,
                ),
                Text("name: ",textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 15, color: Colors.black)),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: 50,
                 height: 50,
                  child: FlatButton(
                    onPressed: null,
                    child: Text('CONTINUE', style: TextStyle(
                        color:primary
                    )
                    ),
                    textColor: primary,
                    shape: RoundedRectangleBorder(side: BorderSide(
                        color: primary,
                        width: 1,
                        style: BorderStyle.solid
                    ), borderRadius: BorderRadius.circular(50)),
                  )
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
