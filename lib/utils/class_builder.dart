import 'package:flutter/material.dart';
import 'package:restaurant/about_us.dart';
import 'package:restaurant/ui/category.dart';
import 'package:restaurant/ui/change_password.dart';
import 'package:restaurant/ui/contact.dart';
import 'package:restaurant/ui/favourite.dart';
import 'package:restaurant/ui/gallary.dart';
import 'package:restaurant/ui/my_order.dart';
import 'package:restaurant/ui/profile.dart';
import 'package:restaurant/ui/reservations.dart';

import '../ui/home_page.dart';

typedef T Constructor<T>();

final Map<String, Constructor<Object>> _constructors = <String, Constructor<Object>>{};

void register<T>(Constructor<T> constructor) {
  _constructors[T.toString()] = constructor;
}

class ClassBuilder {
  static void registerClasses() {
    register<HomePage>(() => HomePage());
    register<CategoryList>(() => CategoryList());
    register<Reservations>(() => Reservations());
    register<AboutUs>(() => AboutUs());
    register<Gallary>(() => Gallary());
    register<ContactScreen>(() => ContactScreen());
    register<Favourite>(() => Favourite());
    register<MyOrders>(() => MyOrders());
    register<ChangePassword>(() => ChangePassword());
    register<Profile>(() => Profile());


  }

  static dynamic fromString(String type) {
    return _constructors[type]();
  }
}
