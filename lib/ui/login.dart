import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:restaurant/blocs/user_bloc.dart';
import 'package:restaurant/models/user_data.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/forgot_password.dart';
import 'package:restaurant/ui/sign_up.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../blocs/login_bloc.dart';

import 'drawer_list.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends Base<Login> {
  final email_mobileController = TextEditingController();
  final passwordController = TextEditingController();
  final FacebookLogin facebookSignIn = new FacebookLogin();
  GoogleSignIn _googleSignIn;
  bool _isLoggedIn = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                                AssetImage('assets/images/login_half_bg.png'),
                            fit: BoxFit.fill)),
                  )),
            ],
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                SizedBox(height: 130.h),
                Image.asset(
                  'assets/images/login_logo.png',
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 50.h),
                Text(
                  Constants.WELCOME_RESTAURENT,
                  style: TextStyle(fontSize: 17.sp),
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
          Center(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              elevation: 20,
              margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      Constants.LOGIN,
                      style: TextStyle(fontSize: 23.sp, color: font_clr),
                      textAlign: TextAlign.left,
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    TextField(
                      controller: email_mobileController,
                      decoration: new InputDecoration(
                        hintText: Constants.MOBILE_OR_EMAIL,
                        hintStyle: TextStyle(color: hint_clr),
                        isDense: true,
                        enabledBorder: const OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        border: const OutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    TextField(
                      obscureText: true,
                      controller: passwordController,
                      decoration: new InputDecoration(
                        hintText: Constants.PASSWORD,
                        hintStyle: TextStyle(color: hint_clr),
                        isDense: true,
                        enabledBorder: const OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                        ),
                        border: const OutlineInputBorder(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ButtonTheme(
                  height: 40.h,
                  minWidth: 320.w,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(color: Colors.white)),
                    onPressed: () async {
                      if(await isConnected()){
                      if (isValid()) {
                        showLoading();
                        _getLoginData();
                      }}
                    },
                    color: Colors.white,
                    textColor: sky_blue,
                    child: Text(Constants.LOGIN,
                        style: TextStyle(fontSize: 18.sp)),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                InkWell(
                  onTap: () {
                    push(ForgotPassword());
                  },
                  child: Text(Constants.FORGOT_PASSWORD,
                      style: TextStyle(fontSize: 18.sp, color: white)),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Text("Login with",
                    style: TextStyle(fontSize: 10.sp, color: white)),
                SizedBox(
                  height: 10.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                        onTap: () async {
                          if(await isConnected()){
                            showLoading();
                            googleLogin();
                          }

                        },
                        child: Image.asset('assets/images/google_plus.png',
                            fit: BoxFit.cover, height: 35.h, width: 35.h)),
                    SizedBox(
                      width: 10.w,
                    ),
                    InkWell(
                        onTap: () async {
                          if(await isConnected()) {
                            _facebookLogin();
                          }
                        },
                        child: Image.asset('assets/images/facebook.png',
                            fit: BoxFit.cover, height: 35.h, width: 35.w)),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                Center(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("New User?",
                          style: TextStyle(fontSize: 15.sp, color: white),
                          textAlign: TextAlign.end),
                      SizedBox(
                        width: 15.w,
                      ),
                      InkWell(
                        onTap: () {
                          push(SignUp());
                        },
                        child: Text("REGISTER NOW",
                            style: TextStyle(fontSize: 15.sp, color: white),
                            textAlign: TextAlign.start),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 30.h,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  void googleLogin() async {
     _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
        // you can add extras if you require
      ],
    );

    _googleSignIn.signIn().then((GoogleSignInAccount acc) async {
      prefs.setString(Constants.DISPLAY_NAME, acc.displayName);
      prefs.setString(Constants.DISPLAY_EMAIL, acc.email);
      _getSocialDetail(acc.id,"Google");
      setState(() {
        _isLoggedIn = true;
      });
      // GoogleSignInAuthentication auth = await acc.authentication;
      // acc.authentication.then((GoogleSignInAuthentication auth) async {
      //   print(auth.idToken);
      //   print(auth.accessToken);
      // });

    });
  }
  _logout(){
    _googleSignIn.signOut();
    setState(() {
      _isLoggedIn = false;
    });
  }
  // void facebookLogin() async {
  //   // final facebookLogin = FacebookLogin();
  //   // final facebookLoginResult = await facebookLogin.logIn(['email']);
  //   //
  //   // // print(facebookLoginResult.accessToken.userId);
  //   //
  //   // print(facebookLoginResult.errorMessage);
  //   // print(facebookLoginResult.status);
  //   //
  //   // final token = facebookLoginResult.accessToken.token;
  //   //
  //   // try {
  //   //   Response response = await Dio().get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=$token');
  //   //   final profile = json.decode(response.toString());
  //   //   print(profile);
  //   //   print(response);
  //   // } catch (e) {
  //   //   print(e);
  //   // }
  //
  //   final facebookLogin = FacebookLogin();
  //   final result = await facebookLogin.logInWithReadPermissions(['email']);
  //
  //   switch (result.status) {
  //     case FacebookLoginStatus.loggedIn:
  //       _sendTokenToServer(result.accessToken.token);
  //       _showLoggedInUI();
  //       break;
  //     case FacebookLoginStatus.cancelledByUser:
  //       _showCancelledMessage();
  //       break;
  //     case FacebookLoginStatus.error:
  //       _showErrorOnUI(result.errorMessage);
  //       break;
  //   }
  // }

  Future<Null> _facebookLogin() async {
    final FacebookLoginResult result = await facebookSignIn.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        showMessage('''
         Logged in!
         
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
        break;
      case FacebookLoginStatus.cancelledByUser:
        showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        showMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();
    showMessage('Logged out.');
  }

  Future<void> _getLoginData() async {
    Map<String, dynamic> res = await bloc.doLogin(
        email_mobileController.text, passwordController.text);
    bool success = res["success"];
    if (success) {
      hideLoading();
      String token = res["data"]["token"];
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(Constants.APP_TOKEN, token);
      _getUserData();
    } else {
      showSnackBar(res["data"]["error"]);
      hideLoading();

    }
  }
  Future<void> _getSocialDetail(String id, String providerName) async {
    Map<String, dynamic> res = await bloc.doSocialDetail(providerName, id);
    bool success = res["success"];
    if (success) {
      hideLoading();
      String token = res["data"]["token"];
      prefs.setString(Constants.APP_TOKEN, token);
     _getUserData();
    } else {
      showSnackBar(res["data"]["error"]);
      hideLoading();
    }
  }
  Future<void> _getUserData() async {
    UserData userData = await userbloc.fetchAllUserData();
    prefs.setString(Constants.USER_DATA, json.encode(userData));
    pushReplacement(DrawerList());
  }
  bool isValid() {
    if (email_mobileController.text.isEmpty) {
      showSnackBar('Please enter email or mobile');
      return false;
    }
    if (passwordController.text.isEmpty) {
      showSnackBar('Please enter password');
      return false;
    }
    return true;
  }
}
