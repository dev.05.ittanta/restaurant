import 'dart:convert';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:restaurant/blocs/signup_bloc.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/detail_confomation.dart';
import 'package:restaurant/ui/drawer_list.dart';
import 'package:restaurant/ui/home_page.dart';
import 'package:restaurant/ui/login.dart';
import 'package:restaurant/utils/const.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SignUpState();
  }
}

class _SignUpState extends Base<SignUp> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPwController = TextEditingController();

  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    color: Colors.white,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                            AssetImage('assets/images/login_half_bg.png'),
                            fit: BoxFit.fill)),
                  )),
            ],
          ),
          ListView(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            children: [
              SizedBox(height: 20.h),
              Container(
                width: 90,
                height: 80,
                child: Image.asset(
                  'assets/images/login_logo.png',
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(height: 30.h),
              Text(
                Constants.SIGNUP_CONTINUE,
                style: TextStyle(fontSize: 17.sp),
                textAlign: TextAlign.center,
              ),
              Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 20,
                margin: EdgeInsets.fromLTRB(30, 20, 30, 0),
                child: Padding(
                  padding: EdgeInsets.all(20),
                  child: ListView(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    children: <Widget>[
                      Text(
                        Constants.SIGNUP,
                        style: TextStyle(fontSize: 23.sp, color: font_clr),
                        textAlign: TextAlign.left,
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextField(
                        controller: nameController,
                        decoration: new InputDecoration(
                          hintText: "Name",
                          hintStyle: TextStyle(color: Colors.black87),
                          isDense: true,
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextField(
                        controller: phoneController,
                        keyboardType: TextInputType.number,
                        decoration:  InputDecoration(
                          hintText: "Phone",
                          hintStyle: TextStyle(color: Colors.black87),
                          isDense: true,
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextField(
                        controller: emailController,
                        decoration: new InputDecoration(
                          hintText: "Email",
                          hintStyle: TextStyle(color: Colors.black87),
                          isDense: true,
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: new InputDecoration(
                          hintText: "Password",
                          hintStyle: TextStyle(color: Colors.black87),
                          isDense: true,
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      TextField(
                        controller: confirmPwController,
                        obscureText: true,
                        decoration: new InputDecoration(
                          hintText: "Confirm Password",
                          hintStyle: TextStyle(color: Colors.black87),
                          isDense: true,
                          enabledBorder: const OutlineInputBorder(
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          border: const OutlineInputBorder(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.h),
              ButtonTheme(
                height: 50,
                minWidth: 350,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(color: Colors.white)),
                  onPressed: () async{
                    if(await isConnected()){
                      if (isValid()) {
                        showLoading();
                        _getSignUpData();
                      }
                    }
                  },
                  color: Colors.white,
                  textColor: sky_blue,
                  child:
                  Text(Constants.SIGNUP, style: TextStyle(fontSize: 18)),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              Text("Sign up with",textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 10.sp, color: white)),
              SizedBox(
                height: 10.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () async{
                        if(await isConnected()){
                          showLoading();
                          googleLogin();
                        }
                      },
                      child: Image.asset('assets/images/google_plus.png',
                          fit: BoxFit.cover, height: 35.h, width: 35.w)),
                  SizedBox(
                    width: 10.w,
                  ),
                  InkWell(
                      onTap: () {

                      },
                      child: Image.asset('assets/images/facebook.png',
                          fit: BoxFit.cover, height: 35.h, width: 35.w)),
                ],
              ),
              SizedBox(
                height: 10.h,
              ),
              InkWell(
                onTap: () {
                  push(Login());
                },
                child: Text("Login",
                    style: TextStyle(fontSize: 15.sp, color: white),
                    textAlign: TextAlign.center),
              ),
              SizedBox(
                height: 10.h,
              ),

            ],
          ),


        ],
      ),
    );
  }

  void googleLogin() async {
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
        // you can add extras if you require
      ],
    );

    await _googleSignIn.signIn().then((GoogleSignInAccount acc) async {
      hideLoading();
      String provider = "Google";
      pushReplacement(DetailConfomation(provider: provider,
          providerId: acc.id,
          name: acc.displayName,
          email: acc.email,
          photoUrl: acc.photoUrl));
    });
  }

  Future<void> _getSignUpData() async {
    Map<String, dynamic> res = await bloc.doSignUp(
        nameController.text,
        emailController.text,
        phoneController.text,
        passwordController.text,
        confirmPwController.text);

    bool success = res["success"];
    if (success) {
      hideLoading();
      String token = res["data"]["token"];
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString(Constants.APP_TOKEN, token);
      prefs.setString(Constants.DISPLAY_EMAIL, emailController.text);
      prefs.setString(Constants.DISPLAY_NAME,  res["data"]["name"]);
      pushReplacement(DrawerList());
    } else {
      if (res["data"].containsKey("email")) {
        showSnackBar(res["data"]["email"][0]);
      } else if (res["data"].containsKey("phone")) {
        showSnackBar(res["data"]["phone"][0]);
      }
      hideLoading();
    }
  }

  bool isValid() {
    if (nameController.text.isEmpty) {
      showSnackBar('Please enter name');
      return false;
    }
    if (emailController.text.isEmpty) {
      showSnackBar('Please enter email');
      return false;
    }
    if (phoneController.text.isEmpty) {
      showSnackBar('Please enter phone');
      return false;
    }
    if (confirmPwController.text.isEmpty) {
      showSnackBar('Please enter password');
      return false;
    }
    if (passwordController.text != confirmPwController.text) {
      showSnackBar('Password is not matched');
    }
    return true;
  }
}
