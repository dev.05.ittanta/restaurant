import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/category_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/sub_category.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';
import 'package:restaurant/utils/oval_shadow.dart';

import '../blocs/category_bloc.dart';

class CategoryList extends KFDrawerContent {
  @override
  State<StatefulWidget> createState() {
    return CategoryState();
  }
}

class CategoryState extends Base<CategoryList> {
  final TextEditingController _searchControl = new TextEditingController();
  List<Categories> _searchResult = [];
  List<Categories> _categoryList = [];
  List<Filters> _filterList = [];

  int startValue = 1;
  Map<Filters, bool> filterValues = new Map();
  final _repository = Repository();
  int cartCount = 0;
  int selectedQuickFilterRadioTile = 0;
  int selectedMenuFilterRadioTile = 0;
  int filterSelectedId;

  @override
  void initState() {
    super.initState();
    getCartCount();
  }

  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchAllCategorys();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Image.asset(
                "assets/images/menu.png",
                color: Colors.black,
                fit: BoxFit.contain,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "Menu",
            textAlign: TextAlign.center,
            style: TextStyle(color: sky_blue, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount!=null?cartCount.toString():"0",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Column(
        children: [
          searchBar(),
          Expanded(
            flex: 1,
            child: StreamBuilder(
              stream: bloc.allCategory,
              builder: (context, AsyncSnapshot<Category> snapshot) {
                if (snapshot.hasData) {
                  _categoryList = snapshot.data != null
                      ? snapshot.data.data.categories
                      : null;
                  _filterList = snapshot.data.data.filters;
                  filterValues.clear();
                  // for (Filters filters
                  //     in snapshot.data.data.filters[0].options) {
                  //   filterValues.putIfAbsent(filters, () => false);
                  // }

                  return buildList();
                } else if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget buildList() {
    List<Categories> searchfilterLists = [];
    if (_searchResult != null && _searchResult.length > 0) {
      searchfilterLists = _searchResult;
    } else {
      searchfilterLists = _categoryList;
    }
    return Container(
        decoration: BoxDecoration(color: category_bg),
        child: Center(
          child: searchfilterLists != null
              ? ListView.builder(
                  itemCount: searchfilterLists.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = searchfilterLists[index];
                    return itemUi(item);
                  })
              : CircularProgressIndicator(),
        ));
  }

  Widget itemUi(Categories item) {
    return GestureDetector(
      onTap: () {
        push(SubCategoryList(catData: item,filterLists:_filterList));
      },
      child: Container(
        margin: EdgeInsets.only(top: 20),
        child: Stack(
          children: [
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 25, right: 25),
              child: Padding(
                padding: const EdgeInsets.all(3.0),
                child: Row(
                  children: [
                    Container(
                      width: 50.w,
                      height: 50.h,
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(25.0),
                        child: FadeInImage.assetNetwork(
                          placeholder: 'assets/images/no_img.png',
                          image: item.image,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Expanded(
                        child: Text(item.name,
                            style: TextStyle(
                                color: Colors.black, fontSize: 15.h))),
                  ],
                ),
              ),
            ),
            Positioned.fill(
                right: 10,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset(
                    'assets/images/arrow_shadow.png',
                    width: 35,
                    height: 35,
                    fit: BoxFit.cover,
                  ),
                )),
          ],
        ),
      ),
    );
  }

  // appBar(Image dwricon, String title, Color bgclr, Color titleclr) {
  //   return Container(
  //     color: bgclr,
  //     child: Row(
  //       children: <Widget>[
  //         ClipRRect(
  //           borderRadius: BorderRadius.all(Radius.circular(32.0)),
  //           child: Material(
  //             shadowColor: Colors.transparent,
  //             color: Colors.transparent,
  //             child: IconButton(
  //               icon: dwricon,
  //               onPressed: widget.onMenuPressed,
  //             ),
  //           ),
  //         ),
  //         Expanded(
  //           flex: 1,
  //           child: Center(
  //             child: Text(
  //               title,
  //               textAlign: TextAlign.center,
  //               style: TextStyle(color: titleclr, fontSize: 15.h),
  //             ),
  //           ),
  //         ),
  //         GestureDetector(
  //           onTap: () {},
  //           child: Image.asset("assets/images/home_food_menu.png",
  //               width: 25.h, height: 20.h),
  //         ),
  //         SizedBox(width: 10.h),
  //         Stack(
  //           children: [
  //             Padding(
  //               padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
  //               child: Text(
  //                 cartCount.toString(),
  //                 textAlign: TextAlign.center,
  //                 style: TextStyle(color: Colors.red, fontSize: 10.h),
  //               ),
  //             ),
  //             GestureDetector(
  //               onTap: () {
  //                 push(AddToCart());
  //               },
  //               child: Image.asset("assets/images/cart.png",
  //                   width: 25.h, height: 25.h),
  //             ),
  //           ],
  //         ),
  //
  //         SizedBox(width: 15.h),
  //       ],
  //     ),
  //   );
  // }

  searchBar() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(
              Radius.circular(0.0),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: TextField(
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.white,
                  ),
                  onChanged: onSearchTextChanged,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20.0),
                    // border: OutlineInputBorder(
                    //   borderRadius: BorderRadius.circular(5.0),
                    //   borderSide: BorderSide(color: Colors.white,),
                    // ),
                    // enabledBorder: OutlineInputBorder(
                    //   borderSide: BorderSide(color: Colors.white,),
                    //   borderRadius: BorderRadius.circular(5.0),
                    // ),
                    hintText: "Search",
                    suffixIcon: Icon(
                      Icons.search,
                      color: primary,
                    ),
                    hintStyle: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                    ),
                  ),
                  maxLines: 1,
                  controller: _searchControl,
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    _showFiltterDialog();
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 5.h),
                    color: Colors.white,
                    width: 30.h,
                    height: 20.h,
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/filter_icon.png",
                            width: 10.h,
                            height: 10.h,
                          ),
                          SizedBox(
                            width: 5.h,
                          ),
                          Text(
                            "Filter",
                            style: TextStyle(fontSize: 12.sp),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _categoryList.forEach((category) {
      if (category.name.toLowerCase().startsWith(text))
        _searchResult.add(category);
    });

    setState(() {});
  }

  void _showFiltterDialog() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return StatefulBuilder(builder: (context, setState) {
            return Container(
              width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: ListView(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    Container(
                      color: filter_bg,
                      padding: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Expanded(
                            child: Center(
                              child: GestureDetector(
                                onTap: () {},
                                child: Text("Reset",
                                    style: TextStyle(
                                        fontSize: 15.sp,
                                        color: primary))
                              ),
                            ),
                          ),
                          Expanded(
                              child: Center(
                                  child: Text("Filters",
                                      style: TextStyle(
                                          fontSize: 17.sp,
                                          color: Colors.black)))),
                          Expanded(
                              child: Center(
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text("Close",
                                        style: TextStyle(
                                          fontSize: 15.sp,
                                          color: primary,
                                        )),
                                  ))),
                        ],
                      ),
                    ),
                    Container(
                        width: double.infinity,
                        color: filter_titile__bg,
                        padding: EdgeInsets.all(10),
                        child: Text(_filterList[0].name,
                            style: TextStyle(
                                fontSize: 10.sp, color: Colors.black87))),
                    ListView(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      children:
                      _filterList[0].options.map((FilterOptions key) {
                        return _filterList[0].type == "0"
                            ? RadioListTile(
                          title: Text(key.name),
                          value: key.id,
                          onChanged: (val) {
                            print(val);
                            for (FilterOptions sItem
                            in _filterList[0].options) {
                              key.isChecked = false;
                            }
                            key.isChecked = true;
                            setState(() {
                              selectedQuickFilterRadioTile = val;
                              print(selectedQuickFilterRadioTile);
                              filterSelectedId = key.id;
                            });
                          },
                          groupValue: selectedQuickFilterRadioTile,
                        )
                            : CheckboxListTile(
                          title: Text(key.name),
                          value: key.isChecked != null
                              ? key.isChecked
                              : false,
                          onChanged: (bool value) {
                            setState(() {
                              key.isChecked = value;
                              filterSelectedId = key.id;
                            });
                          },
                        );
                      }).toList(),
                    ),
                    Container(
                        width: double.infinity,
                        color: filter_titile__bg,
                        padding: EdgeInsets.all(15),
                        child: Text(_filterList[1].name,
                            style: TextStyle(
                                fontSize: 10.sp, color: Colors.black87))),
                    ListView(
                      padding: EdgeInsets.only(top: 10),
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      children:
                      _filterList[1].options.map((FilterOptions key) {
                        return Row(
                          children: [
                            Expanded(
                                child: _filterList[1].type == "0"
                                    ? RadioListTile(
                                  title: Text(key.name),
                                  value: key.id,
                                  onChanged: (val) {
                                    for (FilterOptions sItem
                                    in _filterList[1].options) {
                                      key.isChecked = false;
                                    }
                                    key.isChecked = true;
                                    setState(() {
                                      selectedMenuFilterRadioTile = val;
                                      print(
                                          selectedMenuFilterRadioTile);
                                    });
                                  },
                                  groupValue:
                                  selectedMenuFilterRadioTile,
                                )
                                    : CheckboxListTile(
                                  title: Text(key.name),
                                  value: key.isChecked != null
                                      ? key.isChecked
                                      : false,
                                  onChanged: (bool value) {
                                    setState(() {
                                      key.isChecked = value;
                                    });
                                  },
                                )),
                          ],
                        );
                      }).toList(),
                    ),
                    Container(
                        width: double.infinity,
                        color: filter_titile__bg,
                        padding: EdgeInsets.all(15),
                        child: Text("Sort By",
                            style: TextStyle(
                                fontSize: 10.sp, color: Colors.black87))),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Icon(Icons.arrow_upward, size: 30),
                              Text(
                                "Sort",
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            child: Column(
                              children: [
                                Icon(Icons.arrow_upward, size: 30),
                                Text(
                                  "Sort",
                                ),
                              ],
                            )),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 20, 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Icon(Icons.arrow_upward, size: 30),
                              Text(
                                "Rating",
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    Divider(
                      color: Colors.grey[300],
                      height: 1,
                    ),
                    SizedBox(height: 5.h),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.all(10),
                        onPressed: () {
                          applyFilter(filterSelectedId);
                        },
                        color: primary,
                        textColor: Colors.white,
                        child:
                        Text("APPLY", style: TextStyle(fontSize: 20.sp)),
                      ),
                    ),
                  ],
                ));
          });
        });
  }

  void applyFilter([int filterSelectedId]) {
    print("filterSelectedId.."+filterSelectedId.toString());


  }
}
