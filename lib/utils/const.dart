import 'package:restaurant/utils/strings.dart';

class Constants {
  static String APP_NAME = "Restaurant";

  static String WELCOME_ONE_TITLE = "Eat from the best Restaurant";
  static String WELCOME_ONE_SUBTITLE =
      "Access all the best restaurants arround you and enjoy their cuisine at home";
  static String CONTINUE = "Continue";
  static String APP_TOKEN = "token";

  static var CANCEL = "Cancel";
  static var ADD_ITEM = "Add Item";
  static var Save = "Save";
  static var select_star = "Select Star";
  static var CONFIRM_DETAIL = "Conform Detail";
  static var DETAIL_PROFILE_IMAGE = "Profile Image";
  static var MOBILE = "Mobile";
  static var NAME = "Name";
  static var EMAIL = "Email";
  static var CONFORM = "Confirm";
  static var MOBILE_OR_EMAIL = "Mobile or Email";
  static var PASSWORD = "Password";
  static var RESTORE = "Restore";
  static var LOGIN = "Login";
  static var WELCOME_RESTAURENT = "Welcome to Restaurant";
  static var FORGOT_PASSWORD = "Forgot Password?";
  static var INTERNET_CHECK = "Your Internet is Connected";
  static var VERIFY_EMAIL = "Verify Email";
  static var CURRENCY = "Currency";

  //Base Url
  static const String BASE_URL = "http://food.ittanta.net/api/";

// Category
  static var CATEGORY_URL = "categories";

  // product
  static var PRODUCT_URL = "products";

  // Login
  static var LOGIN_MOBILE_EMAIL = "mobile_email";
  static var LOGIN_PASSWORD = "password";
  static var LOGIN_URL = "login";
  static var LOGOUT = 'Log out';

  // Social Login
  static var PROVIDER = "";
  static var PROVIDER_ID = "provider_id";
  static const String DISPLAY_NAME = "name";
  static const String DISPLAY_EMAIL = "email";
  static var PHOTO_URL = "profile";

  //  SignUP
  static var USER_NAME;
  static var SIGNUP = "SignUp";
  static var SIGNUP_URL = "register";
  static var SIGNUP_CONTINUE = "Please Signup to continue";

  //ForgotPassword
  static var FORGOT_PASSWORD_URL = "forgot-password";

  //Social Login
  static const String SOCIAL_LOGIN = "social-login";
  static const String CONFIRM_DETAIL_URL = "confirm-details";

  //User Data
  static const String USER_URL = "me";
  static const String USER_DATA = "userData";

  // Change Password
  static const String CHANGE_PW_URL = "me/change-password";

  //Add To Cart
  static const String ADDTOCART_URL = "cart/add";

  //get To Cart
  static const String GETTOCART_URL = "cart";

  //update cart product
  static const String UPDATE_CART_URL = "/update";

  //add Address
  static const String ADD_ADDRESS_URL = "me/addresses/add";
  static const String ADDRESS_DATA = "addressData";

  //get Address
  static const String GET_ADDRESS_URL = "me/addresses/";

  //CheckTime
  static const String CHECK_TIME_URL = "checkout/check-time";

  //promo code
  static const String PROMOCODE_URL = "cart/apply-discount-code";

  //getCurrencySign
  static const String CURRENCY_URL = "get-settings";

  //getOder
  static const String GET_ORDER_URL = "me/orders";

  // addReview
  static const String ADD_REVIEW_URL = "review/add";
  // ContactUs
  static const String ContactUs_URL = "contactus";


  // Gallary
  static const String GALLARY_IMAGE_URL = "gallery";

  // Verify
  static var VERIFY = "Verify";
  static var VERIFY_TITLE =
      "Enter 4 digit code we sent you via email to continue";

  static var COTTER_API_KEY = "e99add85-b397-46f7-9856-77dfe85a4960";
  static var COTTER_SECREAT_KEY = "ZxnbSvFRzNn12q1uUxqN";
}
