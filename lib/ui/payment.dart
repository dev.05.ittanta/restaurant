import 'package:flutter/material.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/sub_category_itemlist.dart';
import 'package:restaurant/ui/thank_you.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/oval_shadow.dart';

class PaymentScreen extends StatefulWidget {


  @override
  State<StatefulWidget> createState() {
    return PaymentScreenState();
  }
}

class PaymentScreenState extends Base<PaymentScreen> {
  final _repository = Repository();
  int cartCount = 0;
  @override
  void initState() {
    super.initState();
    getCartCount();
  }
  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Padding(
          padding: const EdgeInsets.all(14.0),
          child: InkWell(
            child: Image.asset(
              "assets/images/back_arrow.png",
              color: Colors.black,
              fit: BoxFit.contain,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        title:Center(
          child: Text(
           "Payment",
            textAlign: TextAlign.center,
            style: TextStyle(color: sky_blue, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Stack(children: [
        Text("Paymet"),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ButtonTheme(
                height: 50,
                minWidth: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  onPressed: () {
                    push(ThankYou());
                  },
                  color: dark_red,
                  textColor: Colors.white,
                  child: Text("Continue", style: TextStyle(fontSize: 18)),
                ),
              ),
            ],
          ),
        ),
      ],
      ),
    );
  }
}
