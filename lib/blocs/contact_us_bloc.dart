import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/contact_us.dart';
import 'package:restaurant/models/gallary_model.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class ContactUsBloc {
  final _repository = Repository();
  final _contactFetcher = PublishSubject<ContactUsModel>();
  Stream<ContactUsModel> get allContact => _contactFetcher.stream;

  fetchContact() async {
    ContactUsModel contactUsModel = await _repository.getContactData();
    _contactFetcher.sink.add(contactUsModel);
  }

}

final contactBlock = ContactUsBloc();