import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/add_reservation.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/kf_drawer.dart';

class Reservations extends KFDrawerContent {
  @override
  State<StatefulWidget> createState() {
    return ReservationsState();
  }
}

class ReservationsState extends Base<Reservations> {
  final TextEditingController _searchControl = new TextEditingController();
  List<String> _searchResult = [];
  final _repository = Repository();
  int cartCount = 0;
  List<bool> isExpand = [];

  @override
  void initState() {
    super.initState();
    getCartCount();
  }

  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Image.asset(
                "assets/images/menu.png",
                color: Colors.black,
                fit: BoxFit.contain,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "Reservation Listing",
            textAlign: TextAlign.center,
            style: TextStyle(color: sky_blue, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            searchBar(),
            itemUi(),
            Expanded(
              child: Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: FloatingActionButton(
                      onPressed: () {
                        push(AddReservation());
                      },
                      child: Icon(Icons.add),
                      backgroundColor: Colors.blue,
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }


  searchBar() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(
              Radius.circular(0.0),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: TextField(
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.white,
                  ),
                  onChanged: onSearchTextChanged,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20.0),
                    // border: OutlineInputBorder(
                    //   borderRadius: BorderRadius.circular(5.0),
                    //   borderSide: BorderSide(color: Colors.white,),
                    // ),
                    // enabledBorder: OutlineInputBorder(
                    //   borderSide: BorderSide(color: Colors.white,),
                    //   borderRadius: BorderRadius.circular(5.0),
                    // ),
                    hintText: "Search",
                    suffixIcon: Icon(
                      Icons.search,
                      color: primary,
                    ),
                    hintStyle: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                    ),
                  ),
                  maxLines: 1,
                  controller: _searchControl,
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(right: 5.h),
                  color: Colors.white,
                  width: 30.h,
                  height: 20.h,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/images/filter_icon.png",
                          width: 10.h,
                          height: 10.h,
                        ),
                        SizedBox(
                          width: 5.h,
                        ),
                        Text(
                          "Filter",
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    // _subCategoryList.forEach((subCategory) {
    //   if (subCategory.name.toLowerCase().startsWith(text))
    //     _searchResult.add(subCategory);
    // });

    setState(() {});
  }

  Widget itemUi() {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Stack(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 5,
            margin: EdgeInsets.only(left: 25, right: 25),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text("Table book : 20-2-2021",
                          style: TextStyle(color: sky_blue, fontSize: 15.h)),
                      Expanded(
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            onTap: () {
                              // setState(() {
                              //   isExpand[index] = !isExpand[index];
                              // });
                            },
                            child: Image.asset(
                              'assets/images/reservation_up.png',
                              width: 35,
                              height: 35,
                              alignment: Alignment.centerRight,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Text("Date",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.h)),
                          ),
                          Expanded(
                              child: Text("20-2-2021",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 11.h)))
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Text("Time",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.h)),
                          ),
                          Expanded(
                              child: Text("10pm",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 11.h)))
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Text("Peoples",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.h)),
                          ),
                          Expanded(
                              child: Text("10",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 11.h)))
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Text("Name",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.h)),
                          ),
                          Expanded(
                              child: Text("Kirti",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 11.h)))
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Text("Email",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.h)),
                          ),
                          Expanded(
                              child: Text("kirti@gmail.com",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 11.h)))
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Expanded(
                            child: Text("Phone",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.h)),
                          ),
                          Expanded(
                              child: Text("111111111",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                      color: Colors.grey, fontSize: 11.h)))
                        ],
                      ),

                    ],
                  )
                ],
              ),
            ),
          ),
          Positioned.fill(
              right: 10,
              bottom: 30,
              child: Align(
                alignment: Alignment.centerRight,
                child: Image.asset(
                  'assets/images/reservation_down.png',
                  width: 35,
                  height: 35,
                  fit: BoxFit.cover,
                ),
              )),
        ],
      ),
    );
  }
}
