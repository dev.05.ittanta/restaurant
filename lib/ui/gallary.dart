import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:restaurant/blocs/gallary_bloc.dart';
import 'package:restaurant/models/gallary_model.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/category.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class Gallary extends KFDrawerContent  {
  @override
  State<StatefulWidget> createState() {
    return GallaryState();
  }
}
class GallaryState extends Base<Gallary> {
  @override
  Widget build(BuildContext context) {
    gallaryBlock.fetchGallaryImages();
    List<String>_gallaryImageList;
    return Scaffold(
      appBar: AppBar(
        backgroundColor:primary,
        leading: IconButton(
          icon: Image.asset(
            "assets/images/menu.png",
            color: Colors.white,
            fit: BoxFit.contain,
            width: 30.w,
          ),
          onPressed: widget.onMenuPressed,
        ),
        title: Center(
          child: Text(
            "Gallary",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {
              push(CategoryList());
            },
            child: Image.asset("assets/images/home_food_menu.png",
                color: Colors.white,
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                //   child: Text(
                //     cartCount.toString(),
                //     textAlign: TextAlign.center,
                //     style: TextStyle(color: Colors.white, fontSize: 10.h),
                //   ),
                // ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      color: Colors.white,
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),

      body:StreamBuilder(
        stream: gallaryBlock.allGallaryImage,
        builder: (context, AsyncSnapshot<GallaryModel> snapshot) {
          if (snapshot.hasData) {
            _gallaryImageList = snapshot.data.data != null
                ? snapshot.data.data
                : null;
            return Padding(
              padding: const EdgeInsets.all(4),
              child: StaggeredGridView.countBuilder(
                crossAxisCount: 4,
                itemCount: _gallaryImageList.length,
                itemBuilder: (BuildContext context, int index) => Container(
                    color: Colors.black,
                    child: Center(
                      child: Image.network(_gallaryImageList[index],fit: BoxFit.fill,)
                    )),
                staggeredTileBuilder: (int index) =>
                    StaggeredTile.count(2, index.isEven ? 2 : 1),
                mainAxisSpacing: 2,
                crossAxisSpacing: 2,
              ),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}


