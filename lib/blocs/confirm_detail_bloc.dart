import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class ConfirmDetailBloc {
  final _repository = Repository();

  Future<Map<String, dynamic>> doConfirmDetail(String name, String email,String phone,String provider,String providerId) async {
    Map<String, dynamic> confirmItem = await _repository.checkConfirmDetail(name, email, phone,provider,providerId);
    return confirmItem;
  }
}

final bloc = ConfirmDetailBloc();