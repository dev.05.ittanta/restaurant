import 'package:flutter/material.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/drawer_list.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../blocs/forgot_pw.dart';

class ForgotPassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ForgotPasswordState();
  }
}

class _ForgotPasswordState extends Base<ForgotPassword> {
  final email_mobileController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                            AssetImage('assets/images/login_half_bg.png'),
                            fit: BoxFit.fill)),
                  )),
            ],
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                SizedBox(height: 80.h),
                Image.asset(
                  'assets/images/login_logo.png',
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 50.h),
                Text(
                  Constants.WELCOME_RESTAURENT,
                  style: TextStyle(fontSize: 17.sp),
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 20,
                  margin: EdgeInsets.fromLTRB(30, 20, 30, 0),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          Constants.FORGOT_PASSWORD,
                          style: TextStyle(fontSize: 23.sp),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(
                          height: 15.h,
                        ),
                        TextField(
                          controller: email_mobileController,
                          decoration: new InputDecoration(
                            hintText: Constants.MOBILE_OR_EMAIL,
                            hintStyle: TextStyle(color: hint_clr),
                            isDense: true,
                            enabledBorder: const OutlineInputBorder(
                              borderSide:
                              const BorderSide(color: Colors.grey),
                            ),
                            border: const OutlineInputBorder(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                ButtonTheme(
                  height: 40.h,
                  minWidth: 290.h,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(color: Colors.white)),
                    onPressed: () {
                      if (isValid()) {
                        _getForgotPwDetail();
                      }
                    },
                    color: Colors.white,
                    textColor: sky_blue,
                    child:
                    Text(Constants.RESTORE, style: TextStyle(fontSize: 18.sp)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  bool isValid() {
    if (email_mobileController.text.isEmpty) {
      showSnackBar('Please enter mobile or email');
      return false;
    }
    return true;
  }
  Future<void> _getForgotPwDetail() async {
    Map<String, dynamic> res = await bloc.doforgotPwDetail(
        email_mobileController.text,);
    bool success = res["success"];
    if (success) {
      print("success");
      pushReplacement(DrawerList());
    } else {
      showSnackBar(res["data"]["error"]);
    }
  }
}
