import 'package:restaurant/resources/repository.dart';

class SignUpBloc {
  final _repository = Repository();

  Future<Map<String, dynamic>> doSignUp(String name, String email,String phone,String password,String confirmPw) async {
    Map<String, dynamic> signUpItem = await _repository.checkSignUp( name,  email, phone, password, confirmPw);
    return signUpItem;
  }
  

}

final bloc = SignUpBloc();