class UserData {
  bool success;
  Data data;
  String message;

  UserData({this.success, this.data, this.message});

  UserData.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  String name;
  String email;
  String image;
  String phone;
  String aboutMe;
  String dateOfBirth;
  String dateOfAnniversary;

  Data(
      {this.name,
        this.email,
        this.image,
        this.phone,
        this.aboutMe,
        this.dateOfBirth,
        this.dateOfAnniversary});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    image = json['image'];
    phone = json['phone'];
    aboutMe = json['about_me'];
    dateOfBirth = json['date_of_birth'];
    dateOfAnniversary = json['date_of_anniversary'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['image'] = this.image;
    data['phone'] = this.phone;
    data['about_me'] = this.aboutMe;
    data['date_of_birth'] = this.dateOfBirth;
    data['date_of_anniversary'] = this.dateOfAnniversary;
    return data;
  }
}
