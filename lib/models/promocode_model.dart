class PromoCodeModel {
  bool success;
  PromoCodeData data;
  String message;

  PromoCodeModel({this.success, this.data, this.message});

  PromoCodeModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new PromoCodeData.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class PromoCodeData {
  int applied;
  int total;
  int discountPrice;
  int totalPrice;
  String couponCode;
  int cid;
  int taxValue;

  PromoCodeData(
      {this.applied,
        this.total,
        this.discountPrice,
        this.totalPrice,
        this.couponCode,
        this.cid,
        this.taxValue});

  PromoCodeData.fromJson(Map<String, dynamic> json) {
    applied = json['applied'];
    total = json['total'];
    discountPrice = json['discount_price'];
    totalPrice = json['total_price'];
    couponCode = json['coupon_code'];
    cid = json['cid'];
    taxValue = json['tax_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['applied'] = this.applied;
    data['total'] = this.total;
    data['discount_price'] = this.discountPrice;
    data['total_price'] = this.totalPrice;
    data['coupon_code'] = this.couponCode;
    data['cid'] = this.cid;
    data['tax_value'] = this.taxValue;
    return data;
  }
}
