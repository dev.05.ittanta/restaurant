import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/componet/custom_radio_button.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/order_type.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/dot_seperater.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../blocs/product_bloc.dart';

class AddToCart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddToCartState();
  }
}

class AddToCartState extends Base<AddToCart> {
  final couponController = TextEditingController();
  List<CartItems> _cartList = [];
  List<Coupons> _couponsList = [];

  CartData cartData;
  PromoCodeModel promoCodeModel;
  final _repository = Repository();
  int cartCount = 0;
  String selectedCouponRadioTile = "";
  String currency;

  @override
  void initState() {
    super.initState();
    getCartCount();
    getCurrency();
  }

  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    bloc.fetchCartProducts();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Padding(
          padding: const EdgeInsets.all(14.0),
          child: InkWell(
            child: Image.asset(
              "assets/images/back_arrow.png",
              color: Colors.black,
              fit: BoxFit.contain,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Center(
          child: Text(
            "Cart",
            textAlign: TextAlign.center,
            style: TextStyle(color: sky_blue, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/welcome_bg.png'),
                    fit: BoxFit.fill)),
          ),
          StreamBuilder(
            stream: bloc.allCartProducts,
            builder: (context, AsyncSnapshot<CartModel> snapshot) {
              if (snapshot.hasData) {
                cartData = snapshot.data.data;
                _cartList =
                    snapshot.data != null ? snapshot.data.data.items : null;
                _couponsList = snapshot.data.data.coupons;
                return cartListData();
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: double.infinity,
              height: 50,
              child: RaisedButton(
                onPressed: () {
                  push(OrderType());
                },
                color: dark_red,
                textColor: Colors.white,
                child: Text("Continue", style: TextStyle(fontSize: 15)),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget cartListData() {
    return ListView(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      padding: EdgeInsets.only(bottom: 60),
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
          elevation: 5,
          margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
          child: ListView.builder(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemCount: _cartList.length,
              itemBuilder: (BuildContext context, int index) {
                return itemUi(index);
              }),
        ),
        Container(
          height: 80.h,
          margin: EdgeInsets.fromLTRB(20, 0, 20, 5),
          decoration: BoxDecoration(
            color: sky_blue,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(
              width: 1,
              color: sky_blue,
              style: BorderStyle.solid,
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: Card(
                  elevation: 5,
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  shape: RoundedRectangleBorder(
                      side: new BorderSide(color: Colors.white, width: 2.0),
                      borderRadius: BorderRadius.circular(4.0)),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Expanded(
                          child: TextField(
                            controller: couponController,
                            decoration: InputDecoration(
                                hintText: "Promo Code",
                                hintStyle: TextStyle(color: Colors.grey),
                                isDense: true,
                                border: InputBorder.none),
                          ),
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: sky_blue)),
                            onPressed: () {
                              applyPromoCode(couponController.text);
                            },
                            color: sky_blue,
                            textColor: Colors.white,
                            child: Text("Apply".toUpperCase(),
                                style: TextStyle(fontSize: 14)),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  appCouponDialog(
                    context,
                  );
                },
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text("View Application Coupons",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 12.h)),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Card(
          elevation: 5,
          margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
          child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: _cartList != null
                  ? calculateUi()
                  : CircularProgressIndicator()),
        ),
      ],
    );
  }

  Widget itemUi(int index) {
    var item = _cartList[index];
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: 60.w,
                    height: 60.h,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: item.product.image != null
                              ? NetworkImage(item.product.image)
                              : AssetImage("assets/images/profile.png"),
                          fit: BoxFit.fill),
                    ),
                  ),
                  Expanded(
                      child: Column(
                    children: [
                      Row(
                        children: [
                          Text(item.name,
                              style: TextStyle(
                                  color: Colors.black, fontSize: 12.h)),
                        ],
                      ),
                      SizedBox(height: 5.h),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top:5.0),
                            child: Text(currency!=null?currency:'',
                                style:
                                TextStyle(color: dark_red, fontSize: 12.h)),
                          ),
                          Text(item.product.price,
                              style:
                                  TextStyle(color: dark_red, fontSize: 15.h)),
                          SizedBox(
                            width: 5.w,
                          ),
                          Text("x" + item.quantity.toString(),
                              style: TextStyle(
                                  color: Colors.black, fontSize: 12.h)),
                        ],
                      ),
                    ],
                  )),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 10, 10),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    ClipOval(
                      child: Material(
                        color: primary, // button color
                        child: InkWell(
                          child: SizedBox(
                              width: 20.w,
                              height: 20.h,
                              child: Icon(
                                Icons.add,
                                color: white,
                                size: 15,
                              )),
                          onTap: () {
                            _cartList[index].quantity =
                                _cartList[index].quantity + 1;
                            updateItemInCart(
                                _cartList[index].quantity, item.id);
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    StreamBuilder<int>(builder: (context, snapshot) {
                      return Text(
                        item.quantity.toString(),
                        style: TextStyle(
                          fontSize: 14.h,
                        ),
                      );
                    }),
                    SizedBox(
                      width: 10,
                    ),
                    ClipOval(
                      child: Material(
                        color: primary, // button color
                        child: InkWell(
                          child: SizedBox(
                              width: 20.w,
                              height: 20.h,
                              child: Icon(
                                Icons.remove,
                                color: white,
                                size: 15,
                              )),
                          onTap: () {
                            setState(() {
                              _cartList[index].quantity =
                                  _cartList[index].quantity - 1;
                              updateItemInCart(
                                  _cartList[index].quantity, item.id);
                            });
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              MySeparator(color: Colors.grey),
            ],
          ),
        ),
      ],
    );
  }

  Widget calculateUi() {
    return cartData != null
        ? Column(
            children: [
              calcuateItemUi("Cart Total",
                  cartData.cartTotal != null && currency!=null? currency+cartData.cartTotal : ""),
              calcuateItemUi("Tax", currency+cartData.taxAmount),
              calcuateItemUi("Delivery",  currency+"0"),
              promoCodeModel != null
                  ? calcuateItemUi("Promo Discount",
                   currency+promoCodeModel.data.discountPrice.toString())
                  : SizedBox(),
              Divider(),
              promoCodeModel != null
                  ? Text(
                      "Total :  " + currency+promoCodeModel.data.totalPrice.toString(),
                      style: TextStyle(color: Colors.black, fontSize: 15))
                  : SizedBox(),
              ButtonTheme(
                height: 40,
                minWidth: 100,
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                      side: BorderSide(color: dark_red)),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  color: dark_red,
                  textColor: Colors.white,
                  child: Text("ADD MORE ITEMS", style: TextStyle(fontSize: 15)),
                ),
              ),
            ],
          )
        : SizedBox();
  }

  calcuateItemUi(String title, String data) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
              flex: 1,
              child: Text(title,
                  style: TextStyle(color: Colors.black, fontSize: 15))),
          Text(data, style: TextStyle(color: Colors.black, fontSize: 15))
        ],
      ),
    );
  }

  Future<void> updateItemInCart(int quntity, int id) async {
    showLoading();
    dynamic res = await bloc.updateToCartData(quntity, id);
    print(res);
    hideLoading();
    if (res != null && res["success"]) {
      setState(() {});
      showSnackBar("Item Updated in Cart");
    } else {
      showSnackBar("Items not updated");
    }
  }

  Future<void> applyPromoCode(String code) async {
    promoCodeModel = await bloc.applyPromoCode(
      code,
    );
    setState(() {});
  }

  Future<void> appCouponDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Coupon Discount",
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.zero,
          content: displayAppCouponUi(_couponsList),
          actions: [
            RaisedButton(
              onLongPress: () {
                Navigator.pop(context);
              },
              color: Colors.white,
              elevation: 0,
              textColor: Colors.red,
              onPressed: () => Navigator.pop(context),
              child: Text("Cancel"),
            ),
            RaisedButton(
              color: green_clr,
              textColor: Colors.white,
              onPressed: () {
                for (Coupons sItem in cartData.coupons) {
                  if (sItem.isChecked != null && sItem.isChecked) {
                    applyPromoCode(selectedCouponRadioTile);
                  }
                }
                Navigator.pop(context);

              },
              child: Text("Apply"),
            ),
          ],
        );
      },
    );
  }

  StatefulBuilder displayAppCouponUi(List<Coupons> _couponsList) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Container(
          width: double.maxFinite,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Divider(
                height: 20,
                color: Colors.grey[300],
              ),
              ListView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemCount: _couponsList.length,
                  itemBuilder: (BuildContext context, int index) {
                    Coupons couponsItem = _couponsList[index];
                    return Padding(
                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: CustomRadioWidget(
                        title: couponsItem.name,
                        description: couponsItem.description,
                        value: couponsItem.code,
                        groupValue: selectedCouponRadioTile,
                        onChanged: (String value) {
                          for (Coupons sItem in cartData.coupons) {
                            couponsItem.isChecked = false;
                          }
                          couponsItem.isChecked = true;
                          setState(() {
                            selectedCouponRadioTile = value;
                            print(selectedCouponRadioTile);
                          });
                        },
                      ),
                    );
                  }),
              Divider(
                height: 20,
                color: Colors.grey[300],
              ),
            ],
          ),
        );
      },
    );
  }
  Future<void> getCurrency() async {
    prefs = await SharedPreferences.getInstance();
    currency = prefs.getString(Constants.CURRENCY);
  }
}

