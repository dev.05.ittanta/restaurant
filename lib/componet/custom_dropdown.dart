import 'package:flutter/material.dart';
class CustomDropdown<T> extends StatelessWidget {
  final List<DropdownMenuItem<T>> dropdownMenuItemList;
  final ValueChanged<T> onChanged;
  final T value;
  final bool isEnabled;
  CustomDropdown({
    Key key,
    @required this.dropdownMenuItemList,
    @required this.onChanged,
    @required this.value,
    this.isEnabled = true,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !isEnabled,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Container(
          height: 30,
          padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(2.0)),
              border: Border.all(
                color: Colors.grey,
                width: 1,
              ),
              color: isEnabled ? Colors.white : Colors.grey),
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              isDense: true,
              isExpanded: true,
              itemHeight: 50.0,
              style: TextStyle(
                  fontSize: 15.0,
                  color: isEnabled ? Colors.black : Colors.grey[700]),
              items: dropdownMenuItemList,
              onChanged: onChanged,
              value: value,
            ),
          ),
        ),
      ),
    );
  }
}