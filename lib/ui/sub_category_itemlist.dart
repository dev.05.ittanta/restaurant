import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/models/category_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/oval_shadow.dart';
import 'package:restaurant/utils/start_rating.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../blocs/product_bloc.dart';

class SubCatItem extends StatefulWidget {
  final Childs subCatItem;
  final List<Filters> filterLists;

  const SubCatItem({Key key, this.subCatItem, this.filterLists})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SubCatItemState();
  }
}

enum ExtraMenu { CheeseWithButter, ExtraCheese }

class SubCatItemState extends Base<SubCatItem> {
  Childs subCatData;

  List<bool> isExpand = [];
  List<ProductData> _productList = [];
  List<ProductData> _searchResult = [];
  List<ProductData> searchfilterLists = [];
  List<Filters> _filterList = [];
  Map<ProductData, bool> filterValues = new Map();
  int selectedRadioTile = 0;
  List<int> _counter = List();
  final TextEditingController _searchControl = new TextEditingController();
  StreamController _event = StreamController<int>.broadcast();
  int productCount = 0;
  String currency;
  final List<Function> sendVariantData = [];

  int selectedQuickFilterRadioTile = 0;
  int selectedMenuFilterRadioTile = 0;
  bool isPriceAsc;
  bool isRatingFilter;

  @override
  void initState() {
    super.initState();
    subCatData = widget.subCatItem;
    _filterList = widget.filterLists;
    getCurrency();
    Future.delayed(Duration.zero, () {
      getProduct();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Padding(
          padding: const EdgeInsets.all(14.0),
          child: InkWell(
            child: Image.asset(
              "assets/images/back_arrow.png",
              color: Colors.black,
              fit: BoxFit.contain,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Center(
          child: Text(
            subCatData.name,
            textAlign: TextAlign.center,
            style: TextStyle(color: sky_blue, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    productCount != null ? productCount.toString() : "0",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Column(
        children: [
          searchBar(),
          Expanded(
            flex: 1,
            child: Container(
                width: double.infinity,
                color: Colors.white,
                child: _productList != null && _productList.length > 0
                    ? buildList()
                    : Center(child: CircularProgressIndicator())),
          ),
        ],
      ),
    );
  }

  List<ProductData> _searchProductList = [];

  Widget buildList() {
    if (_searchResult != null && _searchResult.length > 0) {
      _searchProductList = _searchResult;
    } else {
      _searchProductList = _productList;
    }
    return Container(
        decoration: BoxDecoration(color: category_bg),
        child: Center(
          child: _searchProductList != null
              ? ListView.builder(
                  itemCount: _searchProductList.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (_counter.length < _searchProductList.length) {
                      _counter.add(0);
                    }
                    return itemList(index);
                  })
              : CircularProgressIndicator(),
        ));
  }

  itemList(int index) {
    var item = _searchProductList[index];
    double rating = 3.5;
    List<Variants> _variantsList = item.variants;
    return Container(
      padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
      child: Stack(
        children: [
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
            ),
            elevation: 5,
            margin: EdgeInsets.only(left: 25, right: 25, bottom: 10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(60, 20, 0, 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(item.title,
                                style: TextStyle(
                                  color: isExpand[index] ? primary : sky_blue,
                                  fontSize: 17.h,
                                )),
                          ),
                          // SizedBox(width: 20),

                          Padding(
                            padding: const EdgeInsets.only(right: 50.0),
                            child: Container(
                              width: 20.w,
                              height: 20.h,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                image: DecorationImage(
                                    image: item.typeImage != null
                                        ? NetworkImage(item.typeImage)
                                        : AssetImage(
                                            "assets/images/no_img.png"),
                                    fit: BoxFit.fill),
                              ),
                            ),
                          ),
                          // SizedBox(width: 20),
                          // Padding(
                          //   padding: const EdgeInsets.only(right: 20.0),
                          //   child:
                          //   ClipOvalShadow(
                          //     shadow: Shadow(
                          //       color: Colors.white,
                          //       offset: Offset(1.0, 1.0),
                          //       blurRadius: 2,
                          //     ),
                          //     clipper: CustomClipperOval(),
                          //     child: ClipOval(
                          //       child: Material(
                          //         color: category_bg, // button color
                          //         child: InkWell(
                          //           child: SizedBox(
                          //               width: 25.w,
                          //               height: 25.h,
                          //               child: Icon(Icons.navigate_next,
                          //                   color: primary)),
                          //           onTap: () {
                          //             setState(() {
                          //               isExpand[index] = !isExpand[index];
                          //             });
                          //           },
                          //         ),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      StarRating(
                          rating: rating,
                          color: isExpand[index] ? primary : yellow_clr,
                          onRatingChanged: (newrating) {
                            setState(() {
                              rating = newrating;
                            });
                          }),
                      SizedBox(
                        height: 15,
                      ),
                      isExpand[index]
                          ? Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Column(
                                  children: [
                                    GestureDetector(
                                        onTap: () {},
                                        child: Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 10, 20),
                                          child: Image.asset(
                                            'assets/images/favourites.png',
                                            width: 20,
                                            height: 20,
                                          ),
                                        )),
                                    GestureDetector(
                                        onTap: () async {
                                          await showDialog(
                                            context: context,
                                            builder: (context) =>
                                                new AlertDialog(
                                                    content: addReviewDialog()),
                                          );
                                        },
                                        child: Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 10, 20),
                                          child: Image.asset(
                                            'assets/images/comment.png',
                                            width: 20,
                                            height: 20,
                                          ),
                                        )),
                                    GestureDetector(
                                        onTap: () async {
                                          if (item != null &&
                                              item.images != null &&
                                              item.images.isNotEmpty) {
                                            var request = await HttpClient()
                                                .getUrl(Uri.parse(
                                                    subCatData.image));
                                            var response =
                                                await request.close();
                                            Uint8List bytes =
                                                await consolidateHttpClientResponseBytes(
                                                    response);
                                            await Share.file(
                                                item.title,
                                                'image.jpg',
                                                bytes,
                                                'image/jpg');
                                          }
                                        },
                                        child: Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 0, 10, 20),
                                          child: Image.asset(
                                            'assets/images/forword.png',
                                            width: 20,
                                            height: 20,
                                          ),
                                        )),
                                  ],
                                ),
                                Expanded(
                                  child: Column(
                                    children: [
                                      Text(
                                        item.description != null
                                            ? item.description
                                            : "No Description",
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            item.preparationTime != null
                                                ? '${item.preparationTime}\nminute'
                                                : "0 \nminute ",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: sky_blue,
                                              fontSize: 10.h,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            "/",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Colors.grey.shade400,
                                              fontSize: 22.h,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            item.calorie != null
                                                ? '${item.calorie}\nnutrition'
                                                : "0 \nnutrition ",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: sky_blue,
                                              fontSize: 10.h,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )
                          : SizedBox(),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              "",
                              style: TextStyle(
                                fontSize: 12.h,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              currency != null ? currency + item.price : "",
                              style: TextStyle(
                                color: primary,
                                fontSize: 12.h,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                if (isExpand[index])
                  Row(
                    children: [
                      // IconButton(
                      //     icon: Icon(
                      //       Icons.navigate_before,
                      //     ),
                      //     iconSize: 20,
                      //     onPressed: () {}),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 20.0),
                          height: MediaQuery.of(context).size.height * 0.10,
                          child: ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            // controller: _scrollController,
                            itemBuilder: (BuildContext ctx, int index) {
                              return Card(
                                  semanticContainer: true,
                                  clipBehavior: Clip.antiAliasWithSaveLayer,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(9.0),
                                  ),
                                  child: Image.network(
                                    item.images[index],
                                    width: 60.w,
                                    height: 60.h,
                                    fit: BoxFit.fill,
                                  ));
                            },
                            itemCount: item.images.length,
                          ),
                        ),
                      ),
                      // IconButton(
                      //     icon: Icon(
                      //       Icons.navigate_next,
                      //     ),
                      //     iconSize: 20,
                      //     onPressed: () {
                      //       _scrollController.animateTo(i * 60, duration: new Duration(seconds: 2), curve: Curves.ease);
                      //     }),
                    ],
                  )
                else
                  SizedBox(),
              ],
            ),
          ),
          Positioned(
              right: 6,
              top: 30,
              child: Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  child: Image.asset(
                    'assets/images/arrow_shadow.png',
                    width: 35,
                    height: 35,
                    fit: BoxFit.cover,
                  ),
                  onTap: () {
                    setState(() {
                      isExpand[index] = !isExpand[index];
                    });
                  },
                ),
              )),
          Positioned(
            right: 20,
            bottom: 0,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 10, 0),
              child: Row(
                children: [
                  ClipOval(
                    child: Material(
                      color: primary, // button color
                      child: InkWell(
                        child: SizedBox(
                            width: 20.w,
                            height: 20.h,
                            child: Icon(
                              Icons.add,
                              color: white,
                              size: 15,
                            )),
                        onTap: () {
                          setState(() {
                            _incrementCounter(index);
                          });
                          Map<String, dynamic> map = new Map();
                          map["id"] = item.id;
                          map["quantity"] = _counter[index];

                          if (_variantsList != null &&
                              _variantsList.length > 0) {
                            showMyDialog(
                                context, _variantsList, item.title, map);
                          } else {
                            map["variants"] = [];
                            print(jsonEncode(map));
                            addItemInCart(map);
                          }
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  StreamBuilder<int>(
                      stream: _event.stream,
                      builder: (context, snapshot) {
                        return Text(
                          _counter[index].toString(),
                          style: TextStyle(
                            fontSize: 14.h,
                          ),
                        );
                      }),
                  SizedBox(
                    width: 10,
                  ),
                  ClipOval(
                    child: Material(
                      color: primary, // button color
                      child: InkWell(
                        child: SizedBox(
                            width: 20.w,
                            height: 20.h,
                            child: Icon(
                              Icons.remove,
                              color: white,
                              size: 15,
                            )),
                        onTap: () {
                          setState(() {
                            _decrementCounter(index);
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: 50.w,
            height: 50.h,
            margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
            child: PhysicalModel(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              color: Colors.grey,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(10),
              child: FadeInImage.assetNetwork(
                placeholder: 'assets/images/no_img.png',
                image: subCatData.image,
                fit: BoxFit.contain,
              ),
            ),
          ),
        ],
      ),
    );
  }

  searchBar() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.all(
          Radius.circular(0.0),
        ),
      ),
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: TextField(
              style: TextStyle(
                fontSize: 15.0,
                color: Colors.white,
              ),
              onChanged: onSearchTextChanged,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.all(20.0),
                // border: OutlineInputBorder(
                //   borderRadius: BorderRadius.circular(5.0),
                //   borderSide: BorderSide(color: Colors.white,),
                // ),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.white,),
                //   borderRadius: BorderRadius.circular(5.0),
                // ),
                hintText: "Search",
                suffixIcon: Icon(
                  Icons.search,
                  color: primary,
                ),
                hintStyle: TextStyle(
                  fontSize: 15.0,
                  color: Colors.white,
                ),
              ),
              maxLines: 1,
              controller: _searchControl,
            ),
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                _showFiltterDialog();
              },
              child: Container(
                margin: EdgeInsets.only(right: 5.h),
                color: Colors.white,
                width: 30.h,
                height: 20.h,
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/filter_icon.png",
                        width: 10.h,
                        height: 10.h,
                      ),
                      SizedBox(
                        width: 5.h,
                      ),
                      Text(
                        "Filter",
                        style: TextStyle(fontSize: 12.sp),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _productList.forEach((productName) {
      if (productName.title.toLowerCase().startsWith(text))
        _searchResult.add(productName);
    });
    for (ProductData data in _productList) {
      isExpand.add(false);
    }

    setState(() {});
  }

  Future<void> showMyDialog(BuildContext context, List<Variants> variantsList,
      String title, Map<String, dynamic> map) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            title,
            textAlign: TextAlign.center,
          ),
          contentPadding: EdgeInsets.zero,
          content: displayVariants(variantsList),
          actions: [
            RaisedButton(
              onLongPress: () {
                Navigator.pop(context);
              },
              color: Colors.white,
              elevation: 0,
              textColor: Colors.red,
              onPressed: () => Navigator.pop(context),
              child: Text("Cancel"),
            ),
            RaisedButton(
              color: green_clr,
              textColor: Colors.white,
              onPressed: () {
                List<dynamic> mapVariantList = new List();
                Map<String, dynamic> variantMap = new Map();

                for (Variants variant in variantsList) {
                  List<int> itemsIds = new List();
                  for (Items item in variant.items) {
                    if (item.isChecked != null && item.isChecked) {
                      variantMap["id"] = variant.id;
                      itemsIds.add(item.id);
                    }
                  }
                  if (itemsIds.length > 0) {
                    variantMap["items"] = itemsIds;
                    mapVariantList.add(variantMap);
                  }
                }

                if (mapVariantList.length > 0) {
                  map["variants"] = mapVariantList;
                  print(jsonEncode(map));
                  addItemInCart(map);
                } else {
                  showSnackBar("Variant List is Empty");
                }
              },
              child: Text("Add Item"),
            ),
          ],
        );
      },
    );
  }

  StatefulBuilder displayVariants(List<Variants> variantsList) {
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        width: double.maxFinite,
        child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: variantsList.length,
          itemBuilder: (BuildContext context, int index) {
            Variants variants = variantsList[index];
            return Padding(
              padding: EdgeInsets.all(10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Divider(
                    height: 20,
                    color: Colors.grey[300],
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(25, 0, 0, 10),
                    child: Text(
                      variants.name,
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ),
                  ListView(
                    shrinkWrap: true,
                    children: variants.items.map((Items item) {
                      return Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: variants.type == 0
                                    ? RadioListTile(
                                        title: Text(item.subTitle),
                                        value: item.id,
                                        onChanged: (val) {
                                          print(val);
                                          for (Items sItem in variants.items) {
                                            item.isChecked = false;
                                          }
                                          item.isChecked = true;

                                          setState(() {
                                            selectedRadioTile = val;
                                            print(selectedRadioTile);
                                          });
                                        },
                                        groupValue: selectedRadioTile,
                                      )
                                    : CheckboxListTile(
                                        title: Text(item.subTitle),
                                        controlAffinity:
                                            ListTileControlAffinity.leading,
                                        value: item.isChecked != null
                                            ? item.isChecked
                                            : false,
                                        onChanged: (bool value) {
                                          setState(() {
                                            item.isChecked = value;
                                          });
                                        },
                                      ),
                              ),
                              Text(currency + item.price ?? '',
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.red),
                                  textAlign: TextAlign.center),
                            ],
                          ),
                          Divider(
                            height: 20,
                            color: Colors.grey[300],
                          ),
                        ],
                      );
                    }).toList(),
                  ),
                ],
              ),
            );
          },
        ),
      );
    });
  }

  _incrementCounter(int i) {
    _counter[i]++;
    _event.add(_counter[i]);
  }

  _decrementCounter(int i) {
    if (_counter[i] <= 0) {
      _counter[i] = 0;
    } else {
      _counter[i]--;
    }
    _event.add(_counter[i]);
  }

  Future<void> addItemInCart(Map<String, dynamic> map) async {
    showLoading();
    dynamic res = await bloc.submitAddToCartData(jsonEncode(map));
    hideLoading();
    Navigator.pop(context);
    print(res);
    if (res != null && res["success"]) {
      setState(() {
        productCount = res["data"]["item_count"];
      });
      showSnackBar("Added to Cart");
    } else {
      showSnackBar("Items not addded");
    }
  }

  Widget addReviewDialog() {
    double reviewRating = 3.5;
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    "Add Review",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.cancel,
                    color: primary,
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Container(
                width: 50.w,
                height: 50.h,
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage("assets/images/no_img.png"),
                      fit: BoxFit.fill),
                ),
              ),
              Expanded(
                  child: Text("Item Name",
                      style: TextStyle(color: Colors.black, fontSize: 12.h))),
            ],
          ),
          Row(
            children: [
              Text("Select Star", style: TextStyle(fontSize: 10)),
              StarRating(
                rating: reviewRating,
                // onRatingChanged: (rating) => setState(() => this.rating = rating),
              ),
              SizedBox(width: 3.h),
              Text("(1)", style: TextStyle(fontSize: 10)),
              Spacer(),
              Text("Like", style: TextStyle(fontSize: 10)),
              SizedBox(
                width: 5.h,
              ),
              Icon(
                Icons.favorite_border,
                color: Colors.grey,
              ),
            ],
          ),
          SizedBox(height: 5.h),
          TextField(
            decoration: InputDecoration(
              hintText: "Comment",
              hintStyle: TextStyle(color: Colors.grey),
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              ),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0),
              ),
              border: const OutlineInputBorder(),
            ),
          ),
          SizedBox(height: 5.h),
          Container(
            width: double.infinity,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: primary)),
              onPressed: () {},
              color: primary,
              textColor: Colors.white,
              child: Text("Submit", style: TextStyle(fontSize: 18)),
            ),
          ),
          SizedBox(height: 5.h),
          InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text("No Thanks", style: TextStyle(fontSize: 15)))
        ],
      ),
    );
  }

  Future<void> getCurrency() async {
    prefs = await SharedPreferences.getInstance();
    currency = prefs.getString(Constants.CURRENCY);
  }

  void _showFiltterDialog() {
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return StatefulBuilder(builder: (context, setState) {
            return Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: ListView(
                  physics: ScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    Container(
                      color: filter_bg,
                      padding: EdgeInsets.all(15),
                      child: Row(
                        children: [
                          Expanded(
                            child: Center(
                              child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      selectedQuickFilterRadioTile = 0;
                                      selectedMenuFilterRadioTile = 0;
                                    });
                                  },
                                  child: Text("Reset",
                                      style: TextStyle(
                                          fontSize: 15.sp, color: primary))),
                            ),
                          ),
                          Expanded(
                              child: Center(
                                  child: Text("Filters",
                                      style: TextStyle(
                                          fontSize: 17.sp,
                                          color: Colors.black)))),
                          Expanded(
                              child: Center(
                                  child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text("Close",
                                style: TextStyle(
                                  fontSize: 15.sp,
                                  color: primary,
                                )),
                          ))),
                        ],
                      ),
                    ),
                    Container(
                        width: double.infinity,
                        color: filter_titile__bg,
                        padding: EdgeInsets.all(10),
                        child: Text(_filterList[0].name,
                            style: TextStyle(
                                fontSize: 10.sp, color: Colors.black87))),
                    ListView(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      children: _filterList[0].options.map((FilterOptions key) {
                        return _filterList[0].type == "0"
                            ? RadioListTile(
                                title: Text(key.name),
                                value: key.id,
                                controlAffinity:
                                    ListTileControlAffinity.trailing,
                                onChanged: (val) {
                                  print(val);
                                  for (FilterOptions sItem
                                      in _filterList[0].options) {
                                    sItem.isChecked = false;
                                  }
                                  key.isChecked = true;
                                  setState(() {
                                    selectedQuickFilterRadioTile = val;
                                    print(selectedQuickFilterRadioTile);
                                  });
                                },
                                groupValue: selectedQuickFilterRadioTile,
                              )
                            : CheckboxListTile(
                                title: Text(key.name),
                                value: key.isChecked != null
                                    ? key.isChecked
                                    : false,
                                onChanged: (bool value) {
                                  setState(() {
                                    key.isChecked = value;
                                  });
                                },
                              );
                      }).toList(),
                    ),
                    Container(
                        width: double.infinity,
                        color: filter_titile__bg,
                        padding: EdgeInsets.all(15),
                        child: Text(_filterList[1].name,
                            style: TextStyle(
                                fontSize: 10.sp, color: Colors.black87))),
                    ListView(
                      padding: EdgeInsets.only(top: 10),
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      children: _filterList[1].options.map((FilterOptions key) {
                        return Row(
                          children: [
                            Expanded(
                                child: _filterList[1].type == "0"
                                    ? RadioListTile(
                                        title: Text(key.name),
                                        value: key.id,
                                        controlAffinity:
                                            ListTileControlAffinity.trailing,
                                        onChanged: (val) {
                                          for (FilterOptions sItem
                                              in _filterList[1].options) {
                                            sItem.isChecked = false;
                                          }
                                          // int index = _filterList[1].options.indexOf(key);
                                          // _filterList[1].options[index].isChecked = true;
                                          key.isChecked = true;
                                          setState(() {
                                            selectedMenuFilterRadioTile = val;
                                            print(selectedMenuFilterRadioTile);
                                          });
                                        },
                                        groupValue: selectedMenuFilterRadioTile,
                                      )
                                    : CheckboxListTile(
                                        title: Text(key.name),
                                        value: key.isChecked != null
                                            ? key.isChecked
                                            : false,
                                        onChanged: (bool value) {
                                          setState(() {
                                            key.isChecked = value;
                                          });
                                        },
                                      )),
                          ],
                        );
                      }).toList(),
                    ),
                    Container(
                        width: double.infinity,
                        color: filter_titile__bg,
                        padding: EdgeInsets.all(15),
                        child: Text("Sort By",
                            style: TextStyle(
                                fontSize: 10.sp, color: Colors.black87))),
                    Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20, 5, 0, 5),
                          child: InkWell(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Icon(Icons.arrow_upward, size: 30),
                                Text(
                                  "Sort",
                                ),
                              ],
                            ),
                            onTap: () {
                              isPriceAsc = true;
                              isRatingFilter = null;
                            },
                          ),
                        ),
                        Expanded(
                            child: InkWell(
                          child: Column(
                            children: [
                              Icon(Icons.arrow_upward, size: 30),
                              Text(
                                "Sort",
                              ),
                            ],
                          ),
                          onTap: () {
                            isPriceAsc = false;
                            isRatingFilter = null;
                          },
                        )),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 5, 20, 5),
                          child: InkWell(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Icon(Icons.arrow_upward, size: 30),
                                Text(
                                  "Rating",
                                ),
                              ],
                            ),
                            onTap: () {
                              isPriceAsc = null;
                              isRatingFilter = true;
                            },
                          ),
                        )
                      ],
                    ),
                    Divider(
                      color: Colors.grey[300],
                      height: 1,
                    ),
                    SizedBox(height: 5.h),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        padding: EdgeInsets.all(10),
                        onPressed: () {
                          applyFilter();
                        },
                        color: primary,
                        textColor: Colors.white,
                        child: Text("APPLY", style: TextStyle(fontSize: 20.sp)),
                      ),
                    ),
                  ],
                ));
          });
        });
  }

  Future<void> getProduct() async {
    ProductModel response =
        await bloc.fetchAllProducts(subCatData.id, "", "", "");
    hideLoading();
    if (response != null) {
      bool success = response.success;
      if (success) {
        setState(() {
          _productList = response.data != null ? response.data : null;
          for (ProductData data in _productList) {
            isExpand.add(false);
          }
        });
      }
    }
  }

  Future<void> applyFilter() async {
    String sortBy = "", sort = "", filterOptions = "";
    if (isRatingFilter != null && isRatingFilter) {
      sortBy = "rating";
      sort = "desc";
    }
    if (isPriceAsc != null) {
      sortBy = "price";
      sort = isPriceAsc ? "asc" : "desc";
    }

    for (FilterOptions options in _filterList[0].options) {
      if (options.isChecked != null && options.isChecked) {
        if (filterOptions.isEmpty) {
          filterOptions = options.id.toString();
        } else {
          filterOptions = filterOptions + "," + options.id.toString();
        }
      }
    }

    for (FilterOptions options in _filterList[1].options) {
      if (options.isChecked != null && options.isChecked) {
        if (filterOptions.isEmpty) {
          filterOptions = options.id.toString();
        } else {
          filterOptions = filterOptions + "," + options.id.toString();
        }
      }
    }

    ProductModel response = await bloc.fetchAllProducts(
        subCatData.id, filterOptions, sortBy, sort);
    print(filterOptions);
    hideLoading();
    if (response != null) {
      bool success = response.success;
      if (success) {
        Navigator.pop(context);
        setState(() {
          _productList = response.data != null ? response.data : null;
          for (ProductData data in _productList) {
            isExpand.add(false);
          }
        });
      }
    }
  }
}
