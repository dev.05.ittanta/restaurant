import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:restaurant/models/add_address_model.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/category_model.dart';
import 'package:restaurant/models/contact_us.dart';
import 'package:restaurant/models/gallary_model.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/models/user_data.dart';
import 'package:restaurant/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiProvider {
  var dio;
  String token;

  getBaseOption() {
    return BaseOptions(
      baseUrl: Constants.BASE_URL,
      contentType: Headers.jsonContentType,
      responseType: ResponseType.json,
      validateStatus: (_) => true,
    );
  }

  Future<Map<String, dynamic>> checkLoginData(email, password) async {
    dio = Dio(getBaseOption());
    var params = {
      "mobile_email": email,
      "password": password,
    };
    try {
      Response response = await dio.post(
        Constants.LOGIN_URL,
        data: jsonEncode(params),
      );
      return response.data;
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<Map<String, dynamic>> checkSignUpData(
      name, email, phone, password, confirmPw) async {
    dio = Dio(getBaseOption());
    var params = {
      "name": name,
      "email": email,
      "phone": phone,
      "password": password,
      "confirm_password": confirmPw,
    };
    try {
      Response response = await dio.post(
        Constants.SIGNUP_URL,
        data: jsonEncode(params),
      );
      return response.data;
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<Map<String, dynamic>> checkConfirmData(
      name, email, phone, provider, providerId) async {
    dio = Dio(getBaseOption());
    var params = {
      "name": name,
      "email": email,
      "phone": phone,
      "provider": provider,
      "provider_id": providerId,
    };
    try {
      Response response = await dio.post(
        Constants.CONFIRM_DETAIL_URL,
        data: jsonEncode(params),
      );
      return response.data;
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<Map<String, dynamic>> checkSocialLoginData(
      provider, providerId) async {
    dio = Dio(getBaseOption());
    var params = {
      "provider": provider,
      "provider_id": providerId,
    };
    try {
      Response response = await dio.post(
        Constants.SOCIAL_LOGIN,
        data: jsonEncode(params),
      );
      return response.data;
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<Map<String, dynamic>> changePasswordData(
      currentPw, oldPw, confirmPw) async {
    dio = Dio();
    var params = {
      "password": oldPw,
      "current_password": currentPw,
      "confirm_password": confirmPw
    };
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      token = preferences.getString(Constants.APP_TOKEN);
      dio.options.headers['content-Type'] = 'application/json';
      dio.options.headers["authorization"] = "Bearer " + token;
      Response response = await dio.post(Constants.BASE_URL + Constants.CHANGE_PW_URL, data: params);
      print(response.data);
      return response.data;
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<Map<String, dynamic>> forgotPwData(email) async {
    dio = Dio(getBaseOption());
    var params = {
      "email": email,
    };
    try {
      Response response = await dio.post(
        Constants.FORGOT_PASSWORD_URL,
        data: jsonEncode(params),
      );
      print(response.data);
      return response.data;
    } catch (e) {
      print('Error: $e');
    }
  }

  Future<UserData> fetchUserData() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio.get(Constants.BASE_URL + Constants.USER_URL);
    if (response != null && response.data != null) {
      UserData userData = UserData.fromJson(response.data);
      return userData;
    }
    return null;
  }

  Future<Category> fetchCategoryData() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response =
        await dio.get(Constants.BASE_URL + Constants.CATEGORY_URL);
    if (response != null && response.data != null) {
      Category category = Category.fromJson(response.data);
      return category;
    }
    return null;
  }

  Future<ProductModel> fetchProductData(int id, String filterId,
      String sortByPriceRatng, String sortAscDesc) async {
    dio = Dio();
    var params = {
      "category": id,
      "filters": filterId != 0 ? filterId : "",
      "sortby": sortByPriceRatng,
      "sort": sortAscDesc
    };
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio
        .post(Constants.BASE_URL + Constants.PRODUCT_URL, data: params);
    if (response != null && response.data != null) {
      ProductModel product = ProductModel.fromJson(response.data);
      return product;
    }
    return null;
  }

  Future<dynamic> addToCartData(String jsonEncode) async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    dio.options.headers["accept"] = 'application/json';
    print(Constants.BASE_URL + Constants.ADDTOCART_URL);
    Response response = await dio
        .post(Constants.BASE_URL + Constants.ADDTOCART_URL, data: jsonEncode);
    if (response != null && response.data != null && response.data["success"]) {
      print(response.data);
      return response.data;
    }
    return null;
  }

  Future<dynamic> updateCartData(int quantity, int productId) async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio.post(
        Constants.BASE_URL +
            "cart/" +
            productId.toString() +
            Constants.UPDATE_CART_URL,
        data: {"quantity": quantity});
    if (response != null && response.data != null) {
      return response.data;
    }
    return null;
  }

  Future<CartModel> fetchCartData() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response =
        await dio.get(Constants.BASE_URL + Constants.GETTOCART_URL);
    if (response != null && response.data != null) {
      print(response.data);
      CartModel getCartProduct = CartModel.fromJson(response.data);
      return getCartProduct;
    }
    return null;
  }

  Future<PromoCodeModel> promocodeData(String couponCode) async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio.post(
        Constants.BASE_URL + Constants.PROMOCODE_URL,
        data: {"coupon_code": couponCode});
    if (response != null && response.data != null) {
      PromoCodeModel promoCodeModel = PromoCodeModel.fromJson(response.data);
      return promoCodeModel;
    }
    return null;
  }

  Future<Map<String, dynamic>> addProfileAddressData(
      String address,
      String street,
      String city,
      String state,
      String country,
      String pincode,
      String latitude,
      String longitude) async {
    dio = Dio();
    var formData = {
      "address": address,
      "street": street,
      "city": city,
      "state": state,
      "pincode": pincode,
      "country": country,
      "latitude": latitude,
      "longitude": longitude,
    };
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['accept'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio
        .post(Constants.BASE_URL + Constants.ADD_ADDRESS_URL, data: formData);
    if (response != null && response.data != null && response.data["success"]) {
      return response.data;
    }
    return null;
  }

  Future<AddressModel> getAddressData() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response =
        await dio.get(Constants.BASE_URL + Constants.GET_ADDRESS_URL);
    print(response.data);
    if (response != null && response.data != null && response.data["success"]) {
      AddressModel addressModel = AddressModel.fromJson(response.data);
      return addressModel;
    }
    return null;
  }

  Future<Map<String, dynamic>> editAddressData(
      int id,
      String address,
      String street,
      String city,
      String state,
      String country,
      String pincode,
      String latitude,
      String longitude) async {
    dio = Dio();
    var formData = {
      "address": address,
      "street": street,
      "city": city,
      "state": state,
      "pincode": pincode,
      "country": country,
      "latitude": latitude,
      "longitude": longitude,
    };
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio.post(
        Constants.BASE_URL +
            Constants.GET_ADDRESS_URL +
            id.toString() +
            "/edit",
        data: formData);
    if (response != null && response.data["success"]) {
      return response.data;
    }
    return null;
  }

  Future<Map<String, dynamic>> deleteAddressData(int id) async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio.post(Constants.BASE_URL +
        Constants.GET_ADDRESS_URL +
        id.toString() +
        "/delete");
    if (response != null && response.data["success"]) {
      return response.data;
    }
    return null;
  }

  Future<Map<String, dynamic>> checkTimeData(String date, String time) async {
    dio = Dio();
    var params = {"date": date, "time": time};
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    try {
      Response response = await dio
          .post(Constants.BASE_URL + Constants.CHECK_TIME_URL, data: params);
      if (response != null) {
        return response.data;
      }
    } catch (e) {
      print(e);
      return e.response.data;
    }
    return null;
  }

  Future<GetOrdersModel> getOrder() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response =
        await dio.get(Constants.BASE_URL + Constants.GET_ORDER_URL);
    if (response != null && response.data != null && response.data["success"]) {
      GetOrdersModel getOdersModel = GetOrdersModel.fromJson(response.data);
      return getOdersModel;
    }
    return null;
  }

  Future<dynamic> addReviewData(
      int productId, double rating, String comment) async {
    Dio dio = Dio();
    var param = {"product": productId, "rating": rating, "comment": comment};
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response = await dio
        .post(Constants.BASE_URL + Constants.ADD_REVIEW_URL, data: param);
    if (response != null && response.data != null) {
      return response.data;
    }
    return null;
  }
  Future<ContactUsModel> getContactUs() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response =
    await dio.get(Constants.BASE_URL + Constants.ContactUs_URL);
    if (response != null && response.data != null && response.data["success"]) {
      ContactUsModel getContactModel = ContactUsModel.fromJson(response.data);
      return getContactModel;
    }
    return null;
  }

  Future<GallaryModel> getGallaryImage() async {
    dio = Dio();
    SharedPreferences preferences = await SharedPreferences.getInstance();
    token = preferences.getString(Constants.APP_TOKEN);
    dio.options.headers['content-Type'] = 'application/json';
    dio.options.headers["authorization"] = "Bearer " + token;
    Response response =
        await dio.get(Constants.BASE_URL + Constants.GALLARY_IMAGE_URL);
    if (response != null && response.data != null && response.data["success"]) {
      GallaryModel getOdersModel = GallaryModel.fromJson(response.data);
      return getOdersModel;
    }
    return null;
  }
}
