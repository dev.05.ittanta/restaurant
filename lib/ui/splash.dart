import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/drawer_list.dart';
import 'package:restaurant/ui/welcome_info.dart';
import 'package:restaurant/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends Base<SplashScreen> {
  startTimeout() {
    return Timer(Duration(seconds: 2), changeScreen);
  }

  changeScreen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString(Constants.APP_TOKEN);

    if (token != null && token.isNotEmpty) {
      getCurrencySignData();
    } else {
      pushReplacement(WelcomeOne());
    }
  }

  Future<dynamic> getCurrencySignData() async {
    Dio dio = Dio();
    Response response =
        await dio.get(Constants.BASE_URL + Constants.CURRENCY_URL);
    if (response != null && response.data != null) {
      String currency = response.data["data"]["currencySign"];
      prefs.setString(Constants.CURRENCY, currency);
      pushReplacement(DrawerList());
    }
    return null;
  }

  @override
  void initState() {
    super.initState();
    initPreference().then((value) {
      startTimeout();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/splash_bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                child: Image.asset(
                  'assets/images/logo.png',
                ),
              ),
              SizedBox(
                height: 150.h,
              )
            ],
          ),
        ),
      ),
    );
  }
}
