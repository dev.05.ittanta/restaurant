import 'package:restaurant/resources/repository.dart';

class AddReviewBloc {
  final _repository = Repository();

  Future<dynamic> addReviewDetail(int productId , double rating,String comment) async {
    dynamic addReviewProduct = await _repository.addReview(productId,rating,comment);
    return addReviewProduct;
  }
}

final addReviewBloc = AddReviewBloc();