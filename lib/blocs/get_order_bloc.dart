import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class GetOdersBloc {
  final _repository = Repository();
  final _getOrderFetcher = PublishSubject<GetOrdersModel>();
  Stream<GetOrdersModel> get allOderProducts => _getOrderFetcher.stream;

  fetchOrderProducts() async {
    GetOrdersModel getOrderProduct = await _repository.getOrderData();
    _getOrderFetcher.sink.add(getOrderProduct);
  }

}

final getOrderBlock = GetOdersBloc();