class ContactUsModel {
  bool success;
  ContactData data;
  String message;

  ContactUsModel({this.success, this.data, this.message});

  ContactUsModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new ContactData.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class ContactData {
  Contact contact;
  Business business;

  ContactData({this.contact, this.business});

  ContactData.fromJson(Map<String, dynamic> json) {
    contact =
    json['contact'] != null ? new Contact.fromJson(json['contact']) : null;
    business = json['business'] != null
        ? new Business.fromJson(json['business'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.contact != null) {
      data['contact'] = this.contact.toJson();
    }
    if (this.business != null) {
      data['business'] = this.business.toJson();
    }
    return data;
  }
}

class Contact {
  String label;
  String address;
  String phone;
  String fax;
  String email;

  Contact({this.label, this.address, this.phone, this.fax, this.email});

  Contact.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    address = json['address'];
    phone = json['phone'];
    fax = json['fax'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['label'] = this.label;
    data['address'] = this.address;
    data['phone'] = this.phone;
    data['fax'] = this.fax;
    data['email'] = this.email;
    return data;
  }
}

class Business {
  String label;
  List<Hours> hours;

  Business({this.label, this.hours});

  Business.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    if (json['hours'] != null) {
      hours = new List<Hours>();
      json['hours'].forEach((v) {
        hours.add(new Hours.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['label'] = this.label;
    if (this.hours != null) {
      data['hours'] = this.hours.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Hours {
  String label;
  String days;
  String time;

  Hours({this.label, this.days, this.time});

  Hours.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    days = json['days'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['label'] = this.label;
    data['days'] = this.days;
    data['time'] = this.time;
    return data;
  }
}
