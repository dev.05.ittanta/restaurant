import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';

class HomePage extends KFDrawerContent {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends Base<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: <Widget>[
            appBar(Image.asset("assets/images/menu.png", color: Colors.black,),"Home",Colors.white,sky_blue),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Home'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  appBar(Image dwricon, String title, Color bgclr,Color titleclr) {
    return  Container(
      color: bgclr,
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(32.0)),
            child: Material(
              shadowColor: Colors.transparent,
              color:  Colors.transparent,
              child: IconButton(
                icon: dwricon,
                onPressed: widget.onMenuPressed,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(color:titleclr, fontSize: 15.h),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/cart.png",
                width: 25.h, height: 25.h),
          ),
          SizedBox(width: 15.h),
        ],
      ),
    );
  }
}
