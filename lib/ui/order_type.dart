import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/model.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:restaurant/models/add_address_model.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/payment.dart';
import 'package:restaurant/ui/thank_you.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/select_city_state.dart';
import '../blocs/address_bloc.dart';

class OrderType extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OrderTypeState();
  }
}

class OrderList {
  String name;
  int index;

  OrderList({this.name, this.index});
}

class OrderTypeState extends Base<OrderType> {
  String radioItem;
  int id;
  bool isDelivery;
  final fnameController = TextEditingController();
  final lnameController = TextEditingController();
  final addressController = TextEditingController();
  final streetController = TextEditingController();
  final cityController = TextEditingController();
  final countryController = TextEditingController();
  final pincodeController = TextEditingController();
  final dateController = TextEditingController();
  final timeController = TextEditingController();
  String getTime = DateFormat('HH:mm').format(DateTime.now());
  DateTime selectedDate = DateTime.now();
  var format = new DateFormat("yyyy-MM-dd");
  var location = Location();
  double latitude;
  double longitude;
  String stateValue;
  String countryValue;
  bool isAddAddress = false;
  var addressItem;
  String addressesResponse;
  List<AddressData> _addressList = [];
  AddressData selectedAddress;

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        return TextEditingValue(text: format.format(selectedDate));
      });
  }

  List<OrderList> oList = [
    OrderList(
      index: 0,
      name: "Delivery",
    ),
    OrderList(
      index: 1,
      name: "Pick-up time(in 30 minute)",
    ),
  ];

  @override
  void initState() {
    _getAddressList();
    location.requestPermission().then((granted) {
      if (granted != null) {
        location.getLocation().then((LocationData locationData) async {
          var lat = locationData.latitude.toString();
          var longt = locationData.latitude.toString();
          latitude = double.parse(lat);
          longitude = double.parse(longt);
          // final coordinates = new Coordinates(latitude, longitude);
          // print("latitude_____longitude" + coordinates.toString());
          // var addresses =
          // await Geocoder.local.findAddressesFromCoordinates(coordinates);
          // var first = addresses.first;
          // print("${first.featureName} : ${first.addressLine}");
        });
      }
    });
    // location.onLocationChanged.listen((LocationData currentLocation) {
    //   var updateLong = currentLocation.longitude;
    //   var updateLat = currentLocation.latitude;
    //   print(updateLong);
    //   print(updateLat);
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      appBar: CustomAppBar("Delivery Or Pickup", Colors.white, sky_blue),
      body: Stack(
        children: [
          Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/welcome_bg.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(18.0),
              child: ListView(
                physics: ScrollPhysics(),
                shrinkWrap: true,
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                      padding: EdgeInsets.only(top: 17),
                      child: Text('Oder Type',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 24, color: Colors.white))),
                  Container(
                    child: Column(
                      children: oList
                          .map((data) => Theme(
                                data: Theme.of(context).copyWith(
                                    unselectedWidgetColor: Colors.white),
                                child: Column(
                                  children: [
                                    RadioListTile(
                                      activeColor: Colors.redAccent,
                                      title: Text("${data.name}",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white)),
                                      groupValue: id,
                                      value: data.index,
                                      onChanged: (val) {
                                        setState(() {
                                          radioItem = data.name;
                                          id = data.index;
                                          isDelivery = id == 0;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ))
                          .toList(),
                    ),
                  ),
                  isDelivery!=null ? isDelivery?Text(
                    "Choose For Delivery",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 16, color: Colors.white),
                  ):SizedBox():SizedBox(),
                  SizedBox(height: 5),
                  (isDelivery!=null)
                      ? isDelivery ? isAddAddress
                          ? _deliveryAddress()
                          : displayAddressList() : _pickUpTime()
                      : SizedBox(),
                ],
              )),
          Align(
            alignment: Alignment.bottomCenter,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                ButtonTheme(
                  height: 50,
                  minWidth: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    onPressed: () {
                      checkTimeForDelivery();
                    },
                    color: isDelivery != null ? dark_red : Colors.grey,
                    textColor: isDelivery != null ? Colors.white : Colors.black45,
                    child: Text("Continue", style: TextStyle(fontSize: 18)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _deliveryAddress() {
    return Center(
      child: ListView(
        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        shrinkWrap: true,
        physics: ScrollPhysics(),
        children: [
          SizedBox(
            height: 20,
          ),
          // DeliverytextUi("First name", fnameController..text = selectedAddress != null ? selectedAddress.),
          // DeliverytextUi("Last name", lnameController),
          DeliverytextUi("Street 1", addressController),
          DeliverytextUi("Street 2", streetController),
          DeliverytextUi("City", cityController),
          DeliverytextUi("Postal Code", pincodeController),
          SizedBox(
            height: 7,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
            child: SelectState(
              style: TextStyle(color: Colors.grey),
              dropdownColor: Colors.white,
              selectedCountry:
                  selectedAddress != null ? selectedAddress.country : null,
              selectedState:
                  selectedAddress != null ? selectedAddress.province : null,
              onStateChanged: (value) {
                setState(() {
                  stateValue = value;
                });
              },
              onCountryChanged: (value) {
                setState(() {
                  countryValue = value;
                });
              },
            ),
          ),
          IntrinsicHeight(
            child: Row(
              children: [
                VerticalDivider(
                  thickness: 1,
                  width: 20,
                  color: primary,
                ),
                Expanded(
                  child: Center(
                      child: TextButton(
                          onPressed: () async {
                            if (await isConnected()) {
                              if (isValid()) {
                                if (selectedAddress != null) {
                                  //Edit call
                                } else {
                                  addAddress();
                                }
                              }
                            }
                          },
                          child: Text(
                            "Add Address",
                            style: TextStyle(fontSize: 15, color: primary),
                          ))),
                ),
                VerticalDivider(
                  thickness: 1,
                  width: 20,
                  color: primary,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 40,
          ),
        ],
      ),
    );
  }

  DeliverytextUi(String msg, TextEditingController datacontroller) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: TextField(
          style: TextStyle(color: Colors.white),
          controller: datacontroller,
          // onChanged: (text) {
          //   datacontroller.text = text;
          // },
          decoration: new InputDecoration(
            hintText: msg,
            hintStyle: TextStyle(color: Colors.grey),
            isDense: true,
            enabledBorder: const OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(4)),
              borderSide: BorderSide(width: 1, color: Colors.grey),
            ),
            border: const OutlineInputBorder(),
          ),
        ),
      ),
    );
  }

  _pickUpTime() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: TextField(
            onTap: () {
              selectDate(context);
            },
            style: TextStyle(color: Colors.white),
            controller: dateController
              ..text = selectedDate != null ? format.format(selectedDate) : "",
            decoration: InputDecoration(
              suffixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  "assets/images/calendar.png",
                  fit: BoxFit.contain,
                  width: 20,
                  height: 20,
                ),
              ),
              hintText: "Date",
              hintStyle: TextStyle(color: Colors.grey),
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                borderSide: BorderSide(width: 1, color: Colors.grey),
              ),
              border: OutlineInputBorder(),
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Expanded(
          child: TextField(
            onTap: () async {
              TimeOfDay time = TimeOfDay.now();
              FocusScope.of(context).requestFocus(new FocusNode());
              TimeOfDay picked = await showTimePicker(
                context: context,
                initialTime: time,
              );

              if (picked != null && picked != time) {
                setState(() {
                  String minute = picked.minute.toString();
                  if (minute.length == 1) {
                    minute = "0" + minute;
                  }
                  getTime = picked.hour.toString() + ":" + minute;
                });
              }
            },
            style: TextStyle(color: Colors.white),
            controller: timeController..text = getTime != null ? getTime : "",
            decoration: InputDecoration(
              suffixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  "assets/images/watch.png",
                  fit: BoxFit.contain,
                  width: 20,
                  height: 20,
                ),
              ),
              hintText: "Time",
              hintStyle: TextStyle(color: Colors.grey),
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                borderSide: BorderSide(width: 1, color: Colors.grey),
              ),
              border: OutlineInputBorder(),
            ),
          ),
        ),
      ],
    );
  }

  Widget displayAddressList() {
    return ListView(
      shrinkWrap: true,
      physics: ScrollPhysics(),
      padding: EdgeInsets.only(bottom: 60),
      children: [
        ListView.builder(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            itemCount: _addressList.length,
            itemBuilder: (BuildContext contex, int index) {
              var addressItem = _addressList[index];
              return Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Image.asset(
                      "assets/images/location.png",
                      fit: BoxFit.contain,
                      width: 30,
                      height: 30,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              addressItem.address,
                              style: TextStyle(fontSize: 20, color: primary),
                            ),
                            // Text(
                            //   "Building",
                            //   style: TextStyle(fontSize: 15, color: primary),
                            // ),
                          ],
                        ),
                        Text(
                          addressItem.street,
                          style: TextStyle(fontSize: 20, color: primary),
                        ),
                        Row(
                          children: [
                            Text(
                              addressItem.city + " - ",
                              style: TextStyle(fontSize: 20, color: primary),
                            ),
                            Text(
                              addressItem.pincode,
                              style: TextStyle(fontSize: 20, color: primary),
                            ),
                          ],
                        ),
                        Text(
                          addressItem.province,
                          style: TextStyle(fontSize: 20, color: primary),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                      child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextButton(
                          onPressed: () {},
                          style: ButtonStyle(
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.zero),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                        color: primary,
                                      )))),
                          child:
                              Text("Select", style: TextStyle(color: primary))),
                      TextButton(
                          onPressed: () {
                            setState(() {
                              isAddAddress = true;
                              selectedAddress = addressItem;
                              addressController.text = selectedAddress.address;
                              streetController.text = selectedAddress.street;
                              cityController.text = selectedAddress.city;
                              pincodeController.text = selectedAddress.pincode;
                            });
                          },
                          style: ButtonStyle(
                              padding:
                                  MaterialStateProperty.all(EdgeInsets.zero),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                        color: primary,
                                      )))),
                          child:
                              Text("Edit", style: TextStyle(color: primary))),
                      TextButton(
                          onPressed: () {
                            deleteAddressCall(addressItem.id);
                          },
                          style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                        color: primary,
                                      )))),
                          child:
                              Text("Delete", style: TextStyle(color: primary)))
                    ],
                  ))
                ],
              );
            }),
        ButtonTheme(
          child: TextButton(
              onPressed: () {
                setState(() {
                  isAddAddress = true;
                });
              },
              style: ButtonStyle(
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(
                            color: primary,
                          )))),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child:
                    Text("Add New Address", style: TextStyle(color: primary)),
              )),
        )
      ],
    );
  }

  Future<void> addAddress() async {
    Map<String, dynamic> res = await addressbloc.addAddress(
        addressController.text,
        streetController.text,
        cityController.text,
        stateValue,
        countryValue,
        pincodeController.text,
        latitude.toString(),
        longitude.toString());
    bool success = res["success"];
    var message = res["message"];
    if (success) {
      hideLoading();
      showSnackBar(message);
      setState(() {
        isAddAddress = false;
        _getAddressList();
      });
    } else {
      if (res["data"].containsKey("address")) {
        showSnackBar(res["data"]["address"][0]);
      } else if (res["data"].containsKey("state")) {
        showSnackBar(res["data"]["state"][0]);
      } else if (res["data"].containsKey("city")) {
        showSnackBar(res["data"]["city"][0]);
      } else if (res["data"].containsKey("country")) {
        showSnackBar(res["data"]["country"][0]);
      } else if (res["data"].containsKey("pincode")) {
        showSnackBar(res["data"]["pincode"][0]);
      }
    }
  }

  Future<void> editAddressCall() async {
    Map<String, dynamic> res = await addressbloc.editAddress(
        selectedAddress.id,
        addressController.text,
        streetController.text,
        cityController.text,
        stateValue,
        countryValue,
        pincodeController.text,
        latitude.toString(),
        longitude.toString());
    if (res != null) {
      hideLoading();
      print("Edit Address");
      setState(() {});
    }
  }

  bool isValid() {
    if (addressController.text.isEmpty) {
      showSnackBar('Please enter address');
      return false;
    }
    if (streetController.text.isEmpty) {
      showSnackBar('Please enter street');
      return false;
    }
    if (cityController.text.isEmpty) {
      showSnackBar('Please enter city');
    }
    if (pincodeController.text.isEmpty) {
      showSnackBar('Please enter pincode');
      return false;
    }
    if (stateValue.isEmpty) {
      showSnackBar('Please enter state');
      return false;
    }
    return true;
  }

  Future<void> _getAddressList() async {
    _addressList.clear();
    AddressModel response = await addressbloc.getAddress();
    if (response != null) {
      String message = response.message;
      if (response.success) {
        setState(() {
          _addressList = response.data;
        });
      }
    }
  }

  Future<void> deleteAddressCall(int id) async {
    showLoading();
    Map<String, dynamic> res = await addressbloc.deleteAddress(id);
    bool success = res["success"];
    var message = res["message"];
    if (success) {
      hideLoading();
      showSnackBar(message);
      _getAddressList();
    }
  }

  Future<void> checkTimeForDelivery() async {
    Map<String, dynamic> res = await addressbloc.checkTimeForDeliveryOder(
        dateController.text, timeController.text);
    bool success = res["success"];
    if (success) {
      var message = res["message"];
      hideLoading();
      showSnackBar(message);
      push(PaymentScreen());
    } else {
      showSnackBar(res["data"]["date"][0]);
      hideLoading();
    }
  }
}
