import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/widgets.dart';
import 'dart:convert';
import 'package:restaurant/models/select_status_model.dart' as StatusModel;

class SelectState extends StatefulWidget {
  final ValueChanged<String> onCountryChanged;
  final ValueChanged<String> onStateChanged;

  // final ValueChanged<String> onCityChanged;
  final TextStyle style;
  final Color dropdownColor;
  final String selectedState;
  final String selectedCountry;

  const SelectState(
      {Key key,
      this.onCountryChanged,
      this.onStateChanged,
      // this.onCityChanged,
      this.style,
      this.dropdownColor,
      this.selectedCountry,
      this.selectedState})
      : super(key: key);

  @override
  _SelectStateState createState() => _SelectStateState();
}

class _SelectStateState extends State<SelectState> {
  List<String> _country = [];
  String _selectedCountry = "Choose Country";
  String _selectedState = "Choose State";
  List<String> _states = [];
  var responses;

  @override
  void initState() {
    getCounty();
    super.initState();

    if (widget.selectedCountry != null) {
      _selectedCountry = widget.selectedCountry;
      print(_selectedCountry);
    }

    if (widget.selectedState != null) {
      _selectedState = widget.selectedState;
    }
  }

  Future getResponse() async {
    var res = await rootBundle.loadString('assets/country.json');
    return jsonDecode(res);
  }

  Future getCounty() async {
    var countryres = await getResponse() as List;
    _country.clear();
    _country.add("Choose Country");
    countryres.forEach((data) {
      var model = StatusModel.StatusModel();
      model.name = data['name'];
      model.emoji = data['emoji'];
      if (!mounted) return;
      setState(() {
        _country.add(model.name);
      });
    });

    if (widget.selectedCountry != null) {
      setState(() {
        _states = ["Choose State"];
        this.widget.onCountryChanged(_selectedCountry);
        getState();
      });
    }

    return _country;
  }

  Future getState() async {
    var response = await getResponse();
    var takestate = response
        .map((map) => StatusModel.StatusModel.fromJson(map))
        .where((item) => item.name == _selectedCountry)
        .map((item) => item.state)
        .toList();
    var states = takestate as List;
    states.forEach((f) {
      if (!mounted) return;
      setState(() {
        var name = f.map((item) => item.name).toList();
        _states.clear();
        _states.add("Choose State");
        for (var statename in name) {
          print(statename.toString());
          _states.add(statename.toString());
        }
      });
    });

    return _states;
  }

  // Future getCity() async {
  //   var response = await getResponse();
  //   var takestate = response
  //       .map((map) => StatusModel.StatusModel.fromJson(map))
  //       .where((item) => item.emoji + "    " + item.name == _selectedCountry)
  //       .map((item) => item.state)
  //       .toList();
  //   var states = takestate as List;
  //   states.forEach((f) {
  //     var name = f.where((item) => item.name == _selectedState);
  //     var cityname = name.map((item) => item.city).toList();
  //     cityname.forEach((ci) {
  //       if (!mounted) return;
  //       setState(() {
  //         var citiesname = ci.map((item) => item.name).toList();
  //         for (var citynames in citiesname) {
  //           print(citynames.toString());
  //
  //           _cities.add(citynames.toString());
  //         }
  //       });
  //     });
  //   });
  //   return _cities;
  // }

  void _onSelectedCountry(String value) {
    if (!mounted) return;
    setState(() {
      _selectedState = "Choose State";
      _states = ["Choose State"];
      _selectedCountry = value;
      this.widget.onCountryChanged(value);
      getState();
    });
  }

  void _onSelectedState(String value) {
    if (!mounted) return;
    setState(() {
      // _selectedCity = "Choose City";
      // _cities = ["Choose City"];
      _selectedState = value;
      this.widget.onStateChanged(value);
      // getCity();
    });
  }

  // void _onSelectedCity(String value) {
  //   if (!mounted) return;
  //   setState(() {
  //     _selectedCity = value;
  //     this.widget.onCityChanged(value);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            border: Border.all(
                color: Colors.grey, style: BorderStyle.solid, width: 0),
          ),
          child: DropdownButton<String>(
            underline: SizedBox(),
            dropdownColor: widget.dropdownColor,
            isExpanded: true,
            items: _country.map((String dropDownStringItem) {
              return DropdownMenuItem<String>(
                value: dropDownStringItem,
                child: Row(
                  children: [
                    Text(
                      dropDownStringItem,
                      style: widget.style,
                    )
                  ],
                ),
              );
            }).toList(),
            onChanged: (value) => _onSelectedCountry(value),
            value: _selectedCountry,
          ),
        ),
        SizedBox(height: 8),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            border: Border.all(
                color: Colors.grey, style: BorderStyle.solid, width: 0),
          ),
          child: DropdownButton<String>(
            underline: SizedBox(),
            dropdownColor: widget.dropdownColor,
            isExpanded: true,
            items: _states.map((String dropDownStringItem) {
              return DropdownMenuItem<String>(
                value: dropDownStringItem,
                child: Text(dropDownStringItem, style: widget.style),
              );
            }).toList(),
            onChanged: (value) => _onSelectedState(value),
            value: _selectedState,
          ),
        ),
        // DropdownButton<String>(
        //   dropdownColor: widget.dropdownColor,
        //   isExpanded: true,
        //   items: _cities.map((String dropDownStringItem) {
        //     return DropdownMenuItem<String>(
        //       value: dropDownStringItem,
        //       child: Text(dropDownStringItem, style: widget.style),
        //     );
        //   }).toList(),
        //   onChanged: (value) => _onSelectedCity(value),
        //   value: _selectedCity,
        // ),
        SizedBox(
          height: 10.0,
        ),
      ],
    );
  }
}
