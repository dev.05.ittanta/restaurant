import 'package:flutter/material.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/models/category_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/sub_category_itemlist.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/oval_shadow.dart';

class SubCategoryList extends StatefulWidget {
  final Categories catData;
final List<Filters>filterLists;
  const SubCategoryList({Key key, this.catData, this.filterLists}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SubCategoryState();
  }
}

class SubCategoryState extends Base<SubCategoryList> {
  final TextEditingController _searchControl = new TextEditingController();
  List<Childs> _searchResult = [];
  List<Childs> _subCategoryList = [];
  Categories catData;
  final _repository = Repository();
  int cartCount = 0;
  List<Filters> filterLists = [];

  @override
  void initState() {
    super.initState();
    catData = widget.catData;
    _subCategoryList = catData.childs;
    filterLists = widget.filterLists;
    getCartCount();
  }

  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Padding(
          padding: const EdgeInsets.all(14.0),
          child: InkWell(
            child: Image.asset(
              "assets/images/back_arrow.png",
              color: Colors.black,
              fit: BoxFit.contain,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Center(
          child: Text(
            catData.name,
            textAlign: TextAlign.center,
            style: TextStyle(color: sky_blue, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount!=null?cartCount.toString():"0",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.red, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            searchBar(),
            Expanded(flex: 1, child: buildList())
          ],
        ),
      ),
    );
  }

  Widget buildList() {
    List<Childs> filterLists = [];
    if (_searchResult != null && _searchResult.length > 0) {
      filterLists = _searchResult;
    } else {
      filterLists = _subCategoryList;
    }
    return Container(
        decoration: BoxDecoration(color: category_bg),
        child: Center(
            child: filterLists != null
                ? ListView.builder(
                    itemCount: filterLists.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      var item = filterLists[index];
                      return itemUi(item);
                    })
                : CircularProgressIndicator()));
  }

  Widget itemUi(Childs item) {
    return GestureDetector(
      onTap: () {
        push(SubCatItem(subCatItem: item,filterLists:filterLists));
      },
      child: Container(
        margin: EdgeInsets.only(top: 20),
        child: Stack(
          children: [
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              elevation: 5,
              margin: EdgeInsets.only(left: 25, right: 25),
              child: Padding(
                padding: const EdgeInsets.all(25.0),
                child: Row(
                  children: [
                    Expanded(
                        child: Padding(
                      padding: const EdgeInsets.only(left: 25),
                      child: Text(item.name,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15.h,
                          )),
                    )),
                  ],
                ),
              ),
            ),
            Positioned.fill(
                left: 10,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: 40,
                    height: 40,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.grey,
                      borderRadius: BorderRadius.circular(5),
                        image: DecorationImage(
                            image: item.image!=null && item.image.isNotEmpty?NetworkImage(
                               item.image):AssetImage("assets/images/no_img.png"),
                            fit: BoxFit.fill)
                    ),
                  ),
                )),
            Positioned.fill(
                right: 10,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Image.asset(
                    'assets/images/arrow_shadow.png',
                    width: 35,
                    height: 35,
                    fit: BoxFit.cover,
                  ),
                )),
          ],
        ),
      ),
    );
  }

  searchBar() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(
              Radius.circular(0.0),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 4,
                child: TextField(
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.white,
                  ),
                  onChanged: onSearchTextChanged,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20.0),
                    // border: OutlineInputBorder(
                    //   borderRadius: BorderRadius.circular(5.0),
                    //   borderSide: BorderSide(color: Colors.white,),
                    // ),
                    // enabledBorder: OutlineInputBorder(
                    //   borderSide: BorderSide(color: Colors.white,),
                    //   borderRadius: BorderRadius.circular(5.0),
                    // ),
                    hintText: "Search",
                    suffixIcon: Icon(
                      Icons.search,
                      color: primary,
                    ),
                    hintStyle: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                    ),
                  ),
                  maxLines: 1,
                  controller: _searchControl,
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  margin: EdgeInsets.only(right: 5.h),
                  color: Colors.white,
                  width: 30.h,
                  height: 20.h,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/images/filter_icon.png",
                          width: 10.h,
                          height: 10.h,
                        ),
                        SizedBox(
                          width: 5.h,
                        ),
                        Text(
                          "Filter",
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    _subCategoryList.forEach((subCategory) {
      if (subCategory.name.toLowerCase().startsWith(text))
        _searchResult.add(subCategory);
    });

    setState(() {});
  }
}
