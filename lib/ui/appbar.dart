import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/ui/add_to_cart.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  final String title;
  final Color color;
  final Color titleClr;

  CustomAppBar(
    this.title,
    this.color,
    this.titleClr, {
    Key key,
  })  : preferredSize = Size.fromHeight(50.0),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Padding(
            padding: const EdgeInsets.only(left:8.0),
            child: Image.asset("assets/images/back_arrow.png"),
          )),
      leadingWidth: 30.h,
      title: Center(
        child: Text(
          title,
          style: TextStyle(color: titleClr),
        ),
      ),
      backgroundColor: color,
      actions: [
        Row(
          children: [
            GestureDetector(
              onTap: () {},
              child: Image.asset("assets/images/home_food_menu.png",
                  width: 25.h, height: 20.h),
            ),
            SizedBox(width: 15.h),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddToCart()));
              },
              child: Image.asset("assets/images/cart.png",
                  width: 25.h, height: 25.h),
            ),
            SizedBox(width: 15.h),
          ],
        )
      ],
    );

  }
}
