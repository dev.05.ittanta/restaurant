import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/blocs/address_bloc.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/kf_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Profile extends KFDrawerContent {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends Base<Profile> {
  final addressController = TextEditingController();
  final streetController = TextEditingController();
  final cityController = TextEditingController();
  final provinceController = TextEditingController();
  final countryController = TextEditingController();
  final pincodeController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Image.asset(
                "assets/images/menu.png",
                color: Colors.white,
                fit: BoxFit.contain,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "Edit Profile",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                color: Colors.white, width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                //   child: Text(
                //     cartCount.toString(),
                //     textAlign: TextAlign.center,
                //     style: TextStyle(color: Colors.white, fontSize: 10.h),
                //   ),
                // ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      color: Colors.white, width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Stack(
        children: [
          ListView(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            children: [
              Stack(
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(bottom: 20),
                    child: Image.asset(
                      "assets/images/profile_bg.png",
                      height: 100,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                      bottom: 0,
                      left: 30,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 80,
                                height: 80,
                                alignment: Alignment.centerLeft,
                                decoration: BoxDecoration(
                                    border: Border.all(
                                      color: Colors.white,
                                    ),
                                    shape: BoxShape.circle,
                                    // borderRadius: BorderRadius.all(Radius.circular(20)),
                                    image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/user_img.png"),
                                        fit: BoxFit.cover)),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(25, 30, 15, 15),
                                child: Image.asset(
                                  "assets/images/img_upload.png",
                                  alignment: Alignment.center,
                                  height: 30,
                                  width: 30,
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(width: 30),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                'Kirti Radadiya',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 17.0),
                              ),
                              Text(
                                'Kirti@Radadiya',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 13.0),
                              ),
                            ],
                          )
                        ],
                      )),
                ],
              ),
              ListView(
                shrinkWrap: true,
                physics: ScrollPhysics(),
                padding: EdgeInsets.fromLTRB(15, 15, 15, 50),
                children: [
                  Text(
                    "** Photo must not bigger than 250kb",
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.grey, fontSize: 13.h),
                  ),
                  SizedBox(height: 30),
                  Row(
                    children: [
                      Text(
                        "Name",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 15.h),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Text(
                        "email",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 12.h),
                      ),
                      Spacer(),
                      Text(
                        "verify",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.red, fontSize: 12.h),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Text(
                        "80934739874",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 12.h),
                      ),
                      Spacer(),
                      Text(
                        "verify",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.red, fontSize: 12.h),
                      ),
                    ],
                  ),
                  Divider(),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Text(
                        "About me",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 15.h),
                      ),
                    ],
                  ),
                  SizedBox(height: 30),
                  Row(
                    children: [
                      Text(
                        "Date of birth",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 12.h),
                      ),
                      // Spacer(),
                      // TextField(
                      //   decoration: InputDecoration(
                      //     hintText: "bob",
                      //     hintStyle: TextStyle(color: Colors.grey),
                      //     isDense: true,
                      //   ),
                      // ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Text(
                        "Date of anniversary",
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 12.h),
                      ),

                      // Spacer(),
                      // TextField(
                      //   decoration: InputDecoration(
                      //     hintText: "bob",
                      //     hintStyle: TextStyle(color: Colors.grey),
                      //     isDense: true,
                      //   ),
                      // ),
                    ],
                  ),
                  SizedBox(height: 30),
                  Row(children: <Widget>[
                    Expanded(
                      child: new Container(
                          margin:
                              const EdgeInsets.only(left: 10.0, right: 20.0),
                          child: Divider(
                            color: Colors.grey[300],
                            height: 36,
                            thickness: 8,
                          )),
                    ),
                    Text(
                      "Address",
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.black, fontSize: 15.h),
                    ),
                    Expanded(
                      child: new Container(
                          margin:
                              const EdgeInsets.only(left: 20.0, right: 10.0),
                          child: Divider(
                            color: Colors.grey[300],
                            height: 36,
                            thickness: 8,
                          )),
                    ),
                  ]),
                  SizedBox(
                    height: 20,
                  ),
                  addressUi("Address", addressController),
                  addressUi("Street", streetController),
                  addressUi("City", cityController),
                  addressUi("Province", provinceController),
                  addressUi("ZipCode", pincodeController),
                  addressUi("Country", countryController),
                  SizedBox(width: 30),
                ],
              )
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: SizedBox(
                    height: 45,
                    child: TextButton(
                        onPressed: () {},
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(dark_red),
                            shape: MaterialStateProperty
                                .all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        side: BorderSide(
                              color: dark_red,
                            )))),
                        child: Text("Save",
                            style:
                                TextStyle(color: Colors.white, fontSize: 18))),
                  ),
                ),
                Expanded(
                  child: SizedBox(
                    height: 45,
                    child: TextButton(
                        onPressed: () {},
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey),
                            shape: MaterialStateProperty
                                .all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        side: BorderSide(
                              color: Colors.grey,
                            )))),
                        child: Text("Cancel",
                            style: TextStyle(
                                color: Colors.black45, fontSize: 18))),
                  ),
                ),
                Expanded(
                  child: SizedBox(
                    height: 45,
                    child: TextButton(
                        onPressed: () {},
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.grey),
                            shape: MaterialStateProperty
                                .all<RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        side: BorderSide(
                              color: Colors.grey,
                            )))),
                        child: Text("Deactive",
                            style: TextStyle(
                                color: Colors.black45, fontSize: 18))),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  appBar(Image dwricon, String title, Color bgclr, Color titleclr) {
    return Container(
      color: bgclr,
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(32.0)),
            child: Material(
              shadowColor: Colors.transparent,
              color: Colors.transparent,
              child: IconButton(
                icon: dwricon,
                onPressed: widget.onMenuPressed,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(color: titleclr, fontSize: 15.h),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h, color: Colors.white),
          ),
          SizedBox(width: 10.h),
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/cart.png",
                width: 25.h, height: 25.h, color: Colors.white),
          ),
          SizedBox(width: 15.h),
        ],
      ),
    );
  }

  addressUi(String hintString, TextEditingController profileAddressController) {
    return TextField(
      controller: profileAddressController,
      decoration: InputDecoration(
        hintText: hintString,
        hintStyle: TextStyle(color: Colors.grey),
      ),
    );
  }

// Future<void> addProfileAddress() async {
//   Map<String, dynamic> res = await bloc.addAddress(
//       addressController.text,
//       streetController.text,
//       cityController.text,
//       provinceController.text,
//       countryController.text,
//       pincodeController.text);
//   bool success = res["success"];
//   print("GetAddress_Success");
//   if (success) {
//     hideLoading();
//     String token = res["data"]["token"];
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     prefs.setString(Constants.APP_TOKEN, token);
//   }
// }
}
