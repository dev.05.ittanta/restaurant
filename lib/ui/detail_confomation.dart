import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:restaurant/blocs/confirm_detail_bloc.dart';
import 'package:restaurant/models/user_data.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/drawer_list.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/strings.dart';
import 'package:restaurant/blocs/user_bloc.dart';

class DetailConfomation extends StatefulWidget {
  final provider, providerId, name, email, photoUrl;

  DetailConfomation(
      {this.provider, this.providerId, this.name, this.email, this.photoUrl});

  @override
  State<StatefulWidget> createState() {
    return ConfomationState();
  }
}

class ConfomationState extends Base<DetailConfomation> {
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  String provider, providerId, name, email, photoUrl;

  @override
  void initState() {
    super.initState();
    provider = widget.provider;
    providerId = widget.providerId;
    name = widget.name;
    email = widget.email;
    photoUrl = widget.photoUrl;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                  )),
              Expanded(
                  flex: 1,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                                AssetImage('assets/images/login_half_bg.png'),
                            fit: BoxFit.fill)),
                  )),
            ],
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              children: [
                SizedBox(height: 80.h),
                Image.asset(
                  'assets/images/login_logo.png',
                  fit: BoxFit.cover,
                ),
                SizedBox(height: 50.h),
                Text(
                  Strings.conform_detail_continue,
                  style: TextStyle(fontSize: 17.sp),
                  textAlign: TextAlign.end,
                ),
              ],
            ),
          ),
          Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  elevation: 20,
                  margin: EdgeInsets.fromLTRB(30, 20, 30, 0),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          Constants.CONFIRM_DETAIL,
                          style: TextStyle(fontSize: 23.sp),
                          textAlign: TextAlign.left,
                        ),
                        SizedBox(
                          height: 5.h,
                        ),
                        Row(
                          children: [
                            ClipOval(
                              child: widget.photoUrl != null
                                  ? Image.network(
                                      widget.photoUrl,
                                      width: 60.h,
                                      height: 60.h,
                                    )
                                  : Image.asset(
                                      "assets/images/profile.png",
                                      width: 60.h,
                                      height: 60.h,
                                    ),
                            ),
                            SizedBox(
                              width: 15.h,
                            ),
                            Text(
                              Constants.DETAIL_PROFILE_IMAGE,
                              style: TextStyle(fontSize: 17.sp),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        TextField(
                          controller: nameController..text = name,
                          onChanged: (value){
                            name = value;
                          },
                          decoration: new InputDecoration(
                            hintText: Constants.NAME,
                            hintStyle: TextStyle(color: hint_clr),
                            isDense: true,
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 0.0),
                            ),
                            border: const OutlineInputBorder(),
                          ),
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        TextField(
                          controller: phoneController,
                          keyboardType: TextInputType.number,
                          decoration: new InputDecoration(
                            hintText: Constants.MOBILE,
                            hintStyle: TextStyle(color: hint_clr),
                            isDense: true,
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 0.0),
                            ),
                            border: const OutlineInputBorder(),
                          ),
                        ),
                        SizedBox(
                          height: 20.h,
                        ),
                        TextField(
                          controller:emailController
                            ..text = email,
                          onChanged: (value){
                            email = value;
                          },
                          decoration: new InputDecoration(
                            hintText: Constants.EMAIL,
                            hintStyle: TextStyle(color: hint_clr),
                            isDense: true,
                            enabledBorder: const OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.grey, width: 0.0),
                            ),
                            border: const OutlineInputBorder(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                ButtonTheme(
                  height: 40.h,
                  minWidth: 290.h,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(color: Colors.white)),
                    onPressed: () async {
                      if (await isConnected()) {
                        if (isValid()) {
                          showLoading();
                          _getConfirmDetail();
                        }
                      }
                    },
                    color: Colors.white,
                    textColor: sky_blue,
                    child: Text(Constants.CONFORM,
                        style: TextStyle(fontSize: 18.h)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  bool isValid() {
    if (nameController.text.isEmpty) {
      showSnackBar('Please enter name');
      return false;
    }
    if (emailController.text.isEmpty) {
      showSnackBar('Please enter email');
      return false;
    }
    if (phoneController.text.isEmpty) {
      showSnackBar('Please enter phone');
      return false;
    }
    return true;
  }

  Future<void> _getConfirmDetail() async {
    Map<String, dynamic> res = await bloc.doConfirmDetail(nameController.text,
       emailController.text, phoneController.text, widget.provider, widget.providerId);
    if (res != null) {
      bool success = res["success"];
      if (success) {
        hideLoading();
        String token = res["data"]["token"];
        prefs.setString(Constants.APP_TOKEN, token);
        _getUserData();
      } else {
        if (res["data"].containsKey("email")) {
          showSnackBar(res["data"]["email"][0]);
        } else if (res["data"].containsKey("phone")) {
          showSnackBar(res["data"]["phone"][0]);
        }
        hideLoading();
      }
    }
  }

  Future<void> _getUserData() async {
    UserData userData = await userbloc.fetchAllUserData();
    prefs.setString(Constants.USER_DATA, json.encode(userData));
    pushReplacement(DrawerList());
  }
}
