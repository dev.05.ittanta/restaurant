import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/about_us.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/category.dart';
import 'package:restaurant/ui/change_password.dart';
import 'package:restaurant/ui/contact.dart';
import 'file:///D:/ittanta/Restaurent/restaurant/lib/utils/custom_logout_dialog.dart';
import 'package:restaurant/ui/favourite.dart';
import 'package:restaurant/ui/gallary.dart';
import 'package:restaurant/ui/login.dart';
import 'package:restaurant/ui/my_order.dart';
import 'package:restaurant/ui/profile.dart';
import 'package:restaurant/ui/reservations.dart';
import 'package:restaurant/utils/class_builder.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';

class DrawerList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DrawerListState();
  }
}

class _DrawerListState extends Base<DrawerList> with TickerProviderStateMixin {
  KFDrawerController _drawerController;
  String userName = "", email = "";
  String profileImg = "";
  var dialog;

  @override
  void initState() {
    super.initState();
    initUserData().then((value) {
      setState(() {
        userName = user.data.name;
        email = user.data.email;
        profileImg = user.data.image;
      });
    });
    _drawerController = KFDrawerController(
      initialPage: ClassBuilder.fromString('HomePage'),
      items: [
        KFDrawerItem.initWithPage(
          text: Text("Menu", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/food_menu.png",
            width: 20.h,
            height: 20.h,
          ),
          page: CategoryList(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("Reservations", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/reservations.png",
            width: 20.h,
            height: 20.h,
          ),
          page: Reservations(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("About Us", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/about.png",
            width: 20.h,
            height: 20.h,
          ),
          page: AboutUs(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("Gallary", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/gallary.png",
            width: 20.h,
            height: 20.h,
          ),
          page: Gallary(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("Contact", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/contact.png",
            width: 20.h,
            height: 20.h,
          ),
          page: ContactScreen(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("My Orders", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/my_orders.png",
            width: 20.h,
            height: 20.h,
          ),
          page: MyOrders(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("Favourite", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/favourites.png",
            width: 20.h,
            height: 20.h,
          ),
          page: Favourite(),
        ),
        KFDrawerItem.initWithPage(
          text:
              Text("Change Password", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/change_password.png",
            width: 20.h,
            height: 20.h,
          ),
          page: ChangePassword(),
        ),
        KFDrawerItem.initWithPage(
          text: Text("Profile", style: TextStyle(color: drawer_font_clr)),
          icon: Image.asset(
            "assets/images/drawer_profile.png",
            width: 20.h,
            height: 20.h,
          ),
          page: Profile(),
        ),
      ],
    );
  }

  drawerItems(Image image, String name, CategoryList page) {
    KFDrawerItem.initWithPage(
      text: Text(name, style: TextStyle(color: Colors.white)),
      icon: image,
      page: page,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                colorFilter: ColorFilter.mode(
                    Colors.white.withOpacity(0.7), BlendMode.hardLight),
                image: new ExactAssetImage('assets/images/welcome_bg.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          KFDrawer(
            borderRadius: 0.0,
            shadowBorderRadius: 0.0,
            menuPadding: EdgeInsets.all(0.5),
            scrollable: true,
            controller: _drawerController,
            header: Container(
              height: 100.h,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    begin: Alignment(-1.0, 1.0),
                    end: Alignment(1.0, 4.0),
                    colors: [
                      const Color(0xFF3366FF),
                      const Color(0xFF00CCFF),
                    ],
                    tileMode: TileMode.clamp),
              ),
              child: Row(
                children: [
                  Container(
                    width: 60.w,
                    height: 60.h,
                    margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.grey,
                        image: DecorationImage(
                            image:profileImg!=null && profileImg.isNotEmpty?NetworkImage(
                                profileImg):AssetImage("assets/images/no_img.png"),
                            fit: BoxFit.fill)
                    ),
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            userName,
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 12.h),
                          ),
                          SizedBox(
                            height: 5.h,
                          ),
                          Text(
                            email != null ? email : "",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 10.h),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            footer: KFDrawerItem(
              text: Text(
                'Logout',
                style: TextStyle(color: drawer_font_clr),
              ),
              icon: Image.asset(
                "assets/images/logout.png",
                width: 20.h,
                height: 20.h,
              ),
              onPressed: () {
                dialog = CustomAlertDialog(
                    title: "Logout",
                    message: "Are you sure you want logout?",
                    onPostivePressed: () {
                      appLogOut();
                    },
                    positiveBtnText: 'Yes',
                    onNegativePressed: () {},
                    negativeBtnText: 'No');
                showDialog(
                    context: context,
                    builder: (BuildContext context) => dialog);
              },
            ),
          ),
        ],
      ),
    );
  }

  void appLogOut() {
    prefs.setString(Constants.USER_DATA, "");
    pushAndRemoveUntil(Login());
  }
}
