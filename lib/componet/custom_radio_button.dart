import 'package:flutter/material.dart';
import 'package:restaurant/utils/custom_color.dart';

class CustomRadioWidget<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final ValueChanged<T> onChanged;
  final double width;
  final double height;
  final String title;
  final String description;

  CustomRadioWidget({this.value, this.groupValue, this.onChanged, this.width = 20, this.height = 20,this.title,this.description});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: GestureDetector(
        onTap: () {
          onChanged(this.value);
        },
        child: Card(
          shape: RoundedRectangleBorder(
              side:
              BorderSide(color: Colors.grey[200], width: 1.0),
              borderRadius: BorderRadius.circular(4.0)),
          child: ListView(
            physics: ScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.all(10),
            children: [
              Row(children: [
                Container(
                  height: this.height,
                  width: this.width,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: radio_inactive_clr,
                  ),
                  child: value == groupValue ? Center(
                    child: Container(
                      height: this.height - 8,
                      width: this.width - 8,
                      decoration: ShapeDecoration(
                        shape: CircleBorder(),
                        color: sky_blue,
                      ),
                    ),
                  ) : SizedBox(),
                ),
                SizedBox(width: 10),
                Text(title,textAlign: TextAlign.start,)
              ],),
              Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: Text(description ?? "",
                    style: TextStyle(
                        fontSize: 12, color: Colors.grey),
                    textAlign: TextAlign.start),
              ),
            ],
          ),
        )
      ),
    );
  }
}