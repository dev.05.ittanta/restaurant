import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/blocs/add_review_block.dart';
import 'package:restaurant/blocs/get_order_bloc.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/order_details.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/dot_seperater.dart';
import 'package:restaurant/utils/kf_drawer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/start_rating.dart';

class MyOrders extends KFDrawerContent {
  @override
  State<StatefulWidget> createState() {
    return MyOrdersState();
  }
}

class MyOrdersState extends Base<MyOrders> {
  final _repository = Repository();
  int cartCount = 0;
  double rating;
  List<GetOrders> _getOderList;
  List<GetOrdersItems> _getOderItemList;
  TextEditingController commentController = new TextEditingController();

  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    getOrderBlock.fetchOrderProducts();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Image.asset(
                "assets/images/menu.png",
                color: Colors.white,
                fit: BoxFit.contain,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "Oders",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                color: Colors.white, width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      color: Colors.white, width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: StreamBuilder(
        stream: getOrderBlock.allOderProducts,
        builder: (context, AsyncSnapshot<GetOrdersModel> snapshot) {
          if (snapshot.hasData) {
            _getOderList =
                snapshot.data.data != null ? snapshot.data.data : null;

            return buildList();
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildList() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Past Orders",
            textAlign: TextAlign.start,
            style: TextStyle(color: Colors.black, fontSize: 18.h),
          ),
        ),
        SizedBox(height: 10),
        ListView.builder(
            shrinkWrap: true,
            physics: ScrollPhysics(),
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            itemCount: _getOderList.length,
            itemBuilder: (BuildContext context, int index) {
              var item = _getOderList[index];
              _getOderItemList = item.items;
              var orderItem = _getOderItemList[index];
              return itemUi(
                  item != null ? item : "", orderItem != null ? orderItem : "");
            }),
      ],
    );
  }

  Widget itemUi(GetOrders item, GetOrdersItems orderItem) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            Container(
              width: 40.w,
              height: 40.h,
              margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: Image.asset(
                'assets/images/fish.png',
                fit: BoxFit.contain,
              ),
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(Constants.APP_NAME,
                    style: TextStyle(color: Colors.black, fontSize: 16.h)),
                SizedBox(height: 5),
                Text("Randal Rd,Btavia,IL,Usa",
                    style: TextStyle(color: Colors.grey, fontSize: 14.h)),
              ],
            ),
            Expanded(
              child: Image.asset(
                'assets/images/confirm.png',
                alignment: Alignment.centerRight,
                fit: BoxFit.contain,
                height: 30,
                width: 30,
              ),
            ),
          ],
        ),
        SizedBox(height: 10),
        Divider(color: Colors.grey, height: 1),
        SizedBox(height: 10),
        Row(
          children: [
            Text("Oder Number: ",
                style: TextStyle(color: Colors.black, fontSize: 16.h)),
            SizedBox(height: 5),
            Text(item.orderNumber,
                style: TextStyle(color: Colors.grey, fontSize: 15.h)),
          ],
        ),
        Text(
          orderItem.name + " x " + orderItem.qauntity.toString() + ",",
          style: TextStyle(color: Colors.grey, fontSize: 14.h),
          textAlign: TextAlign.start,
        ),
        SizedBox(height: 30),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Flexible(
                child: InkWell(
                  child: Text("View Detail",
                      style: TextStyle(color: primary, fontSize: 10.h)),
                  onTap: () {
                    push(OderDetails(orderList: item));
                  },
                ),
              ),
              Container(
                  height: 20,
                  child: VerticalDivider(color: Colors.black, thickness: 2)),
              InkWell(
                  child: Text("Add Review",
                      style: TextStyle(color: primary, fontSize: 10.h)),
                  onTap: () async {
                    await showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                          content: addReviewDialog(item, orderItem)),
                    );
                  }),
              SizedBox(width: 40),
              Container(
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey[500],
                    ),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(0))),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(item.orderPlaceAt,
                        style: TextStyle(color: Colors.black, fontSize: 10.h)),
                    // Text("02",
                    //     style: TextStyle(color: primary, fontSize: 10.h)),
                    // Text(", 2010",
                    //     style:
                    //     TextStyle(color: Colors.black, fontSize: 10.h)),
                    // Container(
                    //     height: 20,
                    //     child: VerticalDivider(
                    //         color: Colors.black, thickness: 2)),
                    // Text("06 : 00 Am",
                    //     style:
                    //     TextStyle(color: Colors.black, fontSize: 10.h)),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 10),
        Container(
          decoration: BoxDecoration(color: re_order_bg),
          padding: EdgeInsets.all(10),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: InkWell(
                  child: Text("Re-Oder",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8.h,
                        background: Paint()
                          ..color = Colors.black
                          ..strokeWidth = 17
                          ..style = PaintingStyle.stroke,
                      )),
                  onTap: () {},
                ),
              ),
              Expanded(
                  child: MySeparator(
                color: Colors.grey,
              )),
              Text(item.total,
                  textAlign: TextAlign.end,
                  style: TextStyle(color: primary, fontSize: 12.h)),
            ],
          ),
        ),
        SizedBox(height: 10),
        Divider(
          thickness: 5,
          color: Colors.grey[200],
        )
      ],
    );
  }

  StatefulBuilder addReviewDialog(
      GetOrders productItem, GetOrdersItems orderItem) {
    double rating = 3.5;
    return StatefulBuilder(builder: (context, setState) {
      return Container(
        width: MediaQuery.of(context).size.width - 10,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      child: Text(
                        "Add Review",
                        style: TextStyle(fontSize: 20),
                      ),
                      onTap: () {
                        addReviewCall(productItem);
                      },
                    ),
                  ),
                  InkWell(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.cancel,
                        color: primary,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Container(
                  width: 50.w,
                  height: 50.h,
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage("assets/images/no_img.png"),
                        fit: BoxFit.fill),
                  ),
                ),
                Expanded(
                    child: Text(orderItem.name,
                        style: TextStyle(color: primary, fontSize: 12.h))),
              ],
            ),
            Row(
              children: [
                Text("Select Star", style: TextStyle(fontSize: 10)),
                StarRating(
                    rating: rating,
                    color: primary,
                    onRatingChanged: (ratingProduct) {
                      setState(() {
                        rating = ratingProduct;
                      });
                    }),
                SizedBox(width: 3.h),
                Text("(" + rating.toString() + ")",
                    style: TextStyle(fontSize: 10)),
                Spacer(),
                Text("Like", style: TextStyle(fontSize: 10)),
                SizedBox(
                  width: 5.h,
                ),
                Icon(
                  Icons.favorite_border,
                  color: Colors.grey,
                ),
              ],
            ),
            SizedBox(height: 5.h),
            TextField(
              controller: commentController,
              decoration: InputDecoration(
                hintText: "Comment",
                hintStyle: TextStyle(color: Colors.grey),
                isDense: true,
                enabledBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 0.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.grey, width: 0.0),
                ),
                border: const OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 5.h),
            Container(
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    side: BorderSide(color: primary)),
                onPressed: () {},
                color: primary,
                textColor: Colors.white,
                child: Text("Submit", style: TextStyle(fontSize: 18)),
              ),
            ),
            SizedBox(height: 5.h),
            InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Text("No Thanks", style: TextStyle(fontSize: 15)))
          ],
        ),
      );
    });
  }

  Widget viewReviewDialog() {
    double reviewRating = 3.5;
    return Container(
      width: MediaQuery.of(context).size.width - 10,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    "Add Review",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Icon(
                      Icons.cancel,
                      color: primary,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Container(
                width: 50.w,
                height: 50.h,
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage("assets/images/no_img.png"),
                      fit: BoxFit.fill),
                ),
              ),
              Expanded(
                  child: Text("Item Name",
                      style: TextStyle(color: primary, fontSize: 12.h))),
            ],
          ),
          Row(
            children: [
              Text("Select Star", style: TextStyle(fontSize: 10)),
              StarRating(
                color: primary,
                rating: reviewRating,
                // onRatingChanged: (ratingProduct) =>
                //     setState(() => this.rating = ratingProduct),
              ),
              SizedBox(width: 3.h),
              Text("(1)", style: TextStyle(fontSize: 10)),
              Spacer(),
              Text("Like", style: TextStyle(fontSize: 10)),
              SizedBox(
                width: 5.h,
              ),
              Icon(
                Icons.favorite_border,
                color: Colors.grey,
              ),
            ],
          ),
          SizedBox(height: 5.h),
          TextField(
            decoration: InputDecoration(
              hintText: "Comment",
              hintStyle: TextStyle(color: Colors.grey),
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              ),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0),
              ),
              border: const OutlineInputBorder(),
            ),
          ),
        ],
      ),
    );
  }

  Future<GetOrdersModel> addReviewCall(GetOrders productItem) async {
    GetOrdersModel response = await addReviewBloc.addReviewDetail(
        productItem.id, rating, commentController.text);
    hideLoading();
    if (response != null) {
      bool success = response.success;
      if (success) {
        print("success");
        Navigator.pop(context);
      }
    }
  }
}
