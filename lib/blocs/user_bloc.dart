import 'package:restaurant/models/user_data.dart';
import 'package:restaurant/resources/repository.dart';

class UserBloc {
  final _repository = Repository();

  Future<UserData> fetchAllUserData() async {
    UserData userData = await _repository.fetchUserData();
    return userData;
  }
}

final userbloc = UserBloc();