import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/start_rating.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OderDetails extends StatefulWidget {
  final GetOrders orderList;

  const OderDetails({Key key, this.orderList}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OderDetailsState();
  }
}

class OderDetailsState extends Base<OderDetails> {
  double rating = 3.5;
  List<GetOrdersItems> _getOrdersItems;
  GetOrders item;
  String currency;

  @override
  void initState() {
    super.initState();
    getCurrency();
    item = widget.orderList;
    _getOrdersItems = item.items;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  "assets/images/back_arrow.png",
                  color: Colors.white,
                  fit: BoxFit.contain,
                ),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ),
        title: Center(
          child: Text(
            "My Oder Details",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                color: Colors.white, width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                //   child: Text(
                //     cartCount.toString(),
                //     textAlign: TextAlign.center,
                //     style: TextStyle(color: Colors.white, fontSize: 10.h),
                //   ),
                // ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      color: Colors.white, width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            decoration: BoxDecoration(color: Colors.white),
            padding: EdgeInsets.all(15),
            child: ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: [
                Row(
                  children: [
                    Text("Oder: " + item.orderNumber,
                        style: TextStyle(color: primary, fontSize: 17.h)),
                    Text(
                      " • ",
                      style: TextStyle(color: primary, fontSize: 25),
                    ),
                    SizedBox(height: 5),
                    Text(item.orderPlaceAt,
                        style: TextStyle(color: Colors.black, fontSize: 12.h)),
                    // Container(
                    //     height: 20,
                    //     child:
                    //         VerticalDivider(color: Colors.black, thickness: 2)),
                    // Text("12:00am",
                    //     style: TextStyle(color: Colors.black, fontSize: 12.h)),
                  ],
                ),
                SizedBox(height: 5),
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Text("Items",textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 15.h)),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text("Quntity",textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 15.h)),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text("Price",textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black, fontSize: 15.h)),
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Divider(color: Colors.grey[300], height: 1, thickness: 2),
                SizedBox(height: 5),
                ListView.builder(
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemCount: _getOrdersItems.length,
                  itemBuilder: (BuildContext context, int index) {
                    var item = _getOrdersItems[index];
                    return Column(
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(item.name,
                                        style: TextStyle(
                                            color: Colors.black, fontSize: 14.h)),
                                    SizedBox(height: 5),
                                    Text("Medium with Cheese",
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12.h)),
                                    SizedBox(height: 5),
                                    InkWell(
                                      child: Text("Add Review",
                                          style:
                                          TextStyle(color: primary, fontSize: 10.h)),
                                      onTap: () async {
                                        await showDialog(
                                          context: context,
                                          builder: (context) => AlertDialog(
                                              clipBehavior: Clip.antiAliasWithSaveLayer,
                                              content: addReviewDialog()),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                                flex: 2,
                              ),
                              Expanded(
                                child: Text(item.qauntity!=null?item.qauntity.toString():"", textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 13.h)),
                                flex: 1,
                              ),
                              Expanded(
                                child: Text(currency!=null?currency:'' + item.itemTotal,textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 13.h)),
                                flex: 1,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 5),
                        Divider(color: Colors.grey[300], height: 1, thickness: 2),
                        SizedBox(height: 5),
                      ],
                    );
                  },
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text("Sub Total",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 12.h))),
                            Text(currency!=null?currency:''+item.subTotal,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12.h)),
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey[400], height: 1, thickness: 2),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text("DisCount",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12.h)),
                            ),
                            Text(currency!=null?currency:''+item.discountAmount,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12.h)),
                          ],
                        ),
                      ),
                      Divider(color: Colors.grey[400], height: 1, thickness: 2),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          children: [
                            Expanded(
                              child: Text("Tax",
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 12.h)),
                            ),
                            Text(currency!=null?currency:''+item.tax,
                                style: TextStyle(
                                    color: Colors.black, fontSize: 12.h)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Text("Total",
                            style:
                                TextStyle(color: Colors.black, fontSize: 12.h)),
                      ),
                      Text(currency!=null?currency:'' + item.total,
                          style:
                              TextStyle(color: Colors.black, fontSize: 12.h)),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ButtonTheme(
              height: 40.h,
              minWidth: double.infinity,
              child: RaisedButton(
                onPressed: () {},
                color: dark_red,
                textColor: Colors.white,
                child: Text("Re-Oder", style: TextStyle(fontSize: 18.sp)),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget addReviewDialog() {
    double reviewRating = 3.5;
    return Container(
      width: MediaQuery.of(context).size.width - 10,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 8),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    "Add Review",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Icon(
                      Icons.cancel,
                      color: primary,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              Container(
                width: 50.w,
                height: 50.h,
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: AssetImage("assets/images/no_img.png"),
                      fit: BoxFit.fill),
                ),
              ),
              Expanded(
                  child: Text("Item Name",
                      style: TextStyle(color: primary, fontSize: 12.h))),
            ],
          ),
          Row(
            children: [
              Text("Select Star", style: TextStyle(fontSize: 10)),
              StarRating(
                color: primary,
                rating: reviewRating,
                onRatingChanged: (ratingProduct) =>
                    setState(() => this.rating = ratingProduct),
              ),
              SizedBox(width: 3.h),
              Text("(1)", style: TextStyle(fontSize: 10)),
              Spacer(),
              Text("Like", style: TextStyle(fontSize: 10)),
              SizedBox(
                width: 5.h,
              ),
              Icon(
                Icons.favorite_border,
                color: Colors.grey,
              ),
            ],
          ),
          SizedBox(height: 5.h),
          TextField(
            decoration: InputDecoration(
              hintText: "Comment",
              hintStyle: TextStyle(color: Colors.grey),
              isDense: true,
              enabledBorder: const OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey, width: 0.0),
              ),
              disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0),
              ),
              border: const OutlineInputBorder(),
            ),
          ),
          SizedBox(height: 5.h),
          Container(
            width: double.infinity,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(color: primary)),
              onPressed: () {},
              color: primary,
              textColor: Colors.white,
              child: Text("Submit", style: TextStyle(fontSize: 18)),
            ),
          ),
          SizedBox(height: 5.h),
          InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Text("No Thanks", style: TextStyle(fontSize: 15)))
        ],
      ),
    );
  }

  Future<void> getCurrency() async {
    prefs = await SharedPreferences.getInstance();
    currency = prefs.getString(Constants.CURRENCY);
  }

}
