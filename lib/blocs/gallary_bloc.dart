import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/gallary_model.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class GallaryBloc {
  final _repository = Repository();
  final _gallaryFetcher = PublishSubject<GallaryModel>();
  Stream<GallaryModel> get allGallaryImage => _gallaryFetcher.stream;

  fetchGallaryImages() async {
    GallaryModel gallaryModel = await _repository.getGallaryImageData();
    _gallaryFetcher.sink.add(gallaryModel);
  }

}

final gallaryBlock = GallaryBloc();