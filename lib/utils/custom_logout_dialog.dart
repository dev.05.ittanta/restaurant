import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/custom_color.dart';

class CustomAlertDialog extends StatelessWidget {
  final Color bgColor;
  final String title;
  final String message;
  final String positiveBtnText;
  final String negativeBtnText;
  final Function onPostivePressed;
  final Function onNegativePressed;
  final double circularBorderRadius;

  CustomAlertDialog({
    this.title,
    this.message,
    this.circularBorderRadius = 5.0,
    this.bgColor = Colors.white,
    this.positiveBtnText,
    this.negativeBtnText,
    this.onPostivePressed,
    this.onNegativePressed,
  })  : assert(bgColor != null),
        assert(circularBorderRadius != null);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.all(20),
      title: title != null
          ? Text(
              title,
              style: TextStyle(color: logout_yes, fontSize: 18.h),
            )
          : null,
      content: Container(
          width: MediaQuery.of(context).size.width - 10,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Divider(),
              SizedBox(
                height: 15.h,
              ),
              message != null
                  ? Padding(
                    padding: const EdgeInsets.only(left:20.0),
                    child: Text(
                        message,textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15.sp),
                      ),
                  )
                  : null,
              SizedBox(
                height: 40.h,
              ),
              Divider(),
            ],
          )),
      contentPadding: EdgeInsets.zero,
      backgroundColor: bgColor,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(circularBorderRadius)),
      actions: <Widget>[
        negativeBtnText != null
            ? FlatButton(
                child: Text(negativeBtnText),
                color: logout_no,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.of(context).pop();
                  if (onNegativePressed != null) {
                    onNegativePressed();
                  }
                },
              )
            : null,
        positiveBtnText != null
            ? FlatButton(
                color: logout_yes,
                child: Text(positiveBtnText),
                textColor: Colors.white,
                onPressed: () {
                  if (onPostivePressed != null) {
                    onPostivePressed();
                  }
                },
              )
            : null,
      ],
    );
  }
}
