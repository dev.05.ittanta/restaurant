import 'package:restaurant/models/category_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class CategoryBloc {
  final _repository = Repository();
  final _categoryFetcher = PublishSubject<Category>();

  Stream<Category> get allCategory => _categoryFetcher.stream;

  fetchAllCategorys() async {
    Category category = await _repository.fetchCategory();
    _categoryFetcher.sink.add(category);
  }

  dispose() {
    _categoryFetcher.close();
  }
}

final bloc = CategoryBloc();