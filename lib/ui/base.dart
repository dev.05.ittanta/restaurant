import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:restaurant/models/user_data.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

abstract class Base<T extends StatefulWidget> extends State<T> {
  GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _searchControl = new TextEditingController();
  SharedPreferences prefs;
  BuildContext dialogContext;
  UserData user;

  @override
  void initState() {
    super.initState();
    initPreference();
  }

  Future<void> initPreference() async {
    prefs = await SharedPreferences.getInstance();
  }

  // This method is about push to new widget and replace current widget
  pushReplacement(StatefulWidget screenName) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => screenName));
  }

  // This method is about push to new widget but don't replace current widget
  push(StatefulWidget screenName) {
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => screenName));
  }

  // This method is about push to new widget and remove all previous widget
  pushAndRemoveUntil(StatefulWidget screenName) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => screenName),
        (_) => false);
  }

  Future<bool> initUserData() async {
    prefs = await SharedPreferences.getInstance();
    String userPref = prefs.getString(Constants.USER_DATA); // User Data
    String name = prefs.getString(Constants.DISPLAY_NAME);
    String email= prefs.getString(Constants.DISPLAY_EMAIL);
    if (userPref != null) {
      Map map = json.decode(userPref);
      user = UserData.fromJson(map);
    }else{

    }
    return true;
  }



  void showMessage(String message) {
    setState(() {
      Constants.LOGOUT = message;
    });
  }

  showLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        dialogContext = context;
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  hideLoading() {
    if (dialogContext != null) {
      Navigator.pop(dialogContext);
    }
  }

  // // Show loading with optional message params
  // showLoading({String msg}) {
  //   if (msg != null) {
  //     EasyLoading.show(status: msg);
  //   } else {
  //     EasyLoading.show();
  //   }
  // }
  //
  // hideLoadingSuccess(String msg) async {
  //   EasyLoading.showSuccess(msg, duration: Duration(seconds: 2));
  //   await Future.delayed(Duration(seconds: 3));
  //   EasyLoading.dismiss();
  // }
  //
  // hideLoadingError(String msg) async {
  //   EasyLoading.showError(msg, duration: Duration(seconds: 2));
  //   await Future.delayed(Duration(seconds: 3));
  //   EasyLoading.dismiss();
  // }
  //
  // hideLoading() {
  //   EasyLoading.dismiss();
  // }

  /*
   * Show Snackbar with Global scaffold key
   * scaffoldKey is defined globally as snackbar do not find context of Scaffold widget
   * hideLoading is hide the loader when snackbar message is showing to UI
   */
  showSnackBar(String msg) {
    ScaffoldMessenger.of(context)
        .showSnackBar(new SnackBar(content: Text(msg)));
  }

  //
  // showMessage(String title, String message) {
  //   hideLoading();
  //   if (Platform.isAndroid) {
  //     showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext ctxt) {
  //         return AlertDialog(
  //           title: Text(
  //             title,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 22,
  //               fontWeight: FontWeight.bold,
  //               color: Colors.black,
  //             ),
  //           ),
  //           content: Text(
  //             message,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 18,
  //               color: Colors.black,
  //             ),
  //           ),
  //           actions: <Widget>[
  //             FlatButton(
  //               onPressed: () {
  //                 Navigator.pop(context);
  //               },
  //               child: Text(
  //                 "Ok",
  //                 style: TextStyle(
  //                   fontFamily: "Montserrat",
  //                   fontWeight: FontWeight.bold,
  //                 ),
  //               ),
  //             ),
  //           ],
  //         );
  //       },
  //     );
  //   }
  //   if (Platform.isIOS) {
  //     showCupertinoDialog(
  //       context: context,
  //       builder: (BuildContext ctxt) {
  //         return CupertinoAlertDialog(
  //           title: Text(
  //             title,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 22,
  //               fontWeight: FontWeight.bold,
  //               color: Colors.black,
  //             ),
  //           ),
  //           content: Text(
  //             message,
  //             style: TextStyle(
  //               fontFamily: "Montserrat",
  //               fontSize: 18,
  //               color: Colors.black,
  //             ),
  //           ),
  //           actions: <Widget>[
  //             FlatButton(
  //               onPressed: () {
  //                 Navigator.pop(context);
  //               },
  //               child: Text(
  //                 "Ok",
  //                 style: TextStyle(
  //                   fontFamily: "Montserrat",
  //                   fontWeight: FontWeight.bold,
  //                 ),
  //               ),
  //             ),
  //           ],
  //         );
  //       },
  //     );
  //   }
  // }

// Check Internet Connection Async method with Snackbar message.
  Future<bool> isConnected() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      showSnackBar(Constants.INTERNET_CHECK);
      return false;
    }
    showSnackBar(Constants.INTERNET_CHECK);
    return false;
  }
}
