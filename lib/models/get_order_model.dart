class GetOrdersModel {
  bool success;
  List<GetOrders> data;
  String message;

  GetOrdersModel({this.success, this.data, this.message});

  GetOrdersModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<GetOrders>();
      json['data'].forEach((v) {
        data.add(new GetOrders.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class GetOrders {
  int id;
  String orderNumber;
  List<GetOrdersItems> items;
  String subTotal;
  String discountAmount;
  String tax;
  String total;
  String orderPlaceAt;
  String reorderUrl;

  GetOrders(
      {this.id,
        this.orderNumber,
        this.items,
        this.subTotal,
        this.discountAmount,
        this.tax,
        this.total,
        this.orderPlaceAt,
        this.reorderUrl});

  GetOrders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderNumber = json['order_number'];
    if (json['items'] != null) {
      items = new List<GetOrdersItems>();
      json['items'].forEach((v) {
        items.add(new GetOrdersItems.fromJson(v));
      });
    }
    subTotal = json['sub_total'];
    discountAmount = json['discount_amount'];
    tax = json['tax'];
    total = json['total'];
    orderPlaceAt = json['order_place_at'];
    reorderUrl = json['reorder_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_number'] = this.orderNumber;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    data['sub_total'] = this.subTotal;
    data['discount_amount'] = this.discountAmount;
    data['tax'] = this.tax;
    data['total'] = this.total;
    data['order_place_at'] = this.orderPlaceAt;
    data['reorder_url'] = this.reorderUrl;
    return data;
  }
}

class GetOrdersItems {
  int productId;
  String name;
  String description;
  int qauntity;
  String itemTotal;
  Review review;

  GetOrdersItems(
      {this.productId,
        this.name,
        this.description,
        this.qauntity,
        this.itemTotal,
        this.review});

  GetOrdersItems.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    name = json['name'];
    description = json['description'];
    qauntity = json['qauntity'];
    itemTotal = json['item_total'];
    review =
    json['review'] != null ? new Review.fromJson(json['review']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['name'] = this.name;
    data['description'] = this.description;
    data['qauntity'] = this.qauntity;
    data['item_total'] = this.itemTotal;
    if (this.review != null) {
      data['review'] = this.review.toJson();
    }
    return data;
  }
}

class Review {
  int rating;
  String comment;

  Review({this.rating, this.comment});

  Review.fromJson(Map<String, dynamic> json) {
    rating = json['rating'];
    comment = json['comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rating'] = this.rating;
    data['comment'] = this.comment;
    return data;
  }
}
