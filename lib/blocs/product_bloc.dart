import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class ProductBloc {
  final _repository = Repository();
  final _productFetcher = PublishSubject<ProductModel>();
  final _getcartFetcher = PublishSubject<CartModel>();
  final _couponCodeRepository = Repository();

  Stream<ProductModel> get allProducts => _productFetcher.stream;
  Stream<CartModel> get allCartProducts => _getcartFetcher.stream;

  fetchAllProducts(int id,String filterId, String sortByPriceRatng, String sortAscDesc) async {
    ProductModel product = await _repository.fetchProducts(id,filterId,sortByPriceRatng,sortAscDesc);
    return product;
    // _productFetcher.sink.add(product);
  }

  Future<dynamic> submitAddToCartData(String jsonEncode) async {
    dynamic addToCart = await _repository.addToCartProducts(jsonEncode);
    return addToCart;
  }
  fetchCartProducts() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    _getcartFetcher.sink.add(getCartProduct);
  }
  Future<dynamic> updateToCartData(int quantity , int productId) async {
    dynamic upDateToCart = await _repository.updateCartProducts(quantity,productId);
    return upDateToCart;
  }
  Future<PromoCodeModel> applyPromoCode(String couponCode) async {
    PromoCodeModel applyCode = await _couponCodeRepository.getPromoCode(couponCode);
    return applyCode;
  }
  dispose() {
    _productFetcher.close();
    _getcartFetcher.close();
  }
}

final bloc = ProductBloc();