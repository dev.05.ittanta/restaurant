// class Product {
//   bool success;
//   List<ProductData> data;
//   String message;
//
//   Product({this.success, this.data, this.message});
//
//   Product.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     if (json['data'] != null) {
//       data = new List<ProductData>();
//       json['data'].forEach((v) {
//         data.add(new ProductData.fromJson(v));
//       });
//     }
//     message = json['message'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }
//
// class ProductData {
//   int id;
//   String title;
//   String price;
//   String description;
//   String typeImage;
//   Null subTitle;
//   Null calorie;
//   Null preparationTime;
//   int isVariable;
//   List<Variants> variants;
//   List<Categories> categories;
//   List<String> images;
//
//   ProductData(
//       {this.id,
//       this.title,
//       this.price,
//       this.description,
//       this.typeImage,
//       this.subTitle,
//       this.calorie,
//       this.preparationTime,
//       this.isVariable,
//       this.variants,
//       this.categories,
//       this.images});
//
//   ProductData.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     title = json['title'];
//     price = json['price'];
//     description = json['description'];
//     typeImage = json['type_image'];
//     subTitle = json['sub_title'];
//     calorie = json['calorie'];
//     preparationTime = json['preparation_time'];
//     isVariable = json['isVariable'];
//     if (json['variants'] != null) {
//       variants = new List<Variants>();
//       json['variants'].forEach((v) {
//         variants.add(new Variants.fromJson(v));
//       });
//     }
//     if (json['categories'] != null) {
//       categories = new List<Categories>();
//       json['categories'].forEach((v) {
//         categories.add(new Categories.fromJson(v));
//       });
//     }
//     images = json['images'].cast<String>();
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['title'] = this.title;
//     data['price'] = this.price;
//     data['description'] = this.description;
//     data['type_image'] = this.typeImage;
//     data['sub_title'] = this.subTitle;
//     data['calorie'] = this.calorie;
//     data['preparation_time'] = this.preparationTime;
//     data['isVariable'] = this.isVariable;
//     if (this.variants != null) {
//       data['variants'] = this.variants.map((v) => v.toJson()).toList();
//     }
//     if (this.categories != null) {
//       data['categories'] = this.categories.map((v) => v.toJson()).toList();
//     }
//     data['images'] = this.images;
//     return data;
//   }
// }
//
// class Variants {
//   int id;
//   String name;
//   String description;
//   int type;
//   List<Items> items;
//
//   Variants({this.id, this.name, this.description, this.type, this.items});
//
//   Variants.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     description = json['description'];
//     type = json['type'];
//     if (json['items'] != null) {
//       items = new List<Items>();
//       json['items'].forEach((v) {
//         items.add(new Items.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['description'] = this.description;
//     data['type'] = this.type;
//     if (this.items != null) {
//       data['items'] = this.items.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Items {
//   int id;
//   String name;
//   String price;
//   String description;
//   String image;
//   String subTitle;
//   bool isChecked;
//
//   Items(
//       {this.id,
//       this.name,
//       this.price,
//       this.description,
//       this.image,
//       this.subTitle,
//       this.isChecked});
//
//   Items.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     price = json['price'];
//     description = json['description'];
//     image = json['image'];
//     subTitle = json['sub_title'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['price'] = this.price;
//     data['description'] = this.description;
//     data['image'] = this.image;
//     data['sub_title'] = this.subTitle;
//     return data;
//   }
// }
//
// class Categories {
//   int id;
//   String name;
//   String description;
//   String image;
//   String subTitle;
//
//   Categories({this.id, this.name, this.description, this.image, this.subTitle});
//
//   Categories.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     description = json['description'];
//     image = json['image'];
//     subTitle = json['sub_title'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['description'] = this.description;
//     data['image'] = this.image;
//     data['sub_title'] = this.subTitle;
//     return data;
//   }
// }
// class ProductModel {
//   bool success;
//   List<ProductData> data;
//   String message;
//
//   ProductModel({this.success, this.data, this.message});
//
//   ProductModel.fromJson(Map<String, dynamic> json) {
//     success = json['success'];
//     if (json['data'] != null) {
//       data = new List<ProductData>();
//       json['data'].forEach((v) {
//         data.add(new ProductData.fromJson(v));
//       });
//     }
//     message = json['message'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['success'] = this.success;
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     data['message'] = this.message;
//     return data;
//   }
// }
//
// class ProductData {
//   int id;
//   String title;
//   String price;
//   String description;
//   String typeImage;
//   String subTitle;
//   Null calorie;
//   Null preparationTime;
//   int isVariable;
//   List<Variants> variants;
//   List<Categories> categories;
//   List<String> images;
//   List<ProductFilters> filters;
//
//   ProductData(
//       {this.id,
//         this.title,
//         this.price,
//         this.description,
//         this.typeImage,
//         this.subTitle,
//         this.calorie,
//         this.preparationTime,
//         this.isVariable,
//         this.variants,
//         this.categories,
//         this.images,
//         this.filters});
//
//   ProductData.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     title = json['title'];
//     price = json['price'];
//     description = json['description'];
//     typeImage = json['type_image'];
//     subTitle = json['sub_title'];
//     calorie = json['calorie'];
//     preparationTime = json['preparation_time'];
//     isVariable = json['isVariable'];
//     if (json['variants'] != null) {
//       variants = new List<Variants>();
//       json['variants'].forEach((v) {
//         variants.add(new Variants.fromJson(v));
//       });
//     }
//     if (json['categories'] != null) {
//       categories = new List<Categories>();
//       json['categories'].forEach((v) {
//         categories.add(new Categories.fromJson(v));
//       });
//     }
//     images = json['images'].cast<String>();
//     if (json['filters'] != null) {
//       filters = new List<ProductFilters>();
//       json['filters'].forEach((v) {
//         filters.add(new ProductFilters.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['title'] = this.title;
//     data['price'] = this.price;
//     data['description'] = this.description;
//     data['type_image'] = this.typeImage;
//     data['sub_title'] = this.subTitle;
//     data['calorie'] = this.calorie;
//     data['preparation_time'] = this.preparationTime;
//     data['isVariable'] = this.isVariable;
//     if (this.variants != null) {
//       data['variants'] = this.variants.map((v) => v.toJson()).toList();
//     }
//     if (this.categories != null) {
//       data['categories'] = this.categories.map((v) => v.toJson()).toList();
//     }
//     data['images'] = this.images;
//     if (this.filters != null) {
//       data['filters'] = this.filters.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Variants {
//   int id;
//   String name;
//   Null description;
//   int type;
//   List<Items> items;
//
//   Variants({this.id, this.name, this.description, this.type, this.items});
//
//   Variants.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     description = json['description'];
//     type = json['type'];
//     if (json['items'] != null) {
//       items = new List<Items>();
//       json['items'].forEach((v) {
//         items.add(new Items.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['description'] = this.description;
//     data['type'] = this.type;
//     if (this.items != null) {
//       data['items'] = this.items.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Items {
//   int id;
//   String name;
//   String price;
//   Null description;
//   String image;
//   String subTitle;
//   bool isChecked;
//
//   Items(
//       {this.id,
//         this.name,
//         this.price,
//         this.description,
//         this.image,
//         this.subTitle,this.isChecked});
//
//   Items.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     price = json['price'];
//     description = json['description'];
//     image = json['image'];
//     subTitle = json['sub_title'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['price'] = this.price;
//     data['description'] = this.description;
//     data['image'] = this.image;
//     data['sub_title'] = this.subTitle;
//     return data;
//   }
// }
//
// class Categories {
//   int id;
//   String name;
//   String description;
//   String image;
//   Null subTitle;
//
//   Categories({this.id, this.name, this.description, this.image, this.subTitle});
//
//   Categories.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     description = json['description'];
//     image = json['image'];
//     subTitle = json['sub_title'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['description'] = this.description;
//     data['image'] = this.image;
//     data['sub_title'] = this.subTitle;
//     return data;
//   }
// }
//
// class ProductFilters {
//   int id;
//   String name;
//   String image;
//
//   ProductFilters({this.id, this.name, this.image});
//
//   ProductFilters.fromJson(Map<String, dynamic> json) {
//     id = json['id'];
//     name = json['name'];
//     image = json['image'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['id'] = this.id;
//     data['name'] = this.name;
//     data['image'] = this.image;
//     return data;
//   }
// }
class ProductModel {
  bool success;
  List<ProductData> data;
  String message;

  ProductModel({this.success, this.data, this.message});

  ProductModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<ProductData>();
      json['data'].forEach((v) {
        data.add(new ProductData.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class ProductData {
  int id;
  String title;
  String price;
  String description;
  String typeImage;
  Null subTitle;
  Null calorie;
  Null preparationTime;
  int isVariable;
  List<Variants> variants;
  List<Categories> categories;
  List<String> images;
  List<ProductFilters> filters;
  List<Reviews> reviews;
  int avgRating;
  int likesCount;
  int currentUserLiked;

  ProductData(
      {this.id,
        this.title,
        this.price,
        this.description,
        this.typeImage,
        this.subTitle,
        this.calorie,
        this.preparationTime,
        this.isVariable,
        this.variants,
        this.categories,
        this.images,
        this.filters,
        this.reviews,
        this.avgRating,
        this.likesCount,
        this.currentUserLiked});

  ProductData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    price = json['price'];
    description = json['description'];
    typeImage = json['type_image'];
    subTitle = json['sub_title'];
    calorie = json['calorie'];
    preparationTime = json['preparation_time'];
    isVariable = json['isVariable'];
    if (json['variants'] != null) {
      variants = new List<Variants>();
      json['variants'].forEach((v) {
        variants.add(new Variants.fromJson(v));
      });
    }
    if (json['categories'] != null) {
      categories = new List<Categories>();
      json['categories'].forEach((v) {
        categories.add(new Categories.fromJson(v));
      });
    }
    images = json['images'].cast<String>();
    if (json['filters'] != null) {
      filters = new List<ProductFilters>();
      json['filters'].forEach((v) {
        filters.add(new ProductFilters.fromJson(v));
      });
    }
    if (json['reviews'] != null) {
      reviews = new List<Reviews>();
      json['reviews'].forEach((v) {
        reviews.add(new Reviews.fromJson(v));
      });
    }
    avgRating = json['avg_rating'];
    likesCount = json['likes_count'];
    currentUserLiked = json['current_user_liked'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['price'] = this.price;
    data['description'] = this.description;
    data['type_image'] = this.typeImage;
    data['sub_title'] = this.subTitle;
    data['calorie'] = this.calorie;
    data['preparation_time'] = this.preparationTime;
    data['isVariable'] = this.isVariable;
    if (this.variants != null) {
      data['variants'] = this.variants.map((v) => v.toJson()).toList();
    }
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['images'] = this.images;
    if (this.filters != null) {
      data['filters'] = this.filters.map((v) => v.toJson()).toList();
    }
    if (this.reviews != null) {
      data['reviews'] = this.reviews.map((v) => v.toJson()).toList();
    }
    data['avg_rating'] = this.avgRating;
    data['likes_count'] = this.likesCount;
    data['current_user_liked'] = this.currentUserLiked;
    return data;
  }
}

class Variants {
  int id;
  String name;
  Null description;
  int type;
  List<Items> items;

  Variants({this.id, this.name, this.description, this.type, this.items});

  Variants.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    type = json['type'];
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['type'] = this.type;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  int id;
  String name;
  String price;
  Null description;
  String image;
  String subTitle;
  bool isChecked;
  Items(
      {this.id,
        this.name,
        this.price,
        this.description,
        this.image,
        this.subTitle,this.isChecked});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'];
    description = json['description'];
    image = json['image'];
    subTitle = json['sub_title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['price'] = this.price;
    data['description'] = this.description;
    data['image'] = this.image;
    data['sub_title'] = this.subTitle;
    return data;
  }
}

class Categories {
  int id;
  String name;
  String description;
  String image;
  Null subTitle;

  Categories({this.id, this.name, this.description, this.image, this.subTitle});

  Categories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    image = json['image'];
    subTitle = json['sub_title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['image'] = this.image;
    data['sub_title'] = this.subTitle;
    return data;
  }
}

class ProductFilters {
  int id;
  String name;
  String image;

  ProductFilters({this.id, this.name, this.image});

  ProductFilters.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}

class Reviews {
  int rating;
  String comment;
  User user;

  Reviews({this.rating, this.comment, this.user});

  Reviews.fromJson(Map<String, dynamic> json) {
    rating = json['rating'];
    comment = json['comment'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rating'] = this.rating;
    data['comment'] = this.comment;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  String name;
  String image;

  User({this.name, this.image});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}
