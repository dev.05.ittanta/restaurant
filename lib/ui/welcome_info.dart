import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/login.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';

class WelcomeOne extends StatefulWidget {
  @override
  _WelcomeOneState createState() => _WelcomeOneState();
}

class _WelcomeOneState extends Base<WelcomeOne> {
  double deviceHeight(BuildContext context) => MediaQuery.of(context).size.height;
  bool isLastPage = false;

  // @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        ConstrainedBox(
          constraints:  BoxConstraints.expand(),
          child: new Image.asset(
            "assets/images/welcome_bg.png",
            fit: BoxFit.fill,
          ),
        ),
        Swiper.children(
          autoplay: false,
          onIndexChanged: (index) {
            setState(() {
              isLastPage = index == 2;
            });
          },
          pagination: SwiperPagination(

              margin: EdgeInsets.only(bottom: deviceHeight(context) * 0.25),
              builder: DotSwiperPaginationBuilder(
                  color: swiper_dot_inactive,
                  activeColor: swiper_dot_active,
                  size: 10.0,
                  activeSize: 10.0)),
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height:35.h,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/welcome_logo.png',
                  ),
                ),
                SizedBox(
                  height: 30.h,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/model.png',
                  ),
                ),
                SizedBox(
                  height:40.h,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: Text(Constants.WELCOME_ONE_TITLE,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 25.sp, color: Colors.white)),
                ),
                SizedBox(
                  height:20.h,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                  child: Text(Constants.WELCOME_ONE_SUBTITLE,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.sp, color: Colors.white)),
                )
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height:35.h,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/welcome_logo.png',
                  ),
                ),
                SizedBox(
                  height:30.h,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/model.png',
                  ),
                ),
                SizedBox(
                  height: 40.h,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: Text(Constants.WELCOME_ONE_TITLE,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 25.sp, color: Colors.white)),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                  child: Text(Constants.WELCOME_ONE_SUBTITLE,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.sp, color: Colors.white)),
                )
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height:35.h,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/welcome_logo.png',
                  ),
                ),
                SizedBox(
                  height:30.h,
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0.0, 40.0, 0.0, 0.0),
                  alignment: Alignment.center,
                  child: Image.asset(
                    'assets/images/model.png',
                  ),
                ),
                SizedBox(
                  height: 40.h,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(40, 10, 40, 10),
                  child: Text(Constants.WELCOME_ONE_TITLE,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 25.sp, color: Colors.white)),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25, 10, 25, 10),
                  child: Text(Constants.WELCOME_ONE_SUBTITLE,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.sp, color: Colors.white)),
                ),
              ],
            ),
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              isLastPage
                  ? RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: primary)),
                      onPressed: () {
                        pushReplacement(Login());
                      },
                      color: primary,
                      textColor: Colors.white,
                      child: Text(Constants.CONTINUE,
                          style: TextStyle(fontSize: 14.sp)),
                    ) : SizedBox(),
              isLastPage ? SizedBox(height: 70.h) : SizedBox(),
              Row(
                children: [
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(10),
                      // shape: RoundedRectangleBorder(
                      //     borderRadius: BorderRadius.only(
                      //       bottomLeft: const Radius.circular(20.0),
                      //     ),
                      //     side: BorderSide(color: primary)),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0.0),
                          side: BorderSide(color: Colors.red)
                      ),
                      onPressed: () {},
                      color: primary,
                      textColor: Colors.white,
                      child: Text(
                        "Oder Now",
                        style: TextStyle(fontSize: 20.sp),
                      ),
                    ),
                  ),
                  Expanded(
                    child: RaisedButton(
                      padding: EdgeInsets.all(10),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(0.0),
                          side: BorderSide(color: Colors.white)
                      ),
                      onPressed: () {},
                      color: Colors.white,
                      textColor: Colors.black,
                      child: Text("Reserve", style: TextStyle(fontSize: 20.sp)),
                    ),
                  ),
                ],
              )
            ],
          ),
        )
      ],
    ));
  }
}
