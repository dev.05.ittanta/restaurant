import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/appbar.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';


class AboutUs extends KFDrawerContent  {
  @override
  State<StatefulWidget> createState() {
    return AboutUsState();
  }
}

class AboutUsState extends Base<AboutUs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Image.asset(
                "assets/images/menu.png",
                color: Colors.white,
                fit: BoxFit.contain,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "About Us",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",color: Colors.white,
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                //   child: Text(
                //     cartCount.toString(),
                //     textAlign: TextAlign.center,
                //     style: TextStyle(color: Colors.white, fontSize: 10.h),
                //   ),
                // ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",color: Colors.white,
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),

      body:Column(
        children: [
        Text("AboutUs")
        ],
      ),
    );
  }


}
