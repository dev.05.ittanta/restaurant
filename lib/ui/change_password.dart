import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/blocs/login_bloc.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/login.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePassword extends KFDrawerContent {
  @override
  State<StatefulWidget> createState() {
    return PasswordState();
  }
}

class PasswordState extends Base<ChangePassword> {
  final oldPwController = TextEditingController();
  final newPwController = TextEditingController();
  final confirmPwController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Image.asset(
                "assets/images/back_arrow.png",
                color: Colors.black,
              ),
            )),
        leadingWidth: 30.h,
        title: Center(
          child: Text(
            "Change Password",
            style: TextStyle(color: Colors.white),
          ),
        ),
        backgroundColor: cng_pw_title_bg,
        actions: [
          Row(
            children: [
              GestureDetector(
                onTap: () {},
                child: Image.asset("assets/images/home_food_menu.png",
                    color: Colors.white, width: 25.h, height: 20.h),
              ),
              SizedBox(width: 15.h),
              GestureDetector(
                onTap: () {},
                child: Image.asset("assets/images/cart.png",
                    color: Colors.white, width: 25.h, height: 25.h),
              ),
              SizedBox(width: 15.h),
            ],
          )
        ],
      ),
      body: Stack(
        children: [
          ListView(
            padding: EdgeInsets.all(10),
            children: [
              TextField(
                obscureText: true,
                controller: oldPwController,
                decoration: new InputDecoration(
                  filled: true,
                  fillColor: cng_pw_bg_clr,
                  hintText: "Old Password",
                  hintStyle: TextStyle(color: cng_pw_hint),
                  isDense: true,
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.grey, width: 0.0),
                  ),
                  border: const OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              TextField(
                obscureText: true,
                controller: newPwController,
                decoration: new InputDecoration(
                  filled: true,
                  fillColor: cng_pw_bg_clr,
                  hintText: "New Password",
                  hintStyle: TextStyle(color: cng_pw_hint),
                  isDense: true,
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.grey, width: 0.0),
                  ),
                  border: const OutlineInputBorder(),
                ),
              ),
              SizedBox(
                height: 10.h,
              ),
              TextField(
                obscureText: true,
                controller: confirmPwController,
                decoration: new InputDecoration(
                  filled: true,
                  fillColor: cng_pw_bg_clr,
                  hintText: "Conform Password",
                  hintStyle: TextStyle(color: cng_pw_hint),
                  isDense: true,
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.grey, width: 0.0),
                  ),
                  border: const OutlineInputBorder(),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      _changePasswordData();
                    },
                    child: Container(
                      height: 40.h,
                      alignment: Alignment.center,
                      child: Text(
                        Constants.Save,
                        style: TextStyle(fontSize: 20.sp, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      color: primary,
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      height: 40.h,
                      alignment: Alignment.center,
                      child: Text(
                        Constants.CANCEL,
                        style: TextStyle(fontSize: 20.sp, color: Colors.white),
                        textAlign: TextAlign.center,
                      ),
                      color: cancel_pw,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _changePasswordData() async {
    Map<String, dynamic> res = await bloc.changePwDetail(
        oldPwController.text, newPwController.text,confirmPwController.text);
    bool success = res["success"];
    if (success) {
      hideLoading();
      showSnackBar(res["message"]);
      push(Login());
    } else {
      if(res["data"].containsKey("password")){
        showSnackBar(res["data"]["password"][0]);
      }
      if(res["data"].containsKey("current_password")){
        showSnackBar(res["data"]["current_password"][0]);
      }
      if(res["data"].containsKey("confirm_password")){
        showSnackBar(res["data"]["confirm_password"][0]);
      }
      hideLoading();
    }
  }
}
