import 'package:restaurant/resources/repository.dart';

class ForgotPasswordBloc {
  final _repository = Repository();

  Future<Map<String, dynamic>> doforgotPwDetail( String email) async {
    Map<String, dynamic> confirmItem = await _repository.checkForgotPwDetail(email);
    return confirmItem;
  }
}

final bloc = ForgotPasswordBloc();