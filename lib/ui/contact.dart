import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/blocs/contact_us_bloc.dart';
import 'package:restaurant/models/contact_us.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:restaurant/utils/kf_drawer.dart';

class ContactScreen extends KFDrawerContent {

  @override
  State<StatefulWidget> createState() {
    return ContactState();
  }
}

class ContactState extends Base<ContactScreen> {
  ContactData contactdata;
  Contact contact;
  Business business;
  List<Hours>_hoursList;
  @override
  Widget build(BuildContext context) {
    contactBlock.fetchContact();

    return Scaffold(
      appBar: AppBar(
        backgroundColor: primary,
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Image.asset(
                "assets/images/menu.png",
                color: Colors.white,
                fit: BoxFit.contain,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
        title: Center(
          child: Text(
            "Contact",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                color: Colors.white, width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                //   child: Text(
                //     cartCount.toString(),
                //     textAlign: TextAlign.center,
                //     style: TextStyle(color: Colors.white, fontSize: 10.h),
                //   ),
                // ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      color: Colors.white, width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: StreamBuilder(
        stream: contactBlock.allContact,
        builder: (context, AsyncSnapshot<ContactUsModel> snapshot) {
          if (snapshot.hasData) {
            contactdata = snapshot.data.data != null ? snapshot.data.data : null;
            contact = contactdata.contact;
            business = contactdata.business;
            _hoursList = business.hours;
            return buildList();
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return Center(child: CircularProgressIndicator());
        },
      ),

    );
  }

  Widget buildList() {
    return  Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                Row(
                  children: [
                    Text(contact.label, style: TextStyle(fontSize: 17)),
                  ],
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Text(
                      'Address: ',
                      style: TextStyle(fontSize: 14),
                    ),
                    Text(contact.address,
                        style: TextStyle(fontSize: 10)),
                  ],
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Text(
                      'Phone: ',
                      style: TextStyle(fontSize: 14),
                    ),
                    Text(contact.phone, style: TextStyle(fontSize: 10)),
                  ],
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Text(
                      'Fax: ',
                      style: TextStyle(fontSize: 14),
                    ),
                    Text(contact.fax, style: TextStyle(fontSize: 10)),
                  ],
                ),
                SizedBox(
                  height: 5.h,
                ),
                Row(
                  children: [
                    Text(
                      'Email: ',
                      style: TextStyle(fontSize: 14),
                    ),
                    Text(contact.email,
                        style: TextStyle(fontSize: 10)),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                Row(
                  children: [
                    Text(business.label, style: TextStyle(fontSize: 17)),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                ListView.builder(
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    itemCount: _hoursList.length,
                    itemBuilder: (BuildContext context, int index) {
                      var item = _hoursList[index];
                      return Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                item.label,
                                style: TextStyle(fontSize: 14),
                              ),
                              Text(item.days+item.time,
                                  style: TextStyle(fontSize: 10)),
                            ],
                          ),
                          SizedBox(
                            height: 5.h,
                          ),
                        ],
                      );
                    }),

                // Row(
                //   children: [
                //     Text(
                //       'Dinner: ',
                //       style: TextStyle(fontSize: 14),
                //     ),
                //     Text('Mon - Thu 5:00pm - 10:00pm',
                //         style: TextStyle(fontSize: 10)),
                //   ],
                // ),
                // SizedBox(
                //   height: 5.h,
                // ),
                // Row(
                //   children: [
                //     Text(
                //       'Dinner: ',
                //       style: TextStyle(fontSize: 14),
                //     ),
                //     Text('Fri, Sat 5:00pm - 11:00pm',
                //         style: TextStyle(fontSize: 10)),
                //   ],
                // ),
                // SizedBox(
                //   height: 5.h,
                // ),
                // Row(
                //   children: [
                //     Text(
                //       'Dinner: ',
                //       style: TextStyle(fontSize: 14),
                //     ),
                //     Text('Sun - 4:00pm - 9:00pm',
                //         style: TextStyle(fontSize: 10)),
                //   ],
                // ),
                // SizedBox(
                //   height: 5.h,
                // ),
                // Row(
                //   children: [
                //     Text(
                //       'Dinner: ',
                //       style: TextStyle(fontSize: 14),
                //     ),
                //     Text('', style: TextStyle(fontSize: 10)),
                //   ],
                // ),
                // SizedBox(
                //   height: 10.h,
                // ),
                Row(
                  children: [
                    Text('Happy Hours', style: TextStyle(fontSize: 17)),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                Row(
                  children: [
                    Text('Mon - Fri 5:00pm - 6:00pm and 9:00pm - Closed',
                        style: TextStyle(fontSize: 10)),
                  ],
                ),
                SizedBox(
                  height: 10.h,
                ),
                Row(
                  children: [
                    Text('Follow Us', style: TextStyle(fontSize: 15)),
                    SizedBox(
                      width: 30.w,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Image.asset(
                        "assets/images/contact_fb.png",
                        width: 30.w,
                        height: 30.h,
                      ),
                    ),
                    SizedBox(
                      width: 20.w,
                    ),
                    InkWell(
                      onTap: () {},
                      child: Image.asset(
                        "assets/images/twt.png",
                        width: 30.w,
                        height: 30.h,
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child:
                  Text('Get In Tounch', textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        decoration: new InputDecoration(
                          filled: true,
                          fillColor: cng_pw_bg_clr,
                          labelText: "FirstName",
                          hintStyle: TextStyle(color: hint_clr),
                          isDense: true,
                          enabledBorder: const OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey),
                          ),

                          border: const OutlineInputBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Expanded(child: TextField(
                      decoration: new InputDecoration(
                        filled: true,
                        fillColor: cng_pw_bg_clr,
                        labelText: "LastName",
                        hintStyle: TextStyle(color: hint_clr),
                        isDense: true,
                        enabledBorder: const OutlineInputBorder(
                          borderSide: const BorderSide(color: Colors.grey),
                        ),

                        border: const OutlineInputBorder(),
                      ),
                    ),)
                  ],
                ),
                SizedBox(height: 10.h,),
                TextField(
                  decoration: new InputDecoration(
                    filled: true,
                    fillColor: cng_pw_bg_clr,
                    labelText: "Email",
                    hintStyle: TextStyle(color: hint_clr),
                    isDense: true,
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey),
                    ),

                    border: const OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 10.h,),
                TextField(
                  decoration: new InputDecoration(
                    filled: true,
                    fillColor: cng_pw_bg_clr,
                    labelText: "Message",
                    hintStyle: TextStyle(color: hint_clr),
                    isDense: true,
                    enabledBorder: const OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey),
                    ),

                    border: const OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 10.h,),
                ButtonTheme(
                  height: 40.h,
                  minWidth: 290.h,
                  child: RaisedButton(
                    onPressed: () {
                    },
                    color: Colors.black,
                    textColor: Colors.white,
                    child:
                    Text("SEND MESSAGE", style: TextStyle(fontSize: 15.sp)),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget businessItemUi() {}

}
