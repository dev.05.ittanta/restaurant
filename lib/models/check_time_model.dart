class CheckTime {
  bool success;
  String message;
  CheckTimeData data;

  CheckTime({this.success, this.message, this.data});

  CheckTime.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new CheckTimeData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CheckTimeData {
  String dateTime;

  CheckTimeData({this.dateTime});

  CheckTimeData.fromJson(Map<String, dynamic> json) {
    dateTime = json['date_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date_time'] = this.dateTime;
    return data;
  }
}
