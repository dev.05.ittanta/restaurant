import 'package:restaurant/models/add_address_model.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class AddressBloc {
  final _repository = Repository();
  final _getrepository = Repository();


  Future<Map<String, dynamic>> addAddress(String address,String street,String city,String state,String country,String pincode,String latitude,String longitude) async {
    Map<String, dynamic> addAddressData = await _repository.addAddress(address,street,city,state,country,pincode,latitude,longitude);
    return addAddressData;
  }
  Future<Map<String, dynamic>> editAddress(int id,String address,String street,String city,String state,String country,String pincode,String latitude,String longitude) async {
    Map<String, dynamic> editAddress = await _repository.editAddress(id,address,street,city,state,country,pincode,latitude,longitude);
    return editAddress;
  }
  Future<Map<String, dynamic>> deleteAddress(int id ) async {
    Map<String, dynamic> deleteAddress = await _repository.deleteAddress(id);
    return deleteAddress;
  }
  Future<AddressModel> getAddress() async {
    AddressModel getAddress = await _getrepository.getAddress();
    return getAddress;
  }
  Future<Map<String, dynamic>> checkTimeForDeliveryOder(String date,String time) async {
    Map<String, dynamic> checkTime = await _repository.checkTimeForDelivery(date,time);
    return checkTime;
  }

}

final addressbloc = AddressBloc();