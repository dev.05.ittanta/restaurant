import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:restaurant/ui/splash.dart';
import 'package:restaurant/utils/class_builder.dart';
import 'package:restaurant/utils/const.dart';
import 'package:restaurant/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  ClassBuilder.registerClasses();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
        builder: () => MaterialApp(
          debugShowCheckedModeBanner: false,
          title: Strings.appName,
          theme: ThemeData(
              primarySwatch: Colors.blue,
              canvasColor: Colors.transparent,
              fontFamily: 'Amarnath'),
          home: SplashScreen(),
        ),
    );
  }
}
