import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/componet/custom_dropdown.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/resources/repository.dart';
import 'package:restaurant/ui/add_to_cart.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/ui/category.dart';
import 'package:restaurant/utils/custom_color.dart';

class AddReservation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddReservationState();
  }
}

class AddReservationState extends Base<AddReservation> {

  TimeOfDay pickedTime = new TimeOfDay.now();
  TextEditingController _dateControlller = new TextEditingController();
  TextEditingController _timeController = new TextEditingController();
  TextEditingController _reserveNameController = new TextEditingController();
  TextEditingController _reserveEmailController = new TextEditingController();
  TextEditingController _reservePhoneController = new TextEditingController();
  DateTime selectedDate = DateTime.now();
  var format = new DateFormat("yyyy-MM-dd");
  final _repository = Repository();
  int cartCount = 0;
  String getTime = DateFormat('HH:mm').format(DateTime.now());

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        return TextEditingValue(text: format.format(selectedDate));
      });
  }
  List<DropdownMenuItem<String>> _selectPersonList;

  List<DropdownMenuItem<String>> _buildSelectPersonDropdown() {
    List<DropdownMenuItem<String>> selectPerson = List();

    selectPerson.add(DropdownMenuItem(
      value: "0",
      child: Text("Number of People"),
    ));
    for (String person in selectitems) {
      selectPerson.add(DropdownMenuItem(
        value: person,
        child: Text(person),
      ));
    }
    return selectPerson;
  }

  String selectedPerson = "0";
  final List<String> selectitems = <String>[
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:primary,
        leading: Padding(
          padding: const EdgeInsets.all(14.0),
          child: InkWell(
            child: Image.asset(
              "assets/images/back_arrow.png",
              color: Colors.white,
              fit: BoxFit.contain,
              width: 30.w,
            ),
            onTap: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Center(
          child: Text(
           "Reservation",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 15.h),
          ),
        ),
        centerTitle: true,
        actions: [
          GestureDetector(
            onTap: () {
              push(CategoryList());
            },
            child: Image.asset("assets/images/home_food_menu.png",
                color: Colors.white,
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          Center(
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(12, 2, 0, 0),
                  child: Text(
                    cartCount.toString(),
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 10.h),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    push(AddToCart());
                  },
                  child: Image.asset("assets/images/cart.png",
                      color: Colors.white,
                      width: 25.h, height: 25.h),
                ),
              ],
            ),
          ),
          SizedBox(width: 15.h),
        ],
      ),
      body: Container(
        color: Colors.white,
        alignment: Alignment.topCenter,
        child: Stack(
          children: [
            ListView(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              children: [
            // _selectPersonList = _buildSelectPersonDropdown();
        SizedBox(
                  height: 30.h,
                ),
                Text(
                  "Book a Table",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 30, color: primary),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    child: GestureDetector(
                      onTap: () {
                        selectDate(context);
                      },
                      child: AbsorbPointer(
                        child: TextField(
                          controller: _dateControlller
                            ..text = selectedDate != null ? format.format(selectedDate) : "",
                          autocorrect: true,
                          keyboardType: TextInputType.datetime,
                          decoration: InputDecoration(
                            isDense: true,
                            hintText: 'Date',
                            suffixIcon: Icon(Icons.date_range),
                            hintStyle: TextStyle(color: Colors.grey),
                            filled: true,
                            fillColor: Colors.white70,
                            enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 1),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2.0)),
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 1),
                            ),
                          ),
                        ),
                      ),
                    )),
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(10.0),
                  child: TextField(
                    controller: _timeController..text = getTime != null ? getTime : "",
                    autocorrect: true,
                    decoration: InputDecoration(
                      isDense: true,
                      hintText: 'Time',
                      suffixIcon: Icon(Icons.access_time_rounded),
                      hintStyle: TextStyle(color: Colors.grey),
                      filled: true,
                      fillColor: Colors.white70,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2.0)),
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2.0)),
                        borderSide: BorderSide(color: Colors.grey, width: 1),
                      ),
                    ),
                    onTap: () async {
                      TimeOfDay time = TimeOfDay.now();
                      FocusScope.of(context).requestFocus(new FocusNode());
                      TimeOfDay picked = await showTimePicker(
                        context: context,
                        initialTime: time,
                      );

                      if (picked != null && picked != time) {
                        setState(() {
                          String minute = picked.minute.toString();
                          if (minute.length == 1) {
                            minute = "0" + minute;
                          }
                          getTime = picked.hour.toString() + ":" + minute;
                        });
                      }
                    },
                  ),
                ),
                Container(
                  width: double.infinity,
                  child:  CustomDropdown<String>(
                    dropdownMenuItemList: _selectPersonList,
                    onChanged: (String person) {
                      setState(() {
                        selectedPerson = person;
                      });
                    },
                    value: selectedPerson,
                    isEnabled: true,
                  ),
                ),

                SizedBox(
                  height: 20.h,
                ),
                Text(
                  "Contact Details",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 30, color: primary),
                ),
                SizedBox(
                  height: 20.h,
                ),
                Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: _reserveNameController,
                      autocorrect: true,
                      decoration: InputDecoration(
                        isDense: true,
                        hintText: 'Name',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                      ),
                    )),
                Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: _reserveEmailController,
                      autocorrect: true,
                      decoration: InputDecoration(
                        isDense: true,
                        hintText: 'Email',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                      ),
                    )),
                Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(10.0),
                    child: TextField(
                      controller: _reservePhoneController,
                      autocorrect: true,
                      decoration: InputDecoration(
                        isDense: true,
                        hintText: 'Phone',
                        hintStyle: TextStyle(color: Colors.grey),
                        filled: true,
                        fillColor: Colors.white70,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2.0)),
                          borderSide: BorderSide(color: Colors.grey, width: 1),
                        ),
                      ),
                    )),
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  padding: EdgeInsets.all(12),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                      side: BorderSide(color: primary)),
                  onPressed: () {
                    postReserveData(_dateControlller.text,_timeController.text,_reserveNameController.text,_reserveEmailController.text,_reservePhoneController.text);
                  },
                  color: primary,
                  textColor: Colors.white,
                  child: Text(
                    "Request Booking",
                    style: TextStyle(fontSize: 17),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );

  }
  getCartCount() async {
    CartModel getCartProduct = await _repository.getCartProducts();
    setState(() {
      cartCount = getCartProduct.data.itemCount;
    });
  }

  void postReserveData(String date, String time, String name, String email, String Phone) {
  }


}
