class AddressModel {
  bool success;
  List<AddressData> data;
  String message;

  AddressModel({this.success, this.data, this.message});

  AddressModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    if (json['data'] != null) {
      data = new List<AddressData>();
      json['data'].forEach((v) {
        data.add(new AddressData.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class AddressData {
  int id;
  String address;
  String street;
  String city;
  String province;
  String pincode;
  String country;
  String latitude;
  String longitude;
  int isDefult;

  AddressData(
      {this.id,
        this.address,
        this.street,
        this.city,
        this.province,
        this.pincode,
        this.country,
        this.latitude,
        this.longitude,
        this.isDefult});

  AddressData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    address = json['address'];
    street = json['street'];
    city = json['city'];
    province = json['province'];
    pincode = json['pincode'];
    country = json['country'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    isDefult = json['is_defult'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['address'] = this.address;
    data['street'] = this.street;
    data['city'] = this.city;
    data['province'] = this.province;
    data['pincode'] = this.pincode;
    data['country'] = this.country;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['is_defult'] = this.isDefult;
    return data;
  }
}
