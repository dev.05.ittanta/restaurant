import 'package:restaurant/models/add_address_model.dart';
import 'package:restaurant/models/cart_model.dart';
import 'package:restaurant/models/category_model.dart';
import 'package:restaurant/models/contact_us.dart';
import 'package:restaurant/models/gallary_model.dart';
import 'package:restaurant/models/get_order_model.dart';
import 'package:restaurant/models/product.dart';
import 'package:restaurant/models/promocode_model.dart';
import 'package:restaurant/models/user_data.dart';
import 'package:restaurant/resources/api_provider.dart';
import 'package:restaurant/utils/strings.dart';

class Repository {
  final apiProvider = ApiProvider();

  Future<Map<String, dynamic>> checkLogin(String email, String password) =>
      apiProvider.checkLoginData(email, password);

  Future<Map<String, dynamic>> checkSignUp(String name, String email,
          String phone, String password, String confirmPw) =>
      apiProvider.checkSignUpData(name, email, phone, password, confirmPw);

  Future<Map<String, dynamic>> checkConfirmDetail(String name, String email,
          String phone, String provider, String providerId) =>
      apiProvider.checkConfirmData(name, email, phone, provider, providerId);

  Future<Map<String, dynamic>> checkSocialDetail(
          String provider, String providerId) =>
      apiProvider.checkSocialLoginData(provider, providerId);

  Future<Map<String, dynamic>> changePwDetail(
          String currentPw, String oldPw, String confirmPw) =>
      apiProvider.changePasswordData(currentPw, oldPw, confirmPw);

  Future<Map<String, dynamic>> checkForgotPwDetail(String email) =>
      apiProvider.forgotPwData(email);

  Future<UserData> fetchUserData() => apiProvider.fetchUserData();

  Future<Category> fetchCategory() => apiProvider.fetchCategoryData();

  Future<ProductModel> fetchProducts(int id, String filterId,
          String sortByPriceRatng, String sortAscDesc) =>
      apiProvider.fetchProductData(id, filterId, sortByPriceRatng, sortAscDesc);

  Future<dynamic> addToCartProducts(String jsonEncode) =>
      apiProvider.addToCartData(jsonEncode);

  Future<CartModel> getCartProducts() => apiProvider.fetchCartData();

  Future<dynamic> updateCartProducts(int quantity, int productId) =>
      apiProvider.updateCartData(quantity, productId);

  Future<PromoCodeModel> getPromoCode(String couponCode) =>
      apiProvider.promocodeData(couponCode);

  Future<Map<String, dynamic>> addAddress(
          String address,
          String street,
          String city,
          String state,
          String country,
          String pincode,
          String latitude,
          String longitude) =>
      apiProvider.addProfileAddressData(
          address, street, city, state, country, pincode, latitude, longitude);

  Future<AddressModel> getAddress() => apiProvider.getAddressData();

  Future<Map<String, dynamic>> editAddress(
          int id,
          String address,
          String street,
          String city,
          String state,
          String country,
          String pincode,
          String latitude,
          String longitude) =>
      apiProvider.editAddressData(id, address, street, city, state, country,
          pincode, latitude, longitude);

  Future<Map<String, dynamic>> deleteAddress(int id) =>
      apiProvider.deleteAddressData(id);

  Future<Map<String, dynamic>> checkTimeForDelivery(String date, String time) =>
      apiProvider.checkTimeData(date, time);

  Future<GetOrdersModel> getOrderData() => apiProvider.getOrder();

  Future<dynamic> addReview(int productId, double rating, String comment) =>
      apiProvider.addReviewData(productId, rating, comment);
  Future<ContactUsModel> getContactData() => apiProvider.getContactUs();

  Future<GallaryModel> getGallaryImageData() => apiProvider.getGallaryImage();
}
