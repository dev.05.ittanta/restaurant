
import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}
Color swiper_dot_active = HexColor("f53b50");
Color swiper_dot_inactive = HexColor("948c8a");
Color primary = HexColor("f53b50");
Color font_clr = HexColor("424242");
Color cancel_pw = HexColor("3d4255");
Color cng_pw_hint = HexColor("afafaf");
Color drawer_font_clr = HexColor("4f4f4f");
// Color drawer_bg_clr = HexColor("e6e7e8");
Color hint_clr = HexColor("303030");
Color sky_blue = HexColor("23aad6");
Color white = HexColor("FFFFFF");
Color dark_red = HexColor("ca0000");
Color category_bg = HexColor("faf7f7");
Color logout_no = HexColor("f75d4d");
Color logout_yes = HexColor("4ca8db");
Color drawer_bg_clr = HexColor("e6e6e7");
Color filter_bg = HexColor("f4f4f4");
Color filter_titile__bg = HexColor("fafafa");
Color cng_pw_title_bg = HexColor("f53b50");
Color green_clr = HexColor("77ab59");
Color yellow_clr = HexColor("ffb501");
Color arrow_shadow_clr = HexColor("e9e7e7");
Color cng_pw_bg_clr = HexColor("f9f9f9");
Color radio_inactive_clr = HexColor("eaebec");
Color re_order_bg = HexColor("edf2f2");















