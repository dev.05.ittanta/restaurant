class CartModel {
  bool success;
  CartData data;
  String message;

  CartModel({this.success, this.data, this.message});

  CartModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    data = json['data'] != null ? new CartData.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class CartData {
  int id;
  int itemCount;
  List<CartItems> items;
  String subTotal;
  String taxAmount;
  String cartTotal;
  List<Coupons> coupons;

  CartData(
      {this.id,
        this.itemCount,
        this.items,
        this.subTotal,
        this.taxAmount,
        this.cartTotal,
        this.coupons});

  CartData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    itemCount = json['item_count'];
    if (json['items'] != null) {
      items = new List<CartItems>();
      json['items'].forEach((v) {
        items.add(new CartItems.fromJson(v));
      });
    }
    subTotal = json['sub_total'];
    taxAmount = json['tax_amount'];
    cartTotal = json['cart_total'];
    if (json['coupons'] != null) {
      coupons = new List<Coupons>();
      json['coupons'].forEach((v) {
        coupons.add(new Coupons.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['item_count'] = this.itemCount;
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    data['sub_total'] = this.subTotal;
    data['tax_amount'] = this.taxAmount;
    data['cart_total'] = this.cartTotal;
    if (this.coupons != null) {
      data['coupons'] = this.coupons.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CartItems {
  int id;
  CartProduct product;
  int quantity;
  String name;
  String description;
  String updateUrl;

  CartItems(
      {this.id,
        this.product,
        this.quantity,
        this.name,
        this.description,
        this.updateUrl});

  CartItems.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    product =
    json['product'] != null ? new CartProduct.fromJson(json['product']) : null;
    quantity = json['quantity'];
    name = json['name'];
    description = json['description'];
    updateUrl = json['update_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    data['quantity'] = this.quantity;
    data['name'] = this.name;
    data['description'] = this.description;
    data['update_url'] = this.updateUrl;
    return data;
  }
}

class CartProduct {
  int id;
  String price;
  String image;

  CartProduct({this.id, this.price, this.image});

  CartProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    price = json['price'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['price'] = this.price;
    data['image'] = this.image;
    return data;
  }
}

class Coupons {
  String code;
  String name;
  String description;
  bool isChecked;


  Coupons({this.code, this.name, this.description,this.isChecked});

  Coupons.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['description'] = this.description;
    return data;
  }
}
