import 'package:restaurant/resources/repository.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc {
  final _repository = Repository();
  final _loginFetcher = PublishSubject<Map<String, dynamic>>();

  // Stream<Map<String, dynamic>> get allMovies => _loginFetcher.stream;
  //
  // Map<String, dynamic> get checkLogin => _loginFetcher;

  // doLogin(String email, String password) async {
  //   Map<String, dynamic> itemModel = await _repository.checkLogin(email, password);
  //   _loginFetcher.sink.add(itemModel);
  // }

  Future<Map<String, dynamic>> doLogin(String email, String password) async {
    Map<String, dynamic> loginItem = await _repository.checkLogin(email, password);
    return loginItem;
  }
  Future<Map<String, dynamic>> doSocialDetail(String provider,String providerId) async {
    Map<String, dynamic> confirmItem = await _repository.checkSocialDetail(provider,providerId);
    return confirmItem;
  }

  Future<Map<String, dynamic>> changePwDetail(String currentPw, String oldPw, String confirmPw) async {
    Map<String, dynamic> changePwDetails = await _repository.changePwDetail(currentPw,oldPw,confirmPw);
    return changePwDetails;
  }

  dispose() {
    _loginFetcher.close();
  }
}

final bloc = LoginBloc();