import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restaurant/ui/base.dart';
import 'package:restaurant/utils/custom_color.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:restaurant/utils/kf_drawer.dart';


class Menu extends KFDrawerContent {
  @override
  State<StatefulWidget> createState() {
    return MenuState();
  }
}

class MenuState extends Base<Menu> {
  final TextEditingController _searchControl = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: <Widget>[
            appBar(Image.asset("assets/images/menu.png", color: Colors.black,),"Menu",Colors.white,sky_blue),
            searchBar(),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('Menu'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  appBar(Image dwricon, String title, Color bgclr,Color titleclr) {
    return  Container(
      color: bgclr,
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(32.0)),
            child: Material(
              shadowColor: Colors.transparent,
              color:  Colors.transparent,
              child: IconButton(
                icon: dwricon,
                onPressed: widget.onMenuPressed,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: TextStyle(color:titleclr, fontSize: 15.h),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/home_food_menu.png",
                width: 25.h, height: 20.h),
          ),
          SizedBox(width: 10.h),
          GestureDetector(
            onTap: () {},
            child: Image.asset("assets/images/cart.png",
                width: 25.h, height: 25.h),
          ),
          SizedBox(width: 15.h),
        ],
      ),
    );
  }
  searchBar() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(
              Radius.circular(0.0),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 2,
                child: TextField(
                  style: TextStyle(
                    fontSize: 15.0,
                    color: Colors.white,
                  ),
                  // onChanged: onSearchTextChanged,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(20.0),
                    // border: OutlineInputBorder(
                    //   borderRadius: BorderRadius.circular(5.0),
                    //   borderSide: BorderSide(color: Colors.white,),
                    // ),
                    // enabledBorder: OutlineInputBorder(
                    //   borderSide: BorderSide(color: Colors.white,),
                    //   borderRadius: BorderRadius.circular(5.0),
                    // ),
                    hintText: "Search",
                    suffixIcon: Icon(
                      Icons.search,
                      color: primary,
                    ),
                    hintStyle: TextStyle(
                      fontSize: 15.0,
                      color: Colors.white,
                    ),
                  ),
                  maxLines: 1,
                  controller: _searchControl,
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(right: 5.h),
                  color: Colors.white,
                  width: 30.h,
                  height: 20.h,
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Row(
                      children: [
                        Image.asset(
                          "assets/images/filter_icon.png",
                          width: 10.h,
                          height: 10.h,
                        ),
                        SizedBox(
                          width: 5.h,
                        ),
                        Text(
                          "Filter",
                          style: TextStyle(fontSize: 12.sp),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }


}
